﻿/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) http://ckeditor.com/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) http://ckeditor.com/builder/e6925e77546f38a92a7f1fe3b28dff17
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) http://ckeditor.com/builder/download/e6925e77546f38a92a7f1fe3b28dff17
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'bootstrapck',
	preset: 'standard',
	ignore: [
		'dev',
		'.gitignore',
		'.gitattributes',
		'README.md',
		'.mailmap'
	],
	plugins : {
		'basicstyles' : 1,
		'blockquote' : 1,
		'clipboard' : 1,
		'contextmenu' : 1,
		'enterkey' : 1,
		'entities' : 1,
		'floatingspace' : 1,
		'format' : 1,
		'horizontalrule' : 1,
		'htmlwriter' : 1,
		'image' : 1,
		'imagepaste' : 1,
		'imageresize' : 1,
		'indentlist' : 1,
		'letterspacing' : 1,
		'link' : 1,
		'list' : 1,
		'magicline' : 1,
		'removeformat' : 1,
		'showborders' : 1,
		'sourcearea' : 1,
		'specialchar' : 1,
		'stylescombo' : 1,
		'tab' : 1,
		'table' : 1,
		'tableresize' : 1,
		'tabletools' : 1,
		'toolbarswitch' : 1,
		'undo' : 1,
		'wysiwygarea' : 1,
		'youtube' : 1
	},
	languages : {
		'en' : 1
	}
};
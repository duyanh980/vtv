var scripts = document.getElementsByTagName('script');
var myScript = scripts[ scripts.length - 1 ];
var queryString = myScript.src.replace(/^[^\?]+\??/, '');
var params = parseQuery(queryString);

function parseQuery(query) {
    var Params = new Object();
    if (!query) return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length != 2) continue;
        var key = unescape(KeyVal[0]);
        var val = unescape(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
}

$.getScript('//connect.facebook.net/en_UK/all.js', function(){
    window.fbAsyncInit = function() {
        FB.init({
            appId: facebook_id
    });
};


});

var isajax = false;
$(document).on('click','.votenowin',function() {
if(isajax) return false;
isajax = true;
    var video_id = $(this).data('id');

    if(video_id==0){
        $.ajax({
            type: 'post',
            url: params.url + '/video/forget',
            success: function() {
            isajax  = false;
            	parent.$.fancybox.close();
            }
        });
    }else{
        $.ajax({
            url: params.url + '/video/vote',
            type: 'post',
            data: { name: $('#name').val(), email: $('#email').val(), nric: $('#nric').val(), sent_email: $('.voteCheckbox').val(), status: 0, video_id: video_id },
            dataType: 'json',
            success:function(res) {
            isajax  = false;
                if(res.status) {
                    parent.$('.numVote'+video_id).find('span').html(res.numVote);
                    parent.openPopup(params.url+'/video/vote/info/success/'+video_id);
                    
                }
                else {
                    if(res.isValidate) {
                        $('.formError').html(res.message);
                        parent.$('.fancybox-inner').height($('.form').height()+100);

                    }
                }
            }
        })

    }
});

$(document).on('click','.startVote',function() {

    var video_id = $(this).data('id');

    $.ajax({
        url: params.url + '/video/vote',
        type: 'post',
        data: { name: $('#name').val(), email: $('#email').val(), nric: $('#nric').val(), sent_email: $('.voteCheckbox').val(), status: 1, video_id: video_id },
        dataType: 'json',
        success:function(response) {
            if(response.status) {
                parent.$('.numVote'+video_id).find('span').html(response.numVote);
                //parent.openPopup(params.url+'/video/vote/info/success/'+video_id);
                parent.$.fancybox.close();
                
            }
            else {
                if(response.isValidate) {
                    $('.formError').html(response.message);
                    parent.$('.fancybox-inner').height($('.form').height()+100);
                }
            }
        }
    })
});

$(document).on('click','.shareVideo',function() {
    var video_id = $(this).data('id');
    var youtube_id = $(this).data('youtube');
    parent.shareFB(video_id,youtube_id);
});
$(document).on('click','.vote',function() {
    var video_id = $(this).data('id');
    $.ajax({
        url: params.url+'/video/checkvote',
        type: 'POST',
        data: { video_id: video_id },
        success: function(response) {
            var res = JSON.parse(response);
            if(typeof res.status != "undefined") {
                window.top.location.href =  res.url;
            } else {
                if (check_vote == 0 ) {
                    if (res.isvote > 0) {
                        parent.openPopup(params.url+'/video/vote/havevoted?video_id='+video_id);
                    } else {
                        if (res.isinfo == 0) {
                            parent.openPopup(params.url + '/video/vote/' + video_id+"?popup=true");

                        } else {
                            $.ajax({
                                url: params.url+'/video/postvotehaveinfo',
                                type: 'POST',
                                data: {video_id: video_id},

                                success: function(res1) {
                                    parent.$('.numVote'+ video_id).find('span').html(res1);
                                    parent.$('.numVote' + video_id).find('.btnVote').html("Voted");
                                    parent.openPopup(params.url+'/video/vote/success?video_id='+video_id);

                                }

                            });
                        }
                    }
                } else {
                    parent.openPopup(params.url+'/video/vote/error?video_id='+video_id);
                }
            }
        }
    });
});

$(document).on('click','.watch-video',function() {
    var video_id = $(this).data('id');
    parent.openPopup(params.url+"/video/"+video_id);
});
$(document).on('click','.close-popup',function() {
    parent.$.fancybox.close();
});

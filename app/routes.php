<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

App::error(function(Exception $exception, $code)
{
/*
    \Monster\Monster::HandlingError($exception,$code);
    if(!Config::get('app.debug')) {
        return View::make('errors/'.$code);
    }
    */
});

Route::get('redirect/', array('as'   =>  'link.redirect', function() {
    $page = Input::get("redirect");
    return Redirect::to($page);
}));
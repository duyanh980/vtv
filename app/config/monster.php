<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 2:04 PM
 */

return array(
    'avatar.default'        =>  'assets/images/no-image.png',
    'admin'                 =>  'admin',
    'permissionViewAdmin'   =>  'Website.Backend.Login'
);
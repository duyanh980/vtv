<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 11:14 PM
 */

return array(
    'modifiedOn'    =>  'Modified On',
    'modifiedBy'    =>  'Modified By',
    'publish'       =>  'Publish',
    'status'        =>  'Status',
    'save'          =>  'Save',
    'backToList'    =>  'Back To List',
    'edit'          =>  'Edit',
    'addNew'        =>  'Add New',
    'draft'         =>  'Draft',
    'pendingReview'=>  'Pending Review',
    'unknown'       =>  'Unknown',
    'latestUpdate'  =>  'Latest Update',
    'updateSuccess' =>  'Update Success',
    'order'         =>  'Order',
    'youDontHavePermissionToViewThatPage'  =>  'You don\'t have permission to view that page',
    'trash'         =>  'Trash',

    /*** USER ****/
    'firstName'     =>  'First Name',
    'lastName'      =>  'Last Name',
    'email'         =>  'Email',
    'lastLogin'     =>  'Last Login',
    'group'         =>  'Group',

    /** Settings **/
    'general'       =>  'General',
    'language'      =>  'Language',
    'mailAddress'   =>  'Mail Address',
    'facebook'      =>  'Facebook',
    'activity'      =>  'Activity'

);
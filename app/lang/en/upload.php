<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 5/4/14
 * Time: 1:51 PM
 */

return array(
    'upload_no_file_selected'  =>  'You did not select a file to upload.',
    'upload_invalid_filetype'  =>  'The file type you are attempting to upload is not allowed.',
    'upload_invalid_filesize'  =>  'The file you are attempting to upload is larger than the permitted size.',
    'upload_no_filepath'       =>  'The upload path does not appear to be valid.',
    'upload_not_writable'      =>  'The upload destination folder does not appear to be writable.',

);
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="https://plus.google.com/u/1/102919297309751807753/">
    <title>{{ System::item('web.title') }}</title>

<?php

    use Carbon\Carbon;

    if(\System::item('facebook.close')<=Carbon::now()) {
        $check_vote = 1;
    } else {
        $check_vote = 0;
    }
?>

    <script type="text/javascript">
        var config_fb = "<?php echo Config::get('facebook.url').'?sk=app_'.Config::get('facebook.id') ?>";
        var facebook_id ='<?php echo Config::get('facebook.id'); ?>';
        var check_vote ="{{$check_vote}}";
    </script>
    @include('facebook.section.headPopup')
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).load(function() {
                Cufon.replace('.cufon', { fontFamily: 'Futura'});
            });
            $(document).resize(function() {
                Cufon.replace('.cufon', { fontFamily: 'Futura'});
            });
        });
    </script>
<style>
body{

}

</style>
</head>
<body style="padding-bottom:20px;">
    @yield('content')
</body>
</html>

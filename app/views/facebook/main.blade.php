<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="https://plus.google.com/u/1/102919297309751807753/">
    <title>{{ System::item('web.title') }}</title>
    @include('facebook.section.head')

    <script type="text/javascript">
        $(document).ready(function () {
            $(window).load(function() {
                Cufon.replace('.cufon', { fontFamily: 'Futura'});
            });
            $(document).resize(function() {
                Cufon.replace('.cufon', { fontFamily: 'Futura'});
            });
            var contest_date = "{{ date('Y/m/d H:i:s',strtotime(System::item('facebook.close'))) }}";
            $('#clock').county({ endDateTime: new Date(contest_date), reflection: false, animation: 'show', speed : 1000, theme: 'black' });

            setInterval(
                function(){
                    if($(document).width() < 600){
                        $('.navigation').hide();
                        $('.show_menu_mobile').show();
                    } else {
                        $('.navigation').show();
                        $('.show_menu_mobile').hide();
                        $('.menu_header_mobile').hide();
                    }

                    if($(document).width() > 768){
                        $('.footer768').show();
                        $('.footer300768').hide();
                        $('.footer300').hide();
                    } else if ($(document).width() < 768 && $(document).width() > 300 ) {
                        $('.footer768').hide();
                        $('.footer300768').show();
                        $('.footer300').hide();
                    } else {
                        $('.footer768').hide();
                        $('.footer300768').hide();
                        $('.footer300').show();
                    }
                },100
            );


        });
    </script>
</head>

<body>
<?php
if($isMobile && !$facebook_id):
?>
<div class="btn-login-facebook">
    <a href="<?php echo $login_url; ?>">
        <img src="https://www.tinylightbulbs.com/assets/login-with-facebook-20a673c91b68a2e5d8973d8b8fb5a013.png" />
    </a>
</div>
<?php endif; ?>
<nav>
    <div class="navigation">
        <a target="_blank" href="http://www.nccc.gov.sg"> <img src="facebook/images/logo-header.png" class="banner"></a>
        <ul class="cufon">
            <li><a href="http://www.nccc.gov.sg" title="About NCCC" target="_blank">About NCCC</a></li>
            <li><a href="#" title="Share" class="navShare">Share</a></li>
            <li><a href="#" title="Terms & Conditions" class="terms-conditions">Terms & Conditions</a></li>
        </ul>
    </div>
    <div class="show_menu_mobile">
        <div class="logo-header">
            <img src="<?php echo url("facebook/images/logo-header.png"); ?>" >
        </div>
        <div class="navigation_mobile cufon">MENU</div>
    </div>
    <div class="menu_header_mobile">
        <ul class="cufon">
            <li><a href="http://app.nccs.gov.sg/competition2014/about.html" title="About NCCC" class="about">About NCCC</a></li>
            <li><a href="#" title="Share" class="navShare">Share</a></li>
            <li><a href="#" title="Terms & Conditions" class="terms-conditions">Terms & Conditions</a></li>
        </ul>
    </div>
</nav>
<header >
    <div class="pos-clock hidden-xs">
        <div class="text-vote"><span class="cufon">TIME LEFT TO VOTE</span><span style="font-family: Arial, Helvetica, sans-serif">!</span></div>
        <div id="clock"></div>
    </div>

    <img src="<?php echo url("facebook/images/banner.jpg"); ?>" class="banner">
</header>

<section class="container-fluid">
    <!-- page start-->
    @yield('content')
</section>
<footer class="container-fluid">
<div style="background: #FFFFFF;padding: 9px 23px;">
    <div class="row">
        <div class="col-xs-12 col-sm-5 custom-logo">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-text cufon">ORGANISED BY:</div>
                </div>
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-xs-12"><a href="http://app.nccs.gov.sg" target="_blank"><img src="<?php echo url("facebook/images/NCCS_Logo_01Aug_Highres_v1.png"); ?>" class="footer" border="0"></a></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-7">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-2"><div class="footer-text cufon">PARTNERS:</div></div>
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-xs-6 col-sm-offset-1 col-sm-5">
                    <img  src="<?php echo url("facebook/images/logo-moe.png"); ?>" class="footer" border="0">
                </div>
                <div class="col-xs-6 col-sm-6">
                    <img src="<?php echo url("facebook/images/NYAA-Logo-text.png"); ?>" class="footer" border="0">
                </div>
            </div>
        </div>

    </div>

    <div class="footer-text cufon text-center">SUPPORTING ORGANISATIONS:</div>
    <div class="row">
        <div class="col-xs-12 col-sm-5">
            <div class="row">
                <div class="col-xs-6 col-sm-6">
                    <img src="<?php echo url("facebook/images/nanyang.png"); ?>" class="footer" border="0">
                </div>
                <div class="col-xs-6 col-sm-6">
                    <img src="<?php echo url("facebook/images/footer-support3.png"); ?>" class="footer" border="0">
                </div>
            </div>

        </div>

        <div class="col-xs-12 col-sm-5">
            <div class="row">
                <div class="col-xs-6 col-sm-5">
                    <img src="<?php echo url("facebook/images/polytechnic.png"); ?>" class="footer" border="0">
                </div>
                <div class="col-xs-6 col-sm-6">
                    <img src="<?php echo url("facebook/images/sp.jpg"); ?>" class="footer" border="0">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2" style="padding:0px;">
            <div class="row">
                <div class="col-xs-12">
                    <img src="<?php echo url("facebook/images/temasek.png"); ?>" class="footer" border="0">
                </div>
            </div>
        </div>

    </div>
    <br/>
    <div class="copy cufon">
                <p>&copy; COPYRIGHT 2014 NCCS - NATIONAL CLIMATE CHANGE SECRETARIAT. ALL RIGHTS RESERVED.</p>
    </div>
    </div>
</footer>

</body>
</html>

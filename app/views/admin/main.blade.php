<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="https://plus.google.com/u/1/102919297309751807753/">
    <title>{{ System::item('web.title') }}</title>

    @include('admin.section.head')
</head>

<body>

<section id="container" >
    @include('admin.section.header')
    @include('admin.section.side')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    {{ Monster::breadcrumb() }}
                </div>
            </div>
            <!-- page start-->
            @yield('content')

        </section>
    </section>

    <!--@include('admin.section.right_sidebar')-->
</section>
@include('admin.section.footer')

</body>
</html>

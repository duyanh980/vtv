<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="duyanh980@gmail.com">
    <title></title>
    {{ HTML::style('assets/plugin/bootstrap/bootstrap.min.css') }}
    {{ HTML::style('backend/css/bootstrap-reset.css') }}
    {{ HTML::style('assets/plugin/font-awesome/font-awesome.css') }}
    {{ HTML::style('assets/plugin/dataTable/dataTable.css') }}
    {{ HTML::style('assets/plugin/dataTable/DT_bootstrap.css') }}
    {{ HTML::style('assets/plugin/bootstrap/bootstrap-fileupload.css') }}
    {{ HTML::style('assets/plugin/bootstrap/datepicker.css') }}
    {{ HTML::style('assets/plugin/jquery-validate/validationEngine.jquery.css') }}
    {{ HTML::style('backend/style.css') }}
    {{ HTML::style('backend/css/style-responsive.css') }}
    {{ HTML::style('backend/css/table-responsive.css') }}
    {{ HTML::style('backend/css/popup.css') }}
    <!--[if lt IE 9]>
    {{ HTML::script('backend/js/html5shiv.js') }}
    {{ HTML::script('backend/js/respond.min.js') }}
    <![endif]-->

    {{ HTML::script('backend/js/jquery.js') }}
    {{ HTML::script('backend/js/jquery-migrate-1.2.1.min.js') }}
    {{ HTML::script('assets/plugin/bootstrap/bootstrap.min.js') }}

</head>

<body>

@yield('content')

</body>
</html>

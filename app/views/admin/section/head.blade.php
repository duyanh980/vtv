<?php
Asset::container('header')->add('bootstrap-min', 'assets/plugin/bootstrap/bootstrap.min.css');
Asset::container('header')->add('bootstrap-reset', 'backend/css/bootstrap-reset.css');
Asset::container('header')->add('font-awesome', 'assets/plugin/font-awesome/font-awesome.css');
Asset::container('header')->add('dataTable', 'assets/plugin/dataTable/dataTable.css');
Asset::container('header')->add('Bootstrap-DataTable.css', 'assets/plugin/dataTable/DT_bootstrap.css');
Asset::container('header')->add('Bootstrap-FileUpload', 'assets/plugin/bootstrap/bootstrap-fileupload.css');
Asset::container('header')->add('DatePicker', 'assets/plugin/bootstrap/datepicker.css');
Asset::container('header')->add('validationEngine', 'assets/plugin/jquery-validate/validationEngine.jquery.css');
Asset::container('header')->add('style', 'backend/style.css');
Asset::container('header')->add('style-ad', 'backend/style_ad.css');
Asset::container('header')->add('style-responsive', 'backend/css/style-responsive.css');
Asset::container('header')->add('table-responsive', 'backend/css/table-responsive.css');
Asset::container('header')->add('magnific-popup', 'assets/plugin/magnific/magnific-popup.css');
Asset::container('header')->add('jquery-ui', 'backend/css/jquery-ui.css');
Asset::container('header')->add('datetimepicker.css', 'assets/plugin/datetimepicker/bt.datetimepicker.css');
Asset::container('header')->add('timepicker.css', 'assets/plugin/bootstrap-timepicker/timepicker.css');

Asset::container('header')->add('html5shiv', 'backend/js/html5shiv.js');
Asset::container('header')->add('respond', 'backend/js/respond.min.js');

Asset::container('header')->add('jquery', 'backend/js/jquery.js');
Asset::container('header')->add('jquery-migrate-1.2.1.min', 'backend/js/jquery-migrate-1.2.1.min.js');
Asset::container('header')->add('bootstrap', 'assets/plugin/bootstrap/bootstrap.min.js');
Asset::container('header')->add('jquery-ui', 'backend/js/jquery-ui.js');
Asset::container('header')->add('CKEditor','assets/ckeditor/ckeditor.js?src='.url());
if(Auth::check()) {
    Asset::container('footer')->add('jquery.dcjqaccordion.2.7.js', 'assets/plugin/accordion-menu/jquery.dcjqaccordion.2.7.js');
    Asset::container('footer')->add('jquery.scrollTomin.js', 'assets/plugin/scrollTo/jquery.scrollTo.min.js');
    Asset::container('footer')->add('jquery.nicescroll.js', 'assets/plugin/nicescroll/jquery.nicescroll.js');
    Asset::container('footer')->add('jquery.dataTables.js', 'assets/plugin/dataTable/jquery.dataTables.js');
    Asset::container('footer')->add('jquery.sortable.js', 'assets/plugin/dataTable/jquery.sortable.js');
    Asset::container('footer')->add('TableTools.js', 'assets/plugin/dataTable/TableTools.js');
    Asset::container('footer')->add('DT_bootstrap.js', 'assets/plugin/dataTable/DT_bootstrap.js');
    Asset::container('footer')->add('bootstrap-fileupload.js', 'assets/plugin/bootstrap/bootstrap-fileupload.js');
    Asset::container('footer')->add('bootstrap-datepicker.js', 'assets/plugin/bootstrap/bootstrap-datepicker.js');
    Asset::container('footer')->add('bootstrap-inputmask.js', 'assets/plugin/inputmask/bootstrap-inputmask.min.js');
    Asset::container('footer')->add('jquery.validationEngine.js', 'assets/plugin/jquery-validate/jquery.validationEngine.js');
    Asset::container('footer')->add('jquery.validationEngine-en.js', 'assets/plugin/jquery-validate/jquery.validationEngine-en.js');
    Asset::container('footer')->add('jquery.magnific-popup.js', 'assets/plugin/magnific/jquery.magnific-popup.js');
    Asset::container('header')->add('datetimepicker.js', 'assets/plugin/datetimepicker/bt.datetimepicker.js');
    Asset::container('header')->add('timepicker.js', 'assets/plugin/bootstrap-timepicker/bootstrap-timepicker.js');
    Asset::container('footer')->add('scripts.js', 'backend/js/scripts.js');
}

echo Asset::container('header')->styles();
echo Asset::container('header')->scripts();

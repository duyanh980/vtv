<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="duyanh980@gmail.com">
    <title></title>
    @include('admin.section.head')
</head>


<body class="login-body">
<div class="container">
@yield('content')
</div>
</body>
</html>
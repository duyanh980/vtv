<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="https://plus.google.com/u/1/102919297309751807753/">
    <title>{{ System::item('web.title') }}</title>
    @include('facebook.section.head')
</head>

<body>
<section id="container" >
    <!-- page start-->
    @yield('content')
</section>
</body>
</html>

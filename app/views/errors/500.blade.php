<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="duyanh980@gmail.com">
    <title>404</title>

    {{ HTML::style('backend/css/bootstrap-reset.css') }}
    {{ HTML::style('assets/plugin/font-awesome/font-awesome.css') }}

    {{ HTML::style('backend/style.css') }}
    {{ HTML::style('backend/css/style-responsive.css') }}

    <!--[if lt IE 9]>
    {{ HTML::script('backend/js/html5shiv.js') }}
    {{ HTML::script('backend/js/respond.min.js') }}
    <![endif]-->
</head>
<body class="body-500">

<div class="error-head"> </div>

<div class="container ">

    <section class="error-wrapper text-center">
        <h1><img src="{{ url('assets/images/500.png') }}" alt=""></h1>
        <div class="error-desk">
            <h2>OOOPS!!!</h2>
            <p class="nrml-txt-alt">Something went wrong.</p>
            <p>Why not try refreshing you page? Or you can <a href="http://jupitech.sg" class="sp-link">contact our support</a> if the problem persists.</p>
        </div>
        <a href="{{ url() }}" class="back-btn"><i class="fa fa-home"></i> Back To Home</a>
    </section>

</div>


</body>

</html>

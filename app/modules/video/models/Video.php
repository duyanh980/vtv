<?php namespace App\Modules\Video\Models;
use Carbon\Carbon;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 9:27 AM
 */

class Video extends \Eloquent {

    protected $table = 'videos';
    protected $primaryKey = 'video_id';
    protected $fillable = array('video_name','video_category_id','video_author','video_school','video_url','video_status');

    public static $rules = array(
        'video_name'      =>    'required|max:250',
        'video_author'    =>    'required|max:50',
        'video_school'    =>    'max:50',
        'video_url'       =>    'required|max:250'
    );

    public static function getAll($fid = false) {
        $data = array();
        $categories = Category::where('video_category_status',1)->get();
        if(!$categories->isEmpty()) {
            foreach($categories as $key => $category) {
                $data[$key] = new \stdClass();
                $data[$key]->name = $category->video_category_name;
                switch($key%4) {
                    case 0:
                        $data[$key]->style = 'orange';
                        break;
                    case 1:
                        $data[$key]->style = 'green';
                        break;
                    case 2:
                        $data[$key]->style = 'pink';
                        break;
                    case 3:
                        $data[$key]->style = 'brown';
                        break;
                }

                $videos = self::where('video_status',1)->where('video_category_id',$category->video_category_id)->get();
                if(!$videos->isEmpty()) {
                    foreach($videos as $kVideo => $video) {
                        $data[$key]->videos[$kVideo]                =   new \stdClass();
                        $data[$key]->videos[$kVideo]->id            =   $video->video_id;
                        $data[$key]->videos[$kVideo]->category_id   =   $video->video_category_id;
                        $data[$key]->videos[$kVideo]->name          =   $video->video_name;
                        $data[$key]->videos[$kVideo]->youtube_id    =   $video->video_youtube_id;
                        $data[$key]->videos[$kVideo]->author        =   $video->video_author;
                        $data[$key]->videos[$kVideo]->school        =   $video->video_school;
                        $data[$key]->videos[$kVideo]->numVote       =   Vote::where('video_id',$video->video_id)->count();
                        $currentTime = Carbon::now()->toDateString();
                        $hasVote = Vote::where("video_id",$video->video_id)->where("facebook_id",$fid)->whereRaw('date(date_created) = "'.$currentTime.'"')->first();
                        if($fid && isset($hasVote->exists)) {
                            $data[$key]->videos[$kVideo]->voted = true;
                        } else {
                            $data[$key]->videos[$kVideo]->voted = false;
                        }
                        $style = null;
                        switch($kVideo) {
                            case 0  :
                                $style  =   'first';
                                break;
                            case 1  :
                                $style  =   'bold';
                                break;
                            case 2  :
                                $style  =   'bold';
                                break;
                            case 3  :
                                $style  =   'bold';
                                break;
                            case 4  :
                                $style  =   'last';
                                break;
                        }
                        if(\System::item('facebook.winner.id')==$video->video_id) {
                            $style.= ' winner';
                        }
                        $data[$key]->videos[$kVideo]->style         =   $style;
                    }
                }
            }
        }
        return $data;
    }

    public static function strKeyword($keyword) {
        $data = array();
        if(!empty($keyword)) {
            foreach ($keyword as $item) {
                $data[] = $item->video_name;
            }
            $keyword_str = implode( "','" , $data);

            return $keyword_str;
        }
    }

    public static function arrVideo($array) {
        $data = array();
        if(!empty($array)) {
            foreach ($array as $item) {
                $data[$item->video_name] = $item->video_id;
            }

            return $data;
        }
    }
}
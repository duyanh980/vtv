<?php namespace App\Modules\Video\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 10:45 AM
 */

class Vote extends \Eloquent {

    protected $table = 'video_votes';
    protected $dates = array('date_created');

    public $timestamps = false;

    public static $rules = array(
        'video_id'          =>    'required|max:11',
        'facebook_id'       =>    'required|max:25',
    );


}
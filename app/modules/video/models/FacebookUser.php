<?php namespace App\Modules\Video\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 10:45 AM
 */

class FacebookUser extends \Eloquent {

    protected $table = 'video_facebook';
    protected $primaryKey = 'facebook_id';
    protected $fillable = array('facebook_id','name','email','nric','sent_email', 'status');
    public static $rules = array(
        'name'              =>    'required|max:250',
        'email'             =>    'required|max:250|email',
        'nric'              =>    'required|max:250',
    );

}
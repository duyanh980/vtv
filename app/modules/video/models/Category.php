<?php namespace App\Modules\Video\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 9:27 AM
 */



class Category extends \Eloquent {

    protected $table = 'video_categories';
    protected $primaryKey = 'video_category_id';
    protected $fillable = array('video_category_name','video_category_status');

    public static $rules = array(
        'video_category_name'      =>    'required|max:250',
    );


}
<?php namespace App\Modules\Video\Models;
/**
 * Created by PhpStorm.
 * User: DuyAnk
 * Date: 9/1/14
 * Time: 10:09 PM
 */

class FacebookLogged extends \Eloquent {

    protected $table = 'facebook_logged';
    protected $primaryKey = 'facebook_id';
    protected $fillable = array('facebook_id','logged_on');

    public $timestamps = false;

}
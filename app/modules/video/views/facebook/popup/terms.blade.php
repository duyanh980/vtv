@extends('facebook.popup')

@section('content')
<style type="text/css">
    html, body {
        overflow-y: auto!important;
        overflow-x:hidden;
    }
</style>
<section class="formTerm">
    <div class="row text-center">
        <img src="{{url('facebook/images/big-logo-popup.jpg')}}" />
    </div>
    <div class="message cufon" style="overflow-y: auto;padding:30px;">{{$message}}</div>

    <br>&nbsp;
</section>

@stop

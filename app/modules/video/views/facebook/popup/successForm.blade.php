@extends('facebook.popup')

@section('content')
<section class="formThankYou" style="min-height: 354px;">
    <div class="row text-center">
        <img src="{{url('facebook/images/big-logo-popup.jpg')}}" style="height: 209px;" />
    </div>
    <div class="message text-center uppercase cufon">{{$message}}</div>
    <br>&nbsp;
    <div class="row">
        {{--<div class="col-xs-6">--}}
            {{--<button class="btn watch-video cufon" data-id="{{Input::get('video_id')}}">Watch Again</button>--}}
        {{--</div>--}}
        <div class="col-xs-12 text-center">
            <button class="btn close-popup cufon">See Other Videos</button>
        </div>
    </div>
    <br>&nbsp;
</section>
<script>
    $(document).ready(function(){
        var height = $('.formThankYou').height();
        setTimeout(function(){
                parent.$('.fancybox-inner').height(height);

            },1000)
        });
</script>
@stop
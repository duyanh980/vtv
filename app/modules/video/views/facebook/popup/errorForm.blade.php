@extends('facebook.popup')

@section('content')
<section class="formThankYou">
    <div class="row text-center">
        <img src="{{url('facebook/images/big-logo-popup.jpg')}}" style="height: 209px;" />
    </div>
    <div class="message text-center messageError cufon">{{$message}}</div>
    @if ($check != 1)
    <div class="row">
        <div class="col-xs-12 text-center">
            <button class="btn watch-video cufon" data-id="{{Input::get('video_id')}}">Watch Video</button>
        </div>
    </div>
    @endif
    <br>&nbsp;
</section>
<script>
    $(document).ready(function(){
        var height = $('.formThankYou').height();
        setTimeout(function(){
                parent.$('.fancybox-inner').height(height);

            },1000)
        });
</script>
@stop

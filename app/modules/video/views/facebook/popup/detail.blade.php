@extends('facebook.popup')
@section('content')
<style>
    .mfp-content {
        padding: 12px;
    }
</style>
<div style="padding:10px">
    <div  class="player" >
        <iframe width="770" height="450"
                src="https://www.youtube.com/embed/{{$video->video_youtube_id}}" frameborder="0" allowfullscreen></iframe>
    </div>

    <div  class="video_detail">
        <div class="row" style="margin:0px;">
        @if ($check_vote === 1)
        <div class="col-xs-12 col-sm-10">
        @else
        <div class="col-xs-12 col-sm-8">
        @endif
            <div class="video_name cufon">{{$video->video_name}} | {{$video->video_author}}</div>
            <div class="video_school cufon">{{$video->video_school}}</div>
        </div>
        <div class="col-xs-12 col-sm-2 detail-video">
            <div class="btn shareVideo cufon" data-id="{{$video->video_id}}" data-youtube="{{$video->video_youtube_id}}">Share</div>
        </div>
        @if ($check_vote === 0)
        <div class="col-xs-12 col-sm-2 detail-video">
            <div class="btn vote cufon" data-id="{{$video->video_id}}">Vote</div>
        </div>
        @endif
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.player').find('.mfp-close').css('display','none');
    });
</script>
@stop

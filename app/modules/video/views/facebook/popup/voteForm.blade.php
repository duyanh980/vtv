@extends('facebook.popup')

@section('content')
<section class="form">
        <div class="row">
            <div class="col-xs-12 col-sm-4"><img src="{{url('facebook/images/logo-popup.jpg')}}" style="min-height: 113px;" /></div>
            <div style="text-transform: uppercase;" class="col-xs-12 col-sm-8 popup-text">
                <span class="cufon">Stand a chance to win<br /> a 2D1N stay at Parkroyal on Pickering
                <p class="note">Fill in your details to enter</p></span>
            </div>
        </div>
    <div class="formError"></div>
    <div class="form-body">
        <div class="row">
            <div class="col-xs-12 col-sm-7 formUser">
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><label class="cufon text-passport">Name</label></div>
                    <div class="col-xs-12 col-sm-8">
                        <input type="text" name="name"  id="name" class="form-control" />
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><label class="cufon text-passport">Email</label></div>
                    <div class="col-xs-12 col-sm-8">
                        <input type="text" name="email" id="email" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4"><label class="cufon text-passport">NRIC/Passport No.</label></div>
                    <div class="col-xs-12 col-sm-8">
                        <input name="nric" id="nric" class="form-control" />
                    </div>
                </div>
            </div>

            <div class="formUser col-xs-12 col-sm-5">
                <div class="row submit-vote">
                    <button class="startVote cufon" @if (isset($video_id)) data-id="{{ $video_id }}" @else data-id="{{Request::segment(3)}}" @endif >Submit and<br>Start Voting</button>
                </div>
                <div class="row votenowin cufon" @if (isset($video_id)) data-id="{{ $video_id }}" @else data-id="{{Request::segment(3)}}" @endif >
                    <img src="<?php echo url("facebook/images/nothank.png"); ?>" style="max-width: 100%;" />
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12">
                {{Form::checkbox('sent_email', '1', true,  array('class' => 'voteCheckbox'))}} <span class="cufon">SIGN UP FOR THE  CLIMATE CHALLENGE E-NEWSLETTER</span>
            </div>
        </div>
    </div>
</section>
<?php if(Input::get('popup')==true): ?>
<script>

//console.log(1);
    $(document).ready(function(){
    var height = $('.form').height();
    setTimeout(function(){
            parent.$('.fancybox-inner').height(height+100);

        },1000)
    });
</script>
<?php endif ;?>
@stop
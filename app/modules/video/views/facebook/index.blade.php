@extends('facebook.main')

@section('content')

<div id="fb-root"></div>
@if(!empty($videos))
@foreach($videos as $category)
<section class="box {{$category->style}}">
    <header  class="box-heading cufon">{{$category->name}}</header>
    <div class="box-body">
        @if(!empty($category->videos))
        <ul>
            @foreach($category->videos as $video)
            <li class="video {{$video->style}}">

                <a  data-fancybox-type="iframe" href="{{route('facebook.video.detail',array($video->id))}}" class="open-popup"><img src="https://img.youtube.com/vi/{{$video->youtube_id}}/hqdefault.jpg" /></a>
                <div class="text-description">
                    <p class="video_name height_{{str_replace(' ','_',$category->name)}} cufon">{{$video->name}}</p>
                    <p class="author cufon">{{$video->author}}</p>
                    <?php if($video->category_id !='4'):?>
                    <p class="school cufon">{{$video->school}}</p>
                </div>
                <?php endif?>
                <div class="action numVote{{$video->id}}">

                    <?php if(strlen($video->numVote) >2) {
                        $style_custom="style='width:100%'";
                    }else{
                        $style_custom="style='width:100%'";
                    };?>
                    <div <?php echo $style_custom?>  class="number"><i class="fa fa-heart"></i><span class="cufon"><?php echo $video->numVote; ?></span></div>
                    <div <?php echo $style_custom?>  class="btnVote">
                        <?php if($video->voted): ?>
                            Voted
                        <?php else: ?>
                            Vote
                        <?php endif; ?>
                    </div>
                    <input type="hidden" value="{{$video->id}}" class="vote_video_id"/>
                </div>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</section>
@endforeach
@endif
<script type="text/javascript">
var pageInfo;
$(document).ready(function() {
    var setHeightBox = function() {
        var height = 0;
        $('.box-body').each(function(){
            $(this).find('.video').each(function(){
                var currentHeight = $(this).find('.text-description').height();
                if(height < currentHeight){
                    height = currentHeight;
                }
            });
            $(this).find('.text-description').height(height);
        });
    }
    $(window).load(function() {
        setHeightBox();
    });


    $(document).on('click','.voteCheckbox',function() {
        if($('.voteCheckbox').is(':checked') ) {
            $('.voteCheckbox').val(1);
        } else {
            $('.voteCheckbox').val(0);
        }
    });
    if($('li').hasClass('winner')){
        var html = '<div class="medal"></div>';
        $('.box-body').find('.winner').append(html);
    }

    $('.navigation_mobile').click(function(e){
        e.stopPropagation();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.menu_header_mobile').slideUp(1000);
        }else{
            $(this).addClass('active');
            $('.menu_header_mobile').slideDown(1000);
        }
    });


});


var check_vote = {{$check_vote}};

var isinfo = {{$isinfo}};
var isMobile = <?php echo ($isMobile) ?  "true" : "false"; ?>;

var pageInfo_fb ='';
window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
        appId      : '<?php echo Config::get('facebook.id'); ?>',                        // App ID from the app dashboard
        status     : true,                                 // Check Facebook Login status
        xfbml      : true                                  // Look for social plugins on the page
    });
    FB.Canvas.setAutoGrow();

    var positionDialog = function(dialogHeight){
        FB.Canvas.getPageInfo(function(pageInfo){
            $('.fancybox-wrap').css({top:Math.max(parseInt(pageInfo.scrollTop) - parseInt(pageInfo.offsetTop) +
                ((parseInt(pageInfo.clientHeight)-dialogHeight)/2), 10)});
        });
    };

    $(document).on('click','.shareVideo',function() {
        var video_id = $(this).data('id');
        var youtube_id = $(this).data('youtube');
        $.ajax({
            url: '{{route("facebook.video.share.info")}}?video_id='+video_id,
            type: 'get',
            success: function(response) {
                var res = JSON.parse(response);
                FB.ui({
                    method: 'feed',
                    link: ''+video_id,
                    picture: 'http://img.youtube.com/vi/' + youtube_id + '/hqdefault.jpg',
                    name: res.video_category_name+ ': '+ res.video_name,
                    description: 'I\'ve casted my daily vote for '+ res.video_name + '! Show your support by casting your vote too!',
                    caption: '{{System::item("facebook.caption")}}'
                });
                $('.fancybox-wrap').css({top:10});
            }
        });
    });


    $('.navShare').click(function() {
        FB.ui({
            method: 'feed',
            link: '<?php echo url("video/redirect"); ?>',
            picture: '<?php echo url("facebook/images/thumnail.jpg"); ?>',
            description: '{{System::item("facebook.message")}}',
            name: '{{System::item("facebook.name")}}',
            caption: '{{System::item("facebook.caption")}}'
        });
    });
//todo
    setTimeout(function() {
        <?php if($app_data): ?>
        $.fancybox.open({
            padding : 0,
            width:"100%",
            href: '<?php echo route("facebook.video.detail",array($app_data)); ?>',
            type: 'iframe',
            topRatio:0,
            afterLoad:function(){
                setTimeout(function(){
                    var popupHeight = $('#custom_fancybox').height();
                    var marginTop = 0;
                    var paddingTop = 0;
                    if(!isMobile) {
                        positionDialog(popupHeight);
                    }

                },1000);
            }
        });
        <?php else: ?>
        if (check_vote == 0 ) {
            if (isinfo == 0) {
                $.fancybox.open({
                    padding : 0,
                    width:"100%",
                    href: '{{route("facebook.video.vote.form")}}?video_id=0',
                    type: 'iframe',
                    topRatio:0,
                    closeBtnAjax: true,
                    afterLoad:function(){
                        setTimeout(function(){
                            var popupHeight = $('#custom_fancybox').height();
                            var marginTop = 0;
                            var paddingTop = 0;
                            if(!isMobile) {
                                positionDialog(popupHeight);
                            }

                        },1000);
                    }
                });
            }
        }
        <?php endif; ?>
    },1000);

    (function ($) {
        "use strict";


        $(document).ready(function () {

            $(document).on("click",".fancybox-close-ajax",function() {
                $.ajax({
                    type: 'post',
                    url: '<?php echo route("facebook.video.forget"); ?>'
                });
            });

            var requestAjax = false;
            $(document).on('click','.btnVote',function() {
                if(requestAjax) {
                    return false;
                }
                requestAjax = true;
                var info = $(this).offset();
                var video_id = $(this).parents('.action').find('.vote_video_id').val();
                $.ajax({
                    url: '{{route("facebook.video.vote.check")}}',
                    type: 'POST',
                    data: { video_id: video_id },
                    success: function(response) {
                        var res = JSON.parse(response);
                        if(typeof res.status != "undefined") {
                            window.top.location.href =  res.url;
                        } else {
                            if (check_vote == 0 ) {
                                if (res.isvote > 0) {

                                    requestAjax = false;
                                    $.fancybox.open({
                                        padding : 0,width:"100%",
                                        href:'{{route("facebook.video.vote.havevoted")}}?video_id='+video_id,
                                        type: 'iframe',
                                        autoResize: true,
                                        afterLoad:function(){
                                            setTimeout(function(){
                                                var popupHeight = $('#custom_fancybox').height();
                                                var marginTop = 0;
                                                var paddingTop = 0;
                                                if(!isMobile) {
                                                    positionDialog(popupHeight);
                                                }
                                            },1000);
                                        }

                                    });

                                } else {
                                    if (res.isinfo == 0) {
                                        requestAjax = false;
                                        $.fancybox.open({
                                            padding : 0,width:"100%",
                                            href:'{{route("facebook.video.vote.form")}}/'+video_id,
                                            type: 'iframe',
                                            autoResize: true,
                                            afterLoad:function(){
                                                setTimeout(function(){
                                                    var popupHeight = $('#custom_fancybox').height();
                                                    var marginTop = 0;
                                                    var paddingTop = 0;
                                                    if(!isMobile) {
                                                        positionDialog(popupHeight);
                                                    }
                                                },1000);
                                            }
                                        });
                                    } else {
                                        $.ajax({
                                            url: '{{route("facebook.video.post.vote.have.info")}}',
                                            type: 'POST',
                                            data: {video_id: video_id, facebook_id : '{{$facebook_id}}'},
                                            success: function(res1) {

                                                $('.numVote'+res1.video_id).find('span').html(res1.numVote);

                                                Cufon.replace('.cufon', { fontFamily: 'Futura'});
                                                requestAjax = false;
                                                $.fancybox.open({
                                                    padding : 0,width:"100%",
                                                    href:'<?php echo route('facebook.video.vote.success'); ?>?video_id='+video_id,
                                                    type: 'iframe',
                                                    afterLoad:function(){
                                                        setTimeout(function(){
                                                            var popupHeight = $('#custom_fancybox').height();
                                                            var marginTop = 0;
                                                            var paddingTop = 0;
                                                            if(!isMobile) {
                                                                positionDialog(popupHeight);
                                                            }
                                                        },1000);
                                                    }
                                                });

                                                $('.numVote' + video_id).find('span').text(res1);
                                                $('.numVote' + video_id).find('.btnVote').html("Voted");
                                            }

                                        });
                                    }
                                }
                            } else {
                                requestAjax = false;
                                $.fancybox.open({
                                    padding : 0,width:"100%",
                                    href:'{{ route("facebook.video.vote.error")}}?video_id='+video_id,
                                    type: 'iframe',
                                    afterLoad:function(){
                                        setTimeout(function(){
                                            var popupHeight = $('#custom_fancybox').height();
                                            var marginTop = 0;
                                            var paddingTop = 0;
                                            if(!isMobile) {
                                                positionDialog(popupHeight);
                                            }

                                        },1000);
                                    }
                                });
                            }
                        }
                    }
                });
            });

//            $('.open-popup').magnificPopup({
//                type: 'ajax',
//                overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
//            });
            $(".open-popup").fancybox({
                width:'100%',
                fitToView	: false,
                autoSize	: false,
                closeClick	: false,
                openEffect	: 'none',
                closeEffect	: 'none',
                afterLoad:function(){
                    setTimeout(function(){
                        var popupHeight = $('#custom_fancybox').height();
                        var marginTop = 0;
                        var paddingTop = 0;
                        if(!isMobile) {
                            positionDialog(popupHeight);
                        }
                    },1000);
                }
            });

            $(document).on('click','.close-popup',function() {
                $.magnificPopup.close();
            });

            $(document).on('click','.vote',function() {
                var video_id = $(this).data('id');
                $.ajax({
                    url: '{{route("facebook.video.vote.check")}}',
                    type: 'POST',
                    data: { video_id: video_id },
                    success: function(response) {
                        var res = JSON.parse(response);
                        if (check_vote == 0 ) {
                            if (res.isvote > 0) {

                                $.fancybox.open({
                                    padding : 0,width:"100%",
                                    href:'{{route("facebook.video.vote.havevoted")}}?video_id='+video_id,
                                    type: 'iframe',
                                    afterLoad:function(){
                                        setTimeout(function(){
                                            var popupHeight = $('#custom_fancybox').height();
                                            var marginTop = 0;
                                            var paddingTop = 0;
                                            if(!isMobile) {
                                                positionDialog(popupHeight);
                                            }

                                        },1000);
                                    }
                                });

                            } else {
                                if (res.isinfo == 0) {


                                    $.fancybox.open({
                                        padding : 0,width:"100%",
                                        href:'{{route("facebook.video.vote.form")}}/'+video_id,
                                        type: 'iframe',
                                        afterLoad:function(){
                                            setTimeout(function(){
                                                var popupHeight = $('#custom_fancybox').height();
                                                var marginTop = 0;
                                                var paddingTop = 0;
                                                if(!isMobile) {
                                                    positionDialog(popupHeight);
                                                }
                                            },1000);
                                        }
                                    });
                                } else {
                                    $.ajax({
                                        url: '{{route("facebook.video.post.vote.have.info")}}',
                                        type: 'POST',
                                        data: {video_id: video_id, facebook_id : '{{$facebook_id}}'},
                                        success: function(res1) {


                                            $.fancybox.open({
                                                padding : 0,width:"100%",
                                                href:'<?php echo route('facebook.video.vote.success'); ?>?video_id='+video_id,
                                                type: 'iframe',
                                                afterLoad:function(){
                                                    setTimeout(function(){
                                                        var popupHeight = $('#custom_fancybox').height();
                                                        var marginTop = 0;
                                                        var paddingTop = 0;
                                                        if(!isMobile) {
                                                            positionDialog(popupHeight);
                                                        }

                                                    },1000);
                                                }
                                            });

                                            $('.numVote' + video_id).find('span').text(res1);
                                            $('.numVote' + video_id).find('.btnVote').html("Voted");
                                        }

                                    });
                                }
                            }
                        } else {
                            $.fancybox.open({
                                padding : 0,width:"100%",
                                href:'{{ route("facebook.video.vote.error")}}?video_id='+video_id,
                                type: 'iframe'
                            });

                        }
                    }
                });
            });

            $(document).on('click','.about',function() {

                $.fancybox.open({
                    padding : 0,width:"100%",
                    href:'{{route("facebook.video.vote.about")}}',
                    type: 'iframe',

                    afterLoad:function(){

                        setTimeout(function(){
                            var popupHeight = $('#custom_fancybox').height();
                            var marginTop = 0;
                            var paddingTop = 0;
                            if(pageInfo.viewportTop>=101) {
                                marginTop = parseInt(pageInfo.viewportTop - 101);
                            }
                            paddingTop = parseInt(parseInt(pageInfo.viewportHeight - popupHeight)/2 + marginTop);
                            if(paddingTop<=100) {
                                paddingTop = 100;
                            }
                            if(parseInt(paddingTop+popupHeight) >= pageInfo.pageHeight - 50) {
                                paddingTop = pageInfo.pageHeight - popupHeight - 50;
                            }
                            $('.fancybox-wrap').css({top:paddingTop})
                        },1000)
                    }
                });

            });

            $(document).on('click','.terms-conditions',function() {

                $.fancybox.open({
                    padding : 0,width:"100%",
                    href:'{{route("facebook.video.vote.terms")}}',
                    type: 'iframe',
                    afterLoad:function(){
                        var paddingTop = 10;
                        $('.fancybox-wrap').css({top:paddingTop});
                    }
                });
            });


            $(document).on('click','.votenowin',function() {
                var video_id = $(this).data('id');
                $.ajax({
                    url: '{{route("facebook.video.vote")}}',
                    type: 'post',
                    data: { name: $('#name').val(), email: $('#email').val(), nric: $('#nric').val(), sent_email: $('.voteCheckbox').val(), status: 0, video_id: video_id },
                    dataType: 'json',
                    success:function(res) {
                        if(res.status) {
                            $.fancybox.open({
                                padding : 0,width:"100%",
                                href:'{{ route("facebook.video.info.success")}}/'+video_id,
                                type: 'iframe',

                                afterLoad:function(){
                                    setTimeout(function(){
                                        var popupHeight = $('#custom_fancybox').height();
                                        var marginTop = 0;
                                        var paddingTop = 0;
                                        if(!isMobile) {
                                            positionDialog(popupHeight);
                                        }

                                    },1000);
                                }
                            });

                            $('.numVote'+video_id).find('span').html(res.numVote);
                            Cufon.replace('.cufon', { fontFamily: 'Futura'});
                        }
                        else {
                            if(res.isValidate) {
                                $('.formError').html(res.message);
                                Cufon.replace('.cufon', { fontFamily: 'Futura'});
                            }
                        }
                    }
                })
            });

            $(document).on('click','.startVote',function() {

                var video_id = $(this).data('id');
                $.ajax({
                    url: '{{route("facebook.video.vote")}}',
                    type: 'post',
                    data: { name: $('#name').val(), email: $('#email').val(), nric: $('#nric').val(), sent_email: $('.voteCheckbox').val(), status: 1, video_id: video_id },
                    dataType: 'json',
                    success:function(res) {
                        if(res.status) {
                            $.fancybox.open({
                                padding : 0,width:"100%",
                                href:'{{ route("facebook.video.info.success")}}/'+video_id,
                                type: 'iframe'
                            });
                            $('.numVote'+video_id).find('span').html(res.numVote);
                            Cufon.replace('.cufon', { fontFamily: 'Futura'});
                        }
                        else {
                            if(res.isValidate) {
                                $('.formError').html(res.message);
                                Cufon.replace('.cufon', { fontFamily: 'Futura'});
                            }
                        }
                    }
                })
            });

            $(document).on('click','.watch-video',function() {
                var video_id = $(this).data('id');
                $.fancybox.open({
                    padding : 0,width:"100%",
                    href:'<?php echo route('facebook.video.detail'); ?>/'+video_id,
                    type: 'iframe'
                });

            });
        });

    })(jQuery);


};





</script>
<script type="text/javascript" src="<?php echo url("facebook/js/fb-scroll.js"); ?>"></script>
<script type="text/javascript">
    // Load the SDK asynchronously
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(document).ready(function() {
        $(document).on('fb-scroll', function(evt, info){
            pageInfo = info;
        });
    });

    function shareFB(video_id, youtube_id) {
        $.ajax({
            url: '<?php echo url('video/share/?video_id='); ?>'+video_id,
            type: 'get',
            success: function(response) {
                var res = JSON.parse(response);
                FB.ui({
                    method: 'feed',
                    link: '<?php echo url("video/redirect"); ?>?app_data='+video_id,
                    picture: 'http://img.youtube.com/vi/' + youtube_id + '/hqdefault.jpg',
                    message: res.video_category_name+ ': '+ res.video_name,
                    name: res.video_category_name+ ': '+ res.video_name,
                    description: 'I\'ve casted my daily vote for '+ res.video_name + '! Show your support by casting your vote too and stand to win a 2D1N staycation!',
                    caption: '{{System::item("facebook.caption")}}'
                },function(response) {
                    if (response && !response.error_code) {
                        $.ajax({
                            url: '<?php echo url('video/share/'); ?>',
                            type: 'post',
                            data: { video_id: video_id }
                        })
                    }
                });

                if(isMobile) {
                    $('.fancybox-wrap').css({top:10});
                }
            }
        });
    }
    var openPopup = function(url) {
        $.fancybox.close();
        $.fancybox.open({
            padding : 0,width:"100%",
            href: url,
            type: 'iframe',
            afterLoad:function(){
                setTimeout(function(){
                    var popupHeight = $('#custom_fancybox').height();
                    var marginTop = 0;
                    var paddingTop = 0;
                    if(!isMobile) {
                        FB.Canvas.getPageInfo(function(pageInfo){
                            $('.fancybox-wrap').css({top:Math.max(parseInt(pageInfo.scrollTop) - parseInt(pageInfo.offsetTop) +
                                ((parseInt(pageInfo.clientHeight)-popupHeight)/2), 10)});
                        });
                    }

                },1000);
            }
        });
    }
</script>
@stop
@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.video.category.update',array((isset($category) ? $category->video_category_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('video::monster.moduleCategoryManager') }}
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('video::monster.videoCategory') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('video_category_name',Input::old('video_category_name', isset($category) ? $category->video_category_name : '' ), array('id' => 'video_category_name', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.activity') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-xs-12"><span class="pull-left">{{ Lang::get('monster.status') }}</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('video_category_status',array('0'=>Lang::get('monster.draft'), '2' => Lang::get('monster.pendingReview'), '1'=> Lang::get('monster.publish')),Input::old('video_category_status', isset($category) ? $category->video_category_status : '' ), array('id' => 'video_category_status', 'class'=> 'form-control validate[required]')) }}

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($category) ? $category->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                            {{ Lang::get('monster.modifiedOn') }}: {{ isset($category) ? $category->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.video.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
{{ Form::close() }}
@stop
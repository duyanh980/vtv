@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.video.update',array((isset($video) ? $video->video_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('video::monster.moduleManager') }}
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('video::monster.videoName') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('video_name',Input::old('video_name', isset($video) ? $video->video_name : '' ), array('id' => 'video_name', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('video::monster.videoCategory') }}</label>
                            <div class="col-lg-6">
                                {{ Form::select('video_category_id',$categories,Input::old('video_category_id', isset($video) ? $video->video_category_id : '' ), array('id' => 'video_category_id', 'class'=> 'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('video::monster.videoAuthor') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('video_author',Input::old('video_author', isset($video) ? $video->video_author : '' ), array('id' => 'video_author', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('video::monster.videoSchool') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('video_school',Input::old('video_school', isset($video) ? $video->video_school : '' ), array('id' => 'video_school', 'class'=>'form-control')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('video::monster.videoURL') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('video_url',Input::old('video_url', isset($video) ? $video->video_url : '' ), array('id' => 'video_url', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.activity') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-xs-12"><span class="pull-left">{{ Lang::get('monster.status') }}</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('video_status',array('0'=>Lang::get('monster.draft'), '2' => Lang::get('monster.pendingReview'), '1'=> Lang::get('monster.publish')),Input::old('video_status', isset($video) ? $video->video_status : '' ), array('id' => 'video_status', 'class'=> 'form-control validate[required]')) }}

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($video) ? $video->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                            {{ Lang::get('monster.modifiedOn') }}: {{ isset($video) ? $video->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.video.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
{{ Form::close() }}
@stop
@extends('admin.main')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row prd-row">
                    <div class="col-md-6">
                        <div class="video-container">
                            <iframe width="560" height="315"
                                    src="//www.youtube.com/embed/{{$video->video_youtube_id}}" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h1>{{$video->video_name}} ({{$totalVote}} @if($totalVote>1) Votes @else Vote @endif) ({{$totalShare}} @if($totalShare>1) Shares @else Share @endif)</h1>
                        <p class="normal">{{$video->video_author}}</p>
                        <p class="terques">{{$video->video_school}}</p>
                        <section class="adv-table" id="flip-scroll">
                            <table class="kTable table table-bordered table-striped table-condensed cf" data-source="{{ route('admin.video.votes.dataTable',array($video->video_id)) }}">
                                <thead>
                                <tr>
                                    <th>{{Lang::get('video::monster.facebookName')}}</th>
                                    <th>{{Lang::get('video::monster.facebookEmail')}}</th>
                                    <th>{{Lang::get('video::monster.facebookNRIC')}}</th>
                                    <th style="width:16em;">{{Lang::get('video::monster.time')}}</th>
                                </tr>
                                </thead>
                            </table>
                        </section>
                    </div>
                </div>
            </div>
        </section>


    </div>
</div>
@stop
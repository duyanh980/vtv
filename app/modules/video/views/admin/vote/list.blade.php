@extends('admin.main')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">{{Lang::get('video::monster.moduleVoteManager')}}</header>
            <div class="panel-body">
                <section class="unseen">

                    <section class="adv-table" id="flip-scroll">
                        <table class="kTable table table-bordered table-striped table-condensed cf" data-source="{{ route('admin.video.vote.dataTable') }}">
                            <thead>
                            <tr>
                                <th>{{Lang::get('video::monster.facebookName')}}</th>
                                <th>{{Lang::get('video::monster.facebookEmail')}}</th>
                                <th>{{Lang::get('video::monster.facebookNRIC')}}</th>
                                <th style="width:16em;">{{Lang::get('video::monster.lastVote')}}</th>
                                <th style="width: 10em;"></th>
                            </tr>
                            </thead>
                        </table>
                    </section>
                </section>
            </div>
        </section>
    </div>
</div>
@stop
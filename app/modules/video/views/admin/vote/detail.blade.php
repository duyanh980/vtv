@extends('admin.main')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="feed-box text-center">
            <section class="panel">
                <div class="panel-body">
                    <img src="https://graph.facebook.com/{{$user->facebook_id}}/picture" />
                    <h1>{{ $user->name }}</h1>
                    <p>Email: {{$user->email}}</p>
                    <p>NRIC: {{$user->nric}}</p>
                </div>
            </section>
        </div>

        <section class="panel">
            <header class="panel-heading">{{Lang::get('video::monster.moduleVoteManager')}}</header>
            <div class="panel-body">

                <section class="unseen">

                    <section class="adv-table" id="flip-scroll">
                        <table class="kTable table table-bordered table-striped table-condensed cf" data-source="{{ route('admin.video.vote.detailDataTable',array(Request::segment(5))) }}">
                            <thead>
                            <tr>
                                <th>{{Lang::get('video::monster.videoName')}}</th>
                                <th>{{Lang::get('video::monster.videoSchool')}}</th>
                                <th>{{Lang::get('video::monster.videoCategory')}}</th>
                                <th style="width:16em;">{{Lang::get('video::monster.time')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </section>
                </section>
            </div>
        </section>
    </div>
</div>
@stop
<?php namespace App\Modules\Video\Seeds;
use App\Modules\Menu\Models\MenuItem;
use App\Modules\Video\Models\Category;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 9:56 AM
 */


class DatabaseSeeder extends \Seeder {

    public function run() {
        $videoMenu = MenuItem::create(array(
            'menu_item_name'    =>  'Video',
            'menu_item_url'     =>  '/admin/video',
            'menu_item_class'   =>  'fa-video',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1,
        ));

        MenuItem::create(array(
            'menu_item_name'        =>  'Video',
            'menu_item_url'         =>  '/admin/video',
            'menu_item_class'       =>  'fa-video',
            'menu_item_parent_id'   =>  $videoMenu->menu_item_id,
            'menu_id'               =>  1,
            'status'                =>  1,
            'user_id'               =>  1,
            'order'                 =>  1,
        ));

        MenuItem::create(array(
            'menu_item_name'    =>  'Category',
            'menu_item_url'     =>  '/admin/video/category',
            'menu_item_class'   =>  'fa-category',
            'menu_item_parent_id'   =>  $videoMenu->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1,
        ));

        MenuItem::create(array(
            'menu_item_name'    =>  'Voter',
            'menu_item_url'     =>  '/admin/video/vote',
            'menu_item_class'   =>  'fa-votes',
            'menu_item_parent_id'   =>  $videoMenu->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1,
        ));

        Category::create(array(
            'video_category_name'   =>  'PRIMARY SCHOOLS',
            'video_category_status' =>  1
        ));
        Category::create(array(
            'video_category_name'   =>  'SECONDARY SCHOOL & JCs',
            'video_category_status' =>  1
        ));
        Category::create(array(
            'video_category_name'   =>  'INSTITUTES OF HIGHER LEARNING',
            'video_category_status' =>  1
        ));
        Category::create(array(
            'video_category_name'   =>  'PUBLIC/OPEN',
            'video_category_status' =>  1
        ));
    }
}
<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 10:06 AM
 */

return array(
    'moduleManager'     =>  'Videos Manager',
    'moduleCategoryManager'     =>  'Categories Manager',
    'moduleVoteManager'     =>  'Votes Manager',
    'videoID'           =>  'ID',
    'videoName'         =>  'Name',
    'videoCategory'     =>  'Category',
    'videoAuthor'       =>  'Author',
    'videoSchool'       =>  'School',
    'videoURL'          =>  'URL',

    'detail'            =>  'Detail',
    'facebookName'      =>  'Name',
    'facebookEmail'     =>  'Email',
    'facebookNRIC'      =>  'NRIC',
    'lastVote'          =>  'Last vote',
    'time'              =>  'Time',

    //lang for front end
    'videoNotFound'     =>  'Video not found',
    'youHaveVoted'      =>  'You have vote',
    'serverError'       =>  'Server error occurred. Please reload website',
    'votingClose'       =>  'VOTING HAS CLOSED <br />THANK YOU FOR YOUR PARTICIPATION',
    'successVote'       =>  '<h3>Thank you for voting!</h3>Don\'t forget to cast your daily vote for<br>the other categories </br>You have 1 vote per category',
    'infoSuccess'       =>  'Thank you for subscribing.',
);
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
		    $table->increments('video_id');
            $table->string('video_name',250);
            $table->string('video_author',50);
            $table->string('video_school',50);
            $table->string('video_url',250);
            $table->integer('video_category_id');
            $table->string('video_youtube_id',250);
            $table->tinyInteger('video_status')->default(1);
            $table->integer('user_id');
            $table->timestamps();

            Schema::create('video_categories', function(Blueprint $table) {
                $table->increments('video_category_id');
                $table->string('video_category_name');
                $table->tinyInteger('video_category_status')->default(1);
                $table->integer('user_id');
                $table->timestamps();

                Schema::create('video_facebook', function(Blueprint $table) {
                    $table->double('facebook_id');
                    $table->string('name',250);
                    $table->string('email',250);
                    $table->string('nric',250);
                    $table->timestamps();
                    $table->softDeletes();

                    Schema::create('video_votes',function(Blueprint $table) {
                        $table->double('facebook_id');
                        $table->integer('video_id');
                        $table->timestamp('date_created');
                        $table->softDeletes();
                    });
                });
            });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('videos');
        Schema::drop('video_categories');
        Schema::drop('video_votes');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldSentEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('video_facebook', function(Blueprint $table)
        {
            $table->boolean('sent_email')->default(1);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('video_facebook', function(Blueprint $table)
        {
            $table->dropColumn('sent_email');
        });
	}

}

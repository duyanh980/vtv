<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 9:19 AM
 */

Route::group(array('namespace'=>'App\\Modules\\Video\\Controllers'),function() {
    Route::pattern('video_id', '[0-9]+');
    Route::get('video',                         array('as'   =>  'facebook.video.list',        'uses'  =>  'VideoController@getIndex'));
    Route::post('video',                         array('as'   =>  'facebook.video.list',        'uses'  =>  'VideoController@getIndex'));
    Route::get('video/redirect',                         array('as'   =>  'facebook.video.redirect',        'uses'  =>  'VideoController@getRedirect'));
    Route::get('video/vote/about',              array('as'   =>  'facebook.video.vote.about',        'uses'  =>  'VideoController@getAbout'));
    Route::get('video/vote/terms',              array('as'   =>  'facebook.video.vote.terms',        'uses'  =>  'VideoController@getTerms'));
    Route::get('video/{video_id?}',              array('as'   =>  'facebook.video.detail',        'uses'  =>  'VideoController@getVideo'));
    Route::get('video/vote/{video_id?}',         array('as'   =>  'facebook.video.vote.form',        'uses'  =>  'VideoController@getVoteForm'));
    Route::get('video/vote/success/{video_id?}',            array('as'   =>  'facebook.video.vote.success',        'uses'  =>  'VideoController@getSuccessForm'));
    Route::get('video/share/{video_id?}',            array('as'   =>  'facebook.video.share.info',        'uses'  =>  'VideoController@getVideoShare'));
    Route::post('video/share',            array('as'   =>  'facebook.video.share',        'uses'  =>  'VideoController@postVideoShare'));
    Route::get('video/vote/error',              array('as'   =>  'facebook.video.vote.error',        'uses'  =>  'VideoController@getErrorForm'));
    Route::get('video/vote/havevoted',              array('as'   =>  'facebook.video.vote.havevoted',        'uses'  =>  'VideoController@getHaveVoted'));
    Route::get('video/vote/info/success/{video_id?}',              array('as'   =>  'facebook.video.info.success',        'uses'  =>  'VideoController@getInfoSuccess'));
    Route::post('video/vote',                   array('as'   =>  'facebook.video.vote',        'uses'  =>  'VideoController@postVote'));
    Route::post('video/checkvote',                   array('as'   =>  'facebook.video.vote.check',        'uses'  =>  'VideoController@postCheckVote'));
    Route::post('video/postvotehaveinfo',                   array('as'   =>  'facebook.video.post.vote.have.info',        'uses'  =>  'VideoController@postVoteHaveInfo'));
    Route::post('video/forget',                   array('as'   =>  'facebook.video.forget',        'uses'  =>  'VideoController@postForget'));
});
Route::group(array('prefix'=>'admin',   'before'   =>  'auth.admin', 'namespace'=>'App\\Modules\\Video\\Controllers\\Admin'),function() {
    Route::pattern('video_id', '[0-9]+');
    Route::get('video',                        array('as'   =>  'admin.video.list',            'uses'  =>  'VideoController@getList'));
    Route::get('video/dataTable',              array('as'   =>  'admin.video.dataTable',       'uses'  =>  'VideoController@getDataTable'));
    Route::get('video/create',                 array('as'   =>  'admin.video.create',          'uses'  =>  'VideoController@getCreate'));
    Route::get('video/edit/{video_id?}',       array('as'   =>  'admin.video.edit',            'uses'  =>  'VideoController@getEdit'));
    Route::get('video/tag',                 array('as'   =>  'admin.video.tag.search',          'uses'  =>  'VideoController@getTag'));
    Route::post('video/update/{video_id?}',    array('as'   =>  'admin.video.update',          'uses'  =>  'VideoController@postUpdate'));
    Route::get('video/votes/{video_id?}',      array('as'   =>  'admin.video.votes',          'uses'  =>  'VideoController@getVotes'));
    Route::get('video/votes/dataTable/{video_id?}',      array('as'   =>  'admin.video.votes.dataTable',          'uses'  =>  'VideoController@getVotesDataTable'));
});


Route::group(array('prefix'=>'admin', 'before'=>'auth.admin','namespace'=>'App\\Modules\\Video\\Controllers\\Admin'),function() {
    Route::pattern('cat_id', '[0-9]+');
    Route::get('video/category',                   array('as'   =>  'admin.video.category.list',        'uses'  =>  'CategoryController@getList'));
    Route::get('video/category/dataTable',         array('as'   =>  'admin.video.category.dataTable',   'uses'  =>  'CategoryController@getDataTable'));
    Route::get('video/category/create',            array('as'   =>  'admin.video.category.create',      'uses'  =>  'CategoryController@getCreate'));
    Route::get('video/category/edit/{cat_id?}',    array('as'   =>  'admin.video.category.edit',        'uses'  =>  'CategoryController@getEdit'));
    Route::post('video/category/update/{cat_id?}', array('as'   =>  'admin.video.category.update',      'uses'  =>  'CategoryController@postUpdate'));
});



Route::group(array('prefix'=>'admin', 'before'=>'auth.admin','namespace'=>'App\\Modules\\Video\\Controllers\\Admin'),function() {
    Route::pattern('facebook_id', '[0-9]+');
    Route::get('video/vote',                   array('as'   =>  'admin.video.vote.list',        'uses'  =>  'VoteController@getList'));
    Route::get('video/vote/dataTable',         array('as'   =>  'admin.video.vote.dataTable',   'uses'  =>  'VoteController@getDataTable'));
    Route::get('video/vote/detail/{facebook_id}',         array('as'   =>  'admin.video.vote.detail',   'uses'  =>  'VoteController@getDetail'));
    Route::get('video/vote/detailDataTable/{facebook_id}',         array('as'   =>  'admin.video.vote.detailDataTable',   'uses'  =>  'VoteController@getDetailDataTable'));
});

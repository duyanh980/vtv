<?php namespace App\Modules\Video\Controllers\Admin;
use App\Modules\Video\Models\Category;
use App\Modules\Video\Models\Share;
use App\Modules\Video\Models\Video;
use App\Modules\Video\Models\Vote;
use Helper\Helper;
use View, Input, Datatable, Lang, Validator, Monster\Monster, Redirect, Auth;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 9:21 AM
 */
class VideoController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        return View::make('video::admin.video.list');
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Video::whereIn('video_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Video::all())
                ->showColumns('video_id','video_name','video_author')
                ->searchColumns('video_name')
                ->orderColumns('video_id','video_author','video_category_id','latest_update')
                ->addColumn('video_category_id',function($item) {
                    $category = Category::find($item->video_category_id);
                    if($category) {
                        return $category->video_category_name;
                    }
                })
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.video.edit',array($item->video_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>'.
                        '&nbsp;<a href="'.route('admin.video.votes',array($item->video_id)).'" class="btn-sm btn-primary">Votes</a>';
                })
                ->setRowID(function($item) {
                    return $item->video_id;
                })
                ->make();
        }
    }

    public function getCreate() {
        return View::make('video::admin.video.update')->with('categories',$this->dropDownFromCategory());
    }

    public function getEdit($id) {
        $video = Video::find($id);
        return View::make('video::admin.video.update')->with('video',$video)->with('categories',$this->dropDownFromCategory());
    }

    public function getVotes($id) {
        $video = Video::find($id);
        $totalVote = Vote::where("video_id",$id)->count();
        $totalShare = Share::where("video_id",$id)->count();
        return View::make('video::admin.video.votes')->with('video',$video)->with('totalVote',$totalVote)->with('totalShare',$totalShare);
    }

    public function getVotesDataTable($video_id) {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Video::whereIn('video_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Vote::join('video_facebook','video_facebook.facebook_id','=','video_votes.facebook_id')->where('video_id',$video_id)->get())
                ->showColumns('name','email','nric')
                ->searchColumns('name','email','nric')
                ->orderColumns('name','email','nric','lastest_update')
                ->addColumn('latest_update',function($item) {
                    return $item->date_created->diffForHumans();
                })
                ->make();
        }
    }

    public function postUpdate($id=false) {
        if($this->checkValidator()) {
            $video = Video::firstOrNew(array('video_id'=>$id));
            $video->fill(Input::all());
            $video->video_youtube_id = Helper::getYoutubeID($video->video_url);
            $video->user_id = Auth::user()->user_id;
            $video->save();
            return Redirect::route('admin.video.edit',array($video->video_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.video.edit',array($id));
        }
        else {
            return Redirect::to(route('admin.video.create'))->withInput();
        }
    }

    private function checkValidator() {
        $rules = Video::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }

    private function dropDownFromCategory() {
        $categories = Category::all();
        $data = array();
        if(!$categories->isEmpty()) {
            foreach($categories as $category) {
                $data[$category->video_category_id] = $category->video_category_name;
            }
        }
        return $data;
    }

    public function getTag() {
        $tags = Video::where('video_name','LIKE',Input::get('term').'%')->take(20)->get();

        $data = array();
        if(!$tags->isEmpty()) {
            $i=0;
            foreach($tags as $var) {
                $data[$i] = new \stdClass();
                $data[$i]->label = $var->video_name;
                $data[$i]->id = $var->video_id;
                ++$i;
            }
        }
        return json_encode($data);
    }
}


<?php namespace App\Modules\Video\Controllers\Admin;
use App\Modules\Video\Models\FacebookUser;
use App\Modules\Video\Models\Video;
use App\Modules\Video\Models\Vote;
use View, Datatable, Input, Lang;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 10:45 AM
 */


class VoteController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        return View::make('video::admin.vote.list');
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    FacebookUser::whereIn('facebook_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(FacebookUser::where("nric","!=","")->get())
                ->showColumns('name','email','nric')
                ->searchColumns('name','email','nric')
                ->orderColumns('name','email','nric','last_vote')
                ->addColumn('last_vote',function($item) {
                    $vote = Vote::where('facebook_id',$item->facebook_id)->orderBy('date_created','desc')->get()->first();
                    if($vote) {
                        return $vote->date_created->diffForHumans();
                    } else {
                        return '';
                    }
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.video.vote.detail',array($item->facebook_id)).'" class="btn-sm btn-primary"><i class="fa fa-detail"></i> '.Lang::get('video::monster.detail').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->facebook_id;
                })
                ->make();
        }
    }

    public function getDetail($id) {
        $user = FacebookUser::find($id);
        return View::make('video::admin.vote.detail')->with('user',$user);
    }

    public function getDetailDataTable($facebook_id) {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Video::whereIn('facebook_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Vote::where('facebook_id',$facebook_id)->get())
                ->searchColumns('video_name','video_school','video_author')
                ->orderColumns('video_name','video_school','video_author')
                ->addColumn('video_name',function($item) {
                    $video = Video::find($item->video_id);
                    if($video) {
                        return $video->video_name;
                    }
                })
                ->addColumn('video_author',function($item) {
                    $video = Video::find($item->video_id);
                    if($video) {
                        return $video->video_author;
                    }
                })
                ->addColumn('video_school',function($item) {
                    $video = Video::find($item->video_id);
                    if($video) {
                        return $video->video_school;
                    }
                })
                ->addColumn('last_vote',function($item) {
                    return $item->date_created->diffForHumans();
                })
                ->make();
        }
    }

}


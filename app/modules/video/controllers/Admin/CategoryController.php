<?php namespace App\Modules\Video\Controllers\Admin;

use App\Modules\Video\Models\Category;
use View, Input, Datatable, Lang, Validator, Monster\Monster, Redirect, Auth;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 9:24 AM
 */


class CategoryController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        return View::make('video::admin.category.list');
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Category::whereIn('video_category_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Category::all())
                ->showColumns('video_category_id','video_category_name')
                ->searchColumns('video_category_name')
                ->orderColumns('video_category_id','video_category_name','latest_update')
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.video.category.edit',array($item->video_category_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->video_category_id;
                })
                ->make();
        }
    }

    public function getCreate() {
        return View::make('video::admin.category.update');
    }

    public function getEdit($id) {
        $category = Category::find($id);
        return View::make('video::admin.category.update')->with('category',$category);
    }

    public function postUpdate($id=false) {
        if($this->checkValidator()) {
            $category = Category::firstOrNew(array('video_category_id'=>$id));
            $category->fill(Input::all());
            $category->user_id = Auth::user()->user_id;
            $category->save();
            return Redirect::route('admin.video.category.edit',array($category->video_category_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.video.category.edit',array($id));
        }
        else {
            return Redirect::to(route('admin.video.category.create'))->withInput();
        }
    }

    private function checkValidator() {
        $rules = Category::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }
}


<?php namespace App\Modules\Video\Controllers;
use App\Libraries\Facebook;
use App\Modules\Video\Models\FacebookUser;
use App\Modules\Video\Models\FacebookLogged;
use App\Modules\Video\Models\Share;
use App\Modules\Video\Models\Video;
use App\Modules\Video\Models\Vote;
use App\Modules\Video\Models\Category;
use Carbon\Carbon;
use View, Redirect, Input, Lang, Validator;
use App\Modules\System\Libraries\System;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/14/14
 * Time: 3:10 PM
 */

class VideoController extends \FrontendController {

    public function __construct() {
        parent::__construct();

    }

    public function getIndex() {
        //$this->getRedirect();
        /*
        if(!\Session::get("firstLoad")) {
            \Session::push("firstLoad",true);
            $this->getRedirect();
        }
        die("done");
        $url = Facebook::Login();
        if($url!==true && isset($_SESSION["firstRequestPermission"])) {
            $_SESSION["firstRequestPermission"] = true;
            $str = "<html><head><meta http-equiv='X-Frame-Options' content='allow'></head><body>";
            $str .= "<script>";
            $str .= "window.top.location.href='". $url ."'";
            $str .= "</script>";
            $str .= "</body></html>";
            return $str;
        }
        */
        $url = Facebook::Login();
        $fUser = Facebook::User();
        if($fUser) {
            $fid = $fUser->getProperty('id');
        } else {
            $fid = false;
        }
        $videos = Video::getAll($fid);
        if(\System::item('facebook.close')<=Carbon::now()) {
            $check_vote = 1;
        } else {
            $check_vote = 0;
        }
        if($fUser) {
            $video_facebook = FacebookUser::where('facebook_id',$fid)->where("nric","!=","")->where('sent_email','=','1')->first();
            $checkLogged = FacebookLogged::where("facebook_id",$fid)->where("logged_on",Carbon::now()->format("Y-m-d"))->first();

            if(!empty($video_facebook) || !empty($checkLogged)) {
                $isInfo = 1;
            } else {
                $isInfo = 0;
            }
        } else {
            $isInfo = 1;
        }
        $detect = new \DetectDevice();
        $isMobile = false;
        if($detect->isMobile() || $detect->isTablet()) {
            $isMobile = true;
        }
        if($fid) {
            $facebookUser = FacebookUser::findOrNew($fid);
            if(!$facebookUser->exists) {
                $facebookUser->facebook_id = $fid;
                $facebookUser->email = ($email = $fUser->getProperty('email')) ? $email : $fid.'@facebook.com';
                $facebookUser->name = $fUser->getProperty('name');
                $facebookUser->save();
            }
        }

        return View::make('video::facebook.index')
            ->with('videos',$videos)
            ->with('check_vote',$check_vote)
            ->with('isinfo',$isInfo)
            ->with('facebook_id',$fid)
            ->with("login_url",$url)
            ->with('isMobile',$isMobile)
            ->with('app_data',Facebook::getAppData());
    }
    public function postIndex() {
        //$this->getRedirect();
        /*
        if(!\Session::get("firstLoad")) {
            \Session::push("firstLoad",true);
            $this->getRedirect();
        }
        die("done");
        $url = Facebook::Login();
        if($url!==true && isset($_SESSION["firstRequestPermission"])) {
            $_SESSION["firstRequestPermission"] = true;
            $str = "<html><head><meta http-equiv='X-Frame-Options' content='allow'></head><body>";
            $str .= "<script>";
            $str .= "window.top.location.href='". $url ."'";
            $str .= "</script>";
            $str .= "</body></html>";
            return $str;
        }
        */
        $url = Facebook::Login();
        $fUser = Facebook::User();
        if($fUser) {
            $fid = $fUser->getProperty('id');
        } else {
            $fid = false;
        }
        $videos = Video::getAll($fid);
        if(\System::item('facebook.close')<=Carbon::now()) {
            $check_vote = 1;
        } else {
            $check_vote = 0;
        }
        if($fUser) {
            $video_facebook = FacebookUser::where('facebook_id',$fid)->where("nric","!=","")->where('sent_email','=','1')->first();
            $checkLogged = FacebookLogged::where("facebook_id",$fid)->where("logged_on",Carbon::now()->format("Y-m-d"))->first();

            if(!empty($video_facebook) || !empty($checkLogged)) {
                $isInfo = 1;
            } else {
                $isInfo = 0;
            }
        } else {
            $isInfo = 1;
        }
        $detect = new \DetectDevice();
        $isMobile = false;
        if($detect->isMobile() || $detect->isTablet()) {
            $isMobile = true;
        }
        if($fid) {
            $facebookUser = FacebookUser::findOrNew($fid);
            if(!$facebookUser->exists) {
                $facebookUser->facebook_id = $fid;
                $facebookUser->email = ($email = $fUser->getProperty('email')) ? $email : $fid.'@facebook.com';
                $facebookUser->name = $fUser->getProperty('name');
                $facebookUser->save();
            }
        }

        return View::make('video::facebook.index')
            ->with('videos',$videos)
            ->with('check_vote',$check_vote)
            ->with('isinfo',$isInfo)
            ->with('facebook_id',$fid)
            ->with("login_url",$url)
            ->with('isMobile',$isMobile)
            ->with('app_data',Facebook::getAppData());
    }

    public function getRedirect() {

        $detect = new \DetectDevice();
        if(!$detect->isMobile() && !$detect->isTablet()) {
            $url = \Config::get("facebook.url");
        } else {
            $url = url("video");
        }
        if(Input::get("app_data")) {
            $url .= "?app_data=".Input::get("app_data");
        }
        echo "<html><head><meta http-equiv='X-Frame-Options' content='allow'></head><body>";
        echo "<script>";
        echo "window.top.location.href='". $url ."'";
        echo "</script>";
        echo "</body></html>";
        die;
    }
    public function getVideo($video_id) {
        $video = Video::findOrFail($video_id);
        if(\System::item('facebook.close')<=Carbon::now()) {
            $check_vote = 1;
        } else {
            $check_vote = 0;
        }
        return View::make('video::facebook.popup.detail')->with('video',$video)->with('check_vote',$check_vote);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getVoteForm() {
        $video_id = Input::get('video_id');
        return View::make('video::facebook.popup.voteForm')->with('video_id',$video_id);
    }

    public function getSuccessForm() {
        $message = Lang::get('video::monster.successVote');
        return View::make('video::facebook.popup.successForm')->with('message', $message);
    }

    public function getHaveVoted() {
        $isVideo = Video::find(Input::get('video_id'));
        $cate_id = Category::find($isVideo->video_category_id);
        $message = 'You’ve already voted for this category ' . $cate_id->video_category_name . '. Have you cast your daily vote for the other categories?';
        $check = 1;
        return View::make('video::facebook.popup.errorForm')->with('message', $message)->with('check', $check);
    }

    public function getAbout() {
        $message = \System::item("facebook.about");
        $check = 1;
        return View::make('video::facebook.popup.errorForm')->with('message', $message)->with('check', $check);
    }

    public function getTerms() {
        $message = \System::item("facebook.term");
        $check = 1;

        return View::make('video::facebook.popup.terms')->with('message', $message)->with('check', $check);
    }

    public function getErrorForm() {
        $message = Lang::get('video::monster.votingClose');
        $check = 0;
        return View::make('video::facebook.popup.errorForm')->with('message', $message)->with('check', $check);
    }

    public function getInfoSuccess() {
        $message = Lang::get('video::monster.infoSuccess');
        $check = 1;
        return View::make('video::facebook.popup.errorForm')->with('message', $message)->with('check', $check);
    }

    public function postCheckVote() {

        $fUser = Facebook::User();
        if(!$fUser) {
            $url = Facebook::Login();
            if($url!==true) {
                return json_encode(array("status"=>false,"url"=>$url));
            }
        }
        $currentTime = Carbon::now()->toDateString();
        $isVideo = Video::find(Input::get('video_id'));
        if($fUser) {
            $fid = $fUser->getProperty('id');
        } else {
            $fid = false;
        }
        if($fid) {
            $checkVote = Video::where('videos.video_category_id',$isVideo->video_category_id)
                ->where(function($query) use ($fid, $currentTime) {
                    $query->select('video_votes.video_id')->where('video_votes.facebook_id',$fid)->whereRaw('date(date_created) = "'.$currentTime.'"');
                })
                ->join('video_votes','video_votes.video_id','=','videos.video_id')
                ->count();
        }

        $video_facebook = FacebookUser::where('facebook_id',$fid)->where('sent_email','=','1')->first();
        $checkLogged = FacebookLogged::where("facebook_id",$fid)->where("logged_on",Carbon::now()->format("Y-m-d"))->first();

        if(!empty($video_facebook) || !empty($checkLogged)) {
            $isInfo = 1;
        } else {
            $isInfo = 0;
        }
        return json_encode(array('isvote' => $checkVote, 'isinfo' => $isInfo,'numVote'=> Vote::where('video_id',Input::get('video_id'))->count()) );
    }

    public function postVote() {
        $fUser = Facebook::User();

        $currentTime = Carbon::now()->toDateString();
        if($fUser) {
            $fid = $fUser->getProperty('id');
        } else {
            $fid = false;
        }
        if($fid) {
            //check did video voted
            if(Input::get('video_id') > 0) {
                $isVideo = Video::findOrFail(Input::get("video_id"));
                $checkVote = Video::where('videos.video_category_id', $isVideo->video_category_id)
                    ->where(function ($query) use ($fid, $currentTime) {
                        $query->select('video_votes.video_id')->where('video_votes.facebook_id', $fid)->whereRaw('date(date_created) = "' . $currentTime . '"');
                    })
                    ->join('video_votes', 'video_votes.video_id', '=', 'videos.video_id')
                    ->count();

                //$checkVote = FacebookUser::where('facebook_id',$fUser->getProperty('id'))->where('sent_email','=','1')->count();
                //var_dump(\DB::getQueryLog());
                if ($checkVote > 0) {
                    return json_encode(array('status' => false, 'votingClose' => false));
                } else {
                    //@todo: import user information before if facebook user doesn't exits
                    if (Input::get("status")) {
                        $facebookUser = FacebookUser::findOrNew($fid);
                        $rules = FacebookUser::$rules;
                        $validator = Validator::make(Input::all(), $rules);
                        if ($validator->passes()) {
                            $facebookUser->fill(Input::all());
                            $facebookUser->facebook_id = $fid;
                            $facebookUser->save();
                        } else {
                            $messages = $validator->messages()->all('<p style="color:red">:message</p>');
                            return json_encode(array('status' => false, 'isValidate' => true, 'message' => $messages));
                        }
                    }

                    $vote = new Vote();
                    $vote->facebook_id = $fid;
                    $vote->video_id = Input::get('video_id');
                    $vote->date_created = Carbon::now();
                    $vote->save();
                    return json_encode(array('status' => true, 'video_id' => Input::get('video_id'), 'numVote' => Vote::where('video_id', Input::get('video_id'))->count()));
                }
            } else {
                if (Input::get("status")) {
                    $facebookUser = FacebookUser::findOrNew($fid);
                    $rules = FacebookUser::$rules;
                    $validator = Validator::make(Input::all(), $rules);
                    if ($validator->passes()) {
                        $facebookUser->fill(Input::all());
                        $facebookUser->facebook_id = $fid;
                        $facebookUser->save();
                        return json_encode(array('status'=>true));
                    } else {
                        $messages = $validator->messages()->all('<p style="color:red">:message</p>');
                        return json_encode(array('status' => false, 'isValidate' => true, 'message' => $messages));
                    }
                }
            }
        } else {
            return json_encode(array('status'=>false,'isValidate'=>false,'message'=>Lang::get('video::monster.serverError')));
        }
    }

    public function postVoteHaveInfo() {
        $fUser = Facebook::User();

        if (Input::get('video_id') && $fUser) {
            $vote = new Vote();
            $vote->facebook_id  =   $fUser->getProperty('id');
            $vote->video_id     =   Input::get('video_id');
            $vote->date_created =   Carbon::now();
            $vote->save();
        }

        return Vote::where('video_id',Input::get('video_id'))->count();
    }

    public function getVideoShare() {
        $video_id = Input::get('video_id');
        $video_arr = Video::where('videos.video_id', $video_id)
                            ->join('video_categories','videos.video_category_id','=','video_categories.video_category_id')
                            ->select(array('videos.video_name','video_categories.video_category_name'))
                            ->first()->toArray();
        return json_encode($video_arr);
    }

    public function postVideoShare() {
        $video_id = Input::get("video_id");
        $share = new Share();
        $fUser = Facebook::User();
        if($fUser)
        $share->facebook_id = $fUser->getProperty('id');
        $share->video_id = $video_id;
        $share->date_created =   Carbon::now();
        $share->save();
    }

    public function postForget() {
        $fUser = Facebook::User();
        if($fUser) {
            $fid = $fUser->getProperty("id");
            $logged = new FacebookLogged();
            $logged->facebook_id = $fid;
            $logged->logged_on = Carbon::now()->format("Y-m-d");
            $logged->save();
        }
    }
}
<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/24/14
 * Time: 8:28 PM
 */

Route::group(array('prefix'=>'admin',   'before'   =>  'auth.restrict', 'namespace'=>'App\\Modules\\System\\Controllers\\Admin'),function() {
    Route::get('system',               array('as'   =>  'admin.system.config',        'uses'  =>  'SystemController@getConfig'));
    Route::post('system',               array('as'   =>  'admin.system.config',        'uses'  =>  'SystemController@postConfig'));
});
<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 11:23 PM
 */


return array(
    'moduleManager'    =>  'System Manager ',
    'SiteTitle'   =>  'Title',
    'SiteKeyword'   =>  'Keyword',
    'SiteLang'   =>  'Language',
    'SiteDescription'   =>  'Description',
    'defaultLanguage'   =>  'Default language',
    'system_email'      =>  'System Email',
    'contact_email'     =>  'Contact Email',
    'HandlingErrors'    =>  'Handling Errors',
    'contestEnd'        =>  'Contest End',
    'termConditions'    =>  'Term & Conditions',
    'winner'            =>  'Video Winner',
    'name'            =>  'Share Name',
    'message'            =>  'Share Message',
    'caption'            =>  'Share Caption',
    'about'             =>  'About'
);
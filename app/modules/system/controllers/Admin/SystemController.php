<?php namespace App\Modules\System\Controllers\Admin;

use App\Modules\Video\Models\Video;
use Auth, View, Lang, Monster, Datatable, Input, Validator;
use  App\Modules\System\Models\System;
use Illuminate\Support\Facades\Redirect;
use Orchestra\Support\Facades\Asset;
class SystemController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }


    public function getConfig() {

        Asset::container('footer')->add('tags-input.css', 'assets/plugin/tags/tags-input.css');
        Asset::container('footer')->add('tags-input.js', 'assets/plugin/tags/tags-input.js');

        $keyword_str = Video::strKeyword(Video::all());
        $arr_video = Video::arrVideo(Video::all());
        $system = System::find_all();
        return View::make('system::admin.config')
                    ->with('system',$system)
                    ->with('keyword_str',$keyword_str)
                    ->with('arr_video',$arr_video);
    }

    public function postConfig() {
        $input = Input::all();
        foreach ($input as $key => $value) {
            if ($key != "_token") {
                if($key=='facebook_close') {
                    $value .=":00";
                }
                $key = str_replace('_','.',$key);
                $system = System::findOrNew($key);
                $system->key = $key;
                $system->value = $value;
                $system->save();
            }
        }

        Monster::set_message('Update Success','success');
        return Redirect::route('admin.system.config');
    }
}
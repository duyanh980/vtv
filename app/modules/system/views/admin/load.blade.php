@extends('admin.main')

@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.system.config'), 'id'=> 'validate')) }}

<div class="col-lg-12">
    <div class="panel">
        <section class="panel-heading">
            {{Lang::get('system::monster.moduleManager')}}
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                </span>
        </section>
        <div class="panel-body">
            <div class="form">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label style="text-align:left;" class="control-label col-lg-3" for="first_name">{{Lang::get('system::monster.SiteTitle')}}</label>
                        <div class="col-lg-9">
                            {{ Form::text('site.title',Input::old('title', isset($system['site.title']) ? $system['site.title'] : '' ), array('id' => 'title', 'class'=>'form-control validate[required]')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label style="text-align:left;" class="control-label col-lg-3" for="first_name">{{Lang::get('system::monster.SiteKeyword')}}</label>
                        <div class="col-lg-9">
                            {{ Form::text('site.keyword',Input::old('keyword', isset($system['site.keyword']) ? $system['site.keyword'] : '' ), array('id' => 'keyword', 'class'=>'form-control validate[required]')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label style="text-align:left;" class="control-label col-lg-3" for="first_name">{{Lang::get('system::monster.SiteLang')}}</label>
                        <div class="col-lg-9">
                            {{ Form::select('site.language', array('en' => 'English', 'vn' => 'Vietnamese'), Input::old('keyword', isset($system['site.language']) ? $system['site.language'] : 'en' ) ) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label style="text-align:left;" class="control-label col-lg-3" for="first_name">{{Lang::get('system::monster.SiteDescription')}}</label>
                        <div class="col-lg-9">
                            {{ Form::textarea('site.description',Input::old('description', isset($system['site.description']) ? $system['site.description'] : '' ), array('id' => 'description', 'class'=>'form-control validate[required]')) }}
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <button class="btn btn-primary" style="float:right" type="submit">Submit</button>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

    @stop
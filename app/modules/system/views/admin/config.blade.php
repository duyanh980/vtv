@extends('admin.main')

@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.system.config'), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-12">
        <div id="system" class="panel-group m-bot20">
            <section class="panel">
                <header class="panel-heading">
                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-parent="#system" data-toggle="collapse" href="#general">{{Lang::get('monster.general')}}</a></h4>
                </header>
                <div id="general" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="site_title">{{Lang::get('system::monster.SiteTitle')}}</label>
                                    <div class="col-lg-9">
                                        {{ Form::text('site_title',Input::old('site_title', isset($system['site.title']) ? $system['site.title'] : '' ), array('id' => 'site_title', 'class'=>'form-control validate[required]')) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="site_keyword">{{Lang::get('system::monster.SiteKeyword')}}</label>
                                    <div class="col-lg-9">
                                        {{ Form::text('site_keyword',Input::old('site_keyword', isset($system['site.keyword']) ? $system['site.keyword'] : '' ), array('id' => 'site_keyword', 'class'=>'form-control validate[required] tags')) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="site_description">{{Lang::get('system::monster.SiteDescription')}}</label>
                                    <div class="col-lg-9">
                                        {{ Form::textarea('site_description',Input::old('site_description', isset($system['site.description']) ? $system['site.description'] : '' ), array('id' => 'site_description', 'class'=>'form-control validate[required]')) }}
                                    </div>
                                </div>

                                <div class="col-lg-9 col-lg-offset-3">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-parent="#system" data-toggle="collapse" href="#facebook">{{Lang::get('monster.facebook')}}</a></h4>
                </header>
                <div id="facebook" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="form">
                            <div class="form-horizontal">

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="site_title">{{Lang::get('system::monster.contestEnd')}}</label>
                                    <div class="col-md-4">
                                        <div class="input-group date form_datetime-component">
                                            {{ Form::text('facebook_close',Input::old('facebook_close', isset($system['facebook.close']) ? substr($system['facebook.close'],0,16) : '' ), array('id' => 'facebook_close', 'class'=>'form-control validate[required]','readonly'=>'')) }}
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary date-set" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('system::monster.winner')}}</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            {{ Form::text('facebook.winner',Input::old('facebook_winner', isset($system['facebook.winner']) ? $system['facebook.winner'] : '' ), array('id' => 'facebook_winner', 'class'=>'form-control validate[required]')) }}
                                            {{ Form::hidden('facebook.winner_id',Input::old('facebook_winner_id', isset($system['facebook.winner_id']) ? $system['facebook.winner_id'] : '' ), array('id' => 'facebook_winner_id', 'class'=>'form-control validate[required]')) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('system::monster.name')}}</label>
                                    <div class="col-md-4">
                                        <div class="">
                                            {{ Form::text('facebook.name',Input::old('facebook_name', isset($system['facebook.name']) ? $system['facebook.name'] : '' ), array('id' => 'facebook_name', 'class'=>'form-control validate[required]')) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('system::monster.message')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            {{ Form::textarea('facebook.message',Input::old('facebook_message', isset($system['facebook.message']) ? $system['facebook.message'] : '' ), array('id' => 'facebook_message', 'class'=>'form-control validate[required]')) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('system::monster.caption')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            {{ Form::textarea('facebook.caption',Input::old('facebook_caption', isset($system['facebook.caption']) ? $system['facebook.caption'] : '' ), array('id' => 'facebook_caption', 'class'=>'form-control validate[required]')) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('system::monster.termConditions')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            {{ Form::textarea('facebook.term',Input::old('facebook_term', isset($system['facebook.term']) ? $system['facebook.term'] : '' ), array('id' => 'facebook_term', 'class'=>'form-control validate[required] ckeditor','readonly'=>'')) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('system::monster.about')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            {{ Form::textarea('facebook.about',Input::old('facebook_about', isset($system['facebook.about']) ? $system['facebook.about'] : '' ), array('id' => 'facebook_about', 'class'=>'form-control validate[required] ckeditor','readonly'=>'')) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-9 col-lg-offset-3">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="panel">
                <header class="panel-heading">
                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-parent="#system" data-toggle="collapse" href="#language">{{Lang::get('monster.language')}}</a></h4>
                </header>
                <div id="language" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="form">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="site_title">{{Lang::get('system::monster.defaultLanguage')}}</label>
                                    <div class="col-lg-9">
                                        {{ Form::select('language_default', array('en' => 'English'), Input::old('language_default', isset($system['language.default']) ? $system['language.default'] : 'en' ) , array('id' => 'site_description', 'class'=>'form-control validate[required]')) }}
                                    </div>
                                </div>

                                <div class="col-lg-9 col-lg-offset-3">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-parent="#system" data-toggle="collapse" href="#mail">{{Lang::get('monster.mailAddress')}}</a></h4>
                </header>
                <div id="mail" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="form">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="site_title">{{Lang::get('system::monster.system_email')}}</label>
                                    <div class="col-lg-9">
                                        {{ Form::text('mail_system',Input::old('mail_system', isset($system['mail.system']) ? $system['mail.system'] : '' ), array('id' => 'mail_system', 'class'=>'form-control validate[required,custom[email]]')) }}
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="site_title">{{Lang::get('system::monster.contact_email')}}</label>
                                    <div class="col-lg-9">
                                        {{ Form::text('mail_contact',Input::old('mail_contact', isset($system['mail.contact']) ? $system['mail.contact'] : '' ), array('id' => 'mail_contact', 'class'=>'form-control validate[required,custom[email]]')) }}
                                    </div>
                                </div>

                                <div class="col-lg-9 col-lg-offset-3">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <h4 class="panel-title"><a class="accordion-toggle collapsed" data-parent="#system" data-toggle="collapse" href="#handlingErrors">{{Lang::get('system::monster.HandlingErrors')}}</a></h4>
                </header>
                <div id="handlingErrors" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="form">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('system::monster.HandlingErrors')}}</label>
                                    <div class="col-lg-9">
                                        <label for="handling_error_yes radio-inline">{{ Form::radio('handling_error', '1',Input::old('handling_error', (isset($system['handling.error']) AND $system['handling.error']==1) ? true : false ), array('id' => 'handling_error_yes')) }} Yes</label>
                                        <label for="handling_error_yes radio-inline">{{ Form::radio('handling_error', '1',Input::old('handling_error', (isset($system['handling.error']) AND $system['handling.error']==1) ? true : false ), array('id' => 'handling_error_no')) }} No</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{Lang::get('monster.email')}}</label>
                                    <div class="col-lg-9">
                                        {{ Form::text('handling_error_email',Input::old('handling_error_email', isset($system['handling.error.email']) ? $system['handling.error.email'] : '' ), array('id' => 'handling_error_email', 'class'=>'form-control validate[required,custom[email]]')) }}
                                    </div>
                                </div>

                                <div class="col-lg-9 col-lg-offset-3">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>


    </div>
</div>
{{ Form::close() }}

<script type="text/javascript">
    <?php if(isset($keyword_str)): ?>
    var tagsAvailable = "<?php echo $keyword_str?>";
    <?php endif;?>

$(document).ready(function() {
    $('.tags').tagsInput({width:'auto'});

    $(".form_datetime-component").datetimepicker({
        format: "yyyy-mm-dd hh:ii"
    });

    $( "#facebook_winner" ).autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{{ route('admin.video.tag.search') }}",
                dataType: "json",
                data: {
                    term: request.term,
                    tagsAvailable : tagsAvailable
                },
                success: function(data) {
                    response(data);
                    console.log(data);
                }
            });
        },
        minLength: 1,
        select: function( event, ui ) {
        }
    });
    $( "#facebook_winner" ).on('change', function() {
        var video_arr = [];
        @foreach ($arr_video as $key => $item)
            video_arr["{{$key}}"] = '{{$item}}';
        @endforeach
        $("#facebook_winner_id").val(video_arr[$( "#facebook_winner").val()]);
    });
});

</script>
@stop
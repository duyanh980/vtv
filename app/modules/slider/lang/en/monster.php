<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 5/9/14
 * Time: 12:01 PM
 */

return array(
    'sliderName'    =>  'Slider name',
    'moduleManager' =>  'Sliders Manager',
    'title'         =>  'Title',
    'capture'       =>  'Capture',
    'description'   =>  'Description',
    'listItems'     =>  'List',
    'url'           =>  'URL',
    'activity'      =>  'Actives',
    'settings'      =>  'Settings',
    'fillmode'      =>  'Fill mode',
    'stretch'       =>  'Stretch',
    'contain'       =>  'Contain',
    'cover'         =>  'Cover',
    'actuall'       =>  'Actually',
    'autoplay'      =>  'Auto Play',
    'AutoPlayInterval'  =>  'AutoPlayInterval',
    'browseImage'   =>  'Browse Image'

);
<?php namespace App\Modules\Slider\Seeds;
use App\Modules\Menu\Models\MenuItem;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 5/23/14
 * Time: 9:20 AM
 */


class DatabaseSeeder extends \Seeder {

    public function run() {
        MenuItem::create(array(
            'menu_item_name'    =>  'Slider',
            'menu_item_url'     =>  '/admin/slider',
            'menu_item_class'   =>  'fa-picture-o',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  7
        ));

    }
}


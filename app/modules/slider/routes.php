<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 5/9/14
 * Time: 10:33 AM
 */


Route::group(array('prefix'=>'admin', 'before'=>'auth.restrict','namespace'=>'App\\Modules\\Slider\\Controllers\\Admin'),function() {

    Route::get('slider',                    array('as'   =>  'admin.slider.list',                'uses'  =>  'SliderController@getList'));
    Route::get('slider/dataTable',          array('as'   =>  'admin.slider.dataTable',           'uses'  =>  'SliderController@getDataTable'));
    Route::get('slider/get',          array('as'   =>  'admin.slider.get',           'uses'  =>  'SliderController@getTest'));

    Route::get('slider/create',             array('as'   =>  'admin.slider.create',              'uses'  =>  'SliderController@getCreate'));
    Route::get('slider/edit/{id}',          array('as'   =>  'admin.slider.edit',                'uses'  =>  'SliderController@getEdit'));
    Route::post('slider/update/{id?}',      array('as'   =>  'admin.slider.update',              'uses'  =>  'SliderController@postUpdate'));

    Route::get('slider/effect',                    array('as'   =>  'admin.slider.effect',                'uses'  =>  'SliderController@getEffect'));
    Route::get('slider/captureEffect',             array('as'   =>  'admin.slider.capture.effect',                'uses'  =>  'SliderController@getCaptureEffect'));
    Route::post('slider/image',                    array('as'   =>  'admin.slider.image',              'uses'  =>  'SliderController@getSliderImage'));
});
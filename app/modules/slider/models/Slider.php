<?php namespace App\Modules\Slider\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 5/9/14
 * Time: 10:34 AM
 */

class Slider extends \Eloquent {

    protected $table = 'sliders';
    protected $primaryKey = 'slider_id';
    protected $fillable = array('slider_name','slider_settings');

    public static $rules = array(
        'slider_name'=>'required|min:2|max:50',
    );


}
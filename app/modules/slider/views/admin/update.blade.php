@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.slider.update',array((isset($slider) ? $slider->slider_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('slider::monster.moduleManager') }}
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <div class="col-lg-12" align="center">@if (isset($sliderImage)) {{ $sliderImage }} @endif</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('slider::monster.sliderName') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('slider_name',Input::old('slider_name', isset($slider) ? $slider->slider_name : '' ), array('id' => 'slider_name', 'class'=>'form-control validate[required]')) }}

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-9 col-lg-offset-3">
                                <a href="<?php echo route('admin.media.popup.manager'); ?>?type=multiple-images" class="btn btn-primary open-popup-ajax">
                                    {{ Lang::get('slider::monster.browseImage') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                {{Lang::get('slider::monster.listItems')}}
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">
                        <ul id="sortable" class="sliderImage">
                            @if (isset($slider_contents))
                                @foreach ($slider_contents as $item)
                                <li class="deleteImage ui-state-default">
                                    <div class="delete-slider-image">Delete</div>
                                    <div class="form-group image-content" >
                                        <div class="col-lg-3">
                                            <img src="{{ $item->avatar }}" id="avatar_preview" class="image_preview" />
                                            <input type="hidden" id="order" value="1" name="order[]" />
                                            <input type="hidden" id="avatar" value="{{ $item->avatar }}" name="image[]" />
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.title') }}</label>
                                                <div class="col-lg-9">
                                                    {{ Form::text('title[]', Input::old('text[]', isset($item) ? $item->name : '' ), array('id' => 'slider_name', 'class'=>'form-control')) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.capture') }}</label>
                                                <div class="col-lg-9">
                                                    {{ Form::text('capture[]', Input::old('capture[]', isset($item) ? $item->capture : '' ), array('class'=>'form-control')) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.url') }}</label>
                                                <div class="col-lg-9">
                                                    {{ Form::text('url[]', Input::old('url[]', isset($item) ? $item->url : '' ), array('class'=>'form-control')) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.description') }}</label>
                                                <div class="col-lg-9">
                                                    {{ Form::textarea('description[]', Input::old('description[]', isset($item) ? $item->description : '' ), array('class'=>'form-control', 'rows'=>'2')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('slider::monster.activity') }}
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                </span>
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($permission) ? $permission->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                            {{ Lang::get('monster.modifiedOn') }}: {{ isset($permission) ? $permission->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.slider.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('slider::monster.settings') }}
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                </span>
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-lg-5"><span class="pull-left">{{ Lang::get('slider::monster.fillmode') }}</span></label>
                        <div class="col-xs-7">
                            {{ Form::select('fillmode',array('-1' => '', '0'=>Lang::get('slider::monster.stretch'), '1' => Lang::get('slider::monster.contain'), '2'=> Lang::get('slider::monster.cover'), '3'=> Lang::get('slider::monster.actuall')),Input::old('fillmode', isset($slider_settings) ? $slider_settings->fillmode : '0' ), array('id' => 'status', 'class'=> 'form-control validate[required]')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-5"><span class="pull-left">{{ Lang::get('slider::monster.autoplay') }}</span></label>
                        <div class="col-xs-7">
                            {{ Form::checkbox('autoplay', 1 , isset($slider_settings) ? $slider_settings->autoplay : true, array('id' => 'autoplay')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-5"><span class="pull-left">{{ Lang::get('slider::monster.AutoPlayInterval') }}</span></label>
                        <div class="col-xs-7">
                            {{ Form::text('autoplayinterval', Input::old('autoplayinterval',isset($slider_settings) ? $slider_settings->autoplayinterval : '2000') , array('id' => 'AutoPlayInterval', 'class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-12"><span class="pull-left">pauseOnHover</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('pause',array('-1' => '', '0'=>'No pause', '1' => 'Pause for desktop', '2'=> 'Pause for touch device', '3'=> 'Pause for desktop and touch device'),Input::old('pause', isset($slider_settings) ? $slider_settings->pause : '0' ), array('id' => 'status', 'class'=> 'form-control validate[required]')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-12"><span class="pull-left">Effect</span></label>
                        <div class="col-xs-12">
                            <select name="effect" class="form-control" id="effect">
                                @include('slider::admin.effect')
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-12"><span class="pull-left">Capture Effect</span></label>
                        <div class="col-xs-12">
                            <select name="capture_effect" class="form-control" id="effect_capture">
                                @include('slider::admin.captureEffect')
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-5"><span class="pull-left">SlideDuration</span></label>
                        <div class="col-xs-7">
                            {{ Form::text('duration', Input::old('duration', isset($slider_settings) ? $slider_settings->duration : '2000') , array('id' => 'duration', 'class'=>'form-control')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
{{ Form::close() }}

<script type="text/javascript">

    $(document).ready(function() {
        $(document).bind("addFile",function() {
            $.each(ListImageSelected,function() {
                $.ajax({
                    url: "{{ route('admin.slider.image') }}",
                    type: "post",
                    data: { url : this.media_file_full_url, name : this.media_name },
                    success: function(returnHTML){
                        $(".sliderImage").prepend(returnHTML);
                    }
                })
            })
        });

        $('.remove-image').on("click",function() {
            $('#avatar').val('');
            $('#avatar_preview').remove();
            $('.fileupload').removeClass('fileupload-exists').addClass('fileupload-new');
        });

        $('#effect option[value="@if (isset($slider_settings)){{ $slider_settings->effect }}@else{$Duration:700,$Opacity:2,$Brother:{$Duration:1000,$Opacity:2}}@endif"]').attr('selected','selected');
        $('#effect_capture option[value="@if (isset($slider_settings)){{ $slider_settings->capture_effect }}@else{$Duration:900,$FlyDirection:1,$Easing:{$Left:$JssorEasing$.$EaseInOutSine},$ScaleHorizontal:0.6,$Opacity:2}@endif"]').attr('selected','selected');

        $(".sliderImage").sortable({
            handle: '.image_preview'
        });
    });

    $(document).on('click','#autoplay',function() {
        if ($('#autoplay').attr('checked')) {
            $('#autoplay').val('true');
        } else {
            $('#autoplay').val('false');
        }
    });

    $(document).on('click','.delete-slider-image',function() {
        $(this).parent().remove();
    });

</script>
@stop
<li class="deleteImage ui-state-default">
    <div class="delete-slider-image">Delete</div>
    <div class="form-group">
        <div class="col-lg-3">
            <img src="{{ $url }}" id="avatar_preview" class="image_preview" />
            <input type="hidden" id="avatar" value="{{ $url }}" name="image[]" />
        </div>
        <div class="col-lg-9">
            <div class="form-group">
                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.title') }}</label>
                <div class="col-lg-9">
                    {{ Form::text('title[]', $name , array('id' => 'slider_name', 'class'=>'form-control')) }}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.capture') }}</label>
                <div class="col-lg-9">
                    {{ Form::text('capture[]',null, array('class'=>'form-control')) }}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.url') }}</label>
                <div class="col-lg-9">
                    {{ Form::text('url[]',null, array('class'=>'form-control')) }}
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{{ Lang::get('slider::monster.description') }}</label>
                <div class="col-lg-9">
                    {{ Form::textarea('description[]',null, array('class'=>'form-control', 'rows'=>'2')) }}
                </div>
            </div>
        </div>
    </div>
</li>

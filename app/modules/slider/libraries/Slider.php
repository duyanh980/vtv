<?php namespace App\Modules\Slider\Libraries;
use App\Modules\Slider\Models\Slider as SliderModel;
use Asset;
use View;
/**
 * Author: TrongVd
 * Email: trongvd85@gmail.com
 * Date: 4/20/14
 * Time: 9:16 PM
 */

class Slider {

    public static function load($id=false) {

        $slider = json_decode(SliderModel::find($id));
        if(!$slider) return false;
        $contents = json_decode($slider->slider_contents);
        $settings = json_decode($slider->slider_settings);

        if (!empty($contents)) {
            Asset::container('footer')->add('jssor.core.js', 'assets/plugin/jssor/jssor.core.js');
            Asset::container('footer')->add('jssor.utils.js', 'assets/plugin/jssor/jssor.utils.js');
            Asset::container('footer')->add('jssor.slider.js', 'assets/plugin/jssor/jssor.slider.js');
            Asset::container('footer')->add('jssor.style.css', 'assets/plugin/jssor/style.css');

            $data = View::make('slider::admin.slider')
                ->with('id',$id)
                ->with('contents',$contents)
                ->with('settings', $settings)
                ->render();

            return $data;
        }
    }

}
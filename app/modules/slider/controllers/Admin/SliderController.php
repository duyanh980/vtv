<?php namespace App\Modules\Slider\Controllers\Admin;
use View,Input,Validator,Redirect, Datatable, Monster, Auth, Asset;
use App\Modules\Slider\Models\Slider as SliderModel;
use App\Modules\Slider\Libraries\Slider;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 5/9/14
 * Time: 10:34 AM
 */


class SliderController extends \BackEndController {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        return View::make('slider::admin.list');
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    SliderModel::whereIn('slider_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(SliderModel::all())
                ->showColumns('slider_name')
                ->searchColumns('slider_name')
                ->orderColumns('slider_id','slider_name','latest_update')
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.slider.edit',array($item->slider_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->slider_id;
                })
                ->make();
        }
    }

    public function getCreate() {
        return View::make('slider::admin.update');
    }

    public function getEdit($id) {
        $slider = SliderModel::find($id);
        $slider_contents = json_decode($slider->slider_contents);
        $slider_settings = json_decode($slider->slider_settings);

        $sliderImage = Slider::load($id);

        return View::make('slider::admin.update')
                    ->with('slider',$slider)
                    ->with('slider_contents', $slider_contents)
                    ->with('slider_settings', $slider_settings)
                    ->with('sliderImage', $sliderImage);
    }

    public function getSliderImage() {
        $name = Input::get('name');
        $url = Input::get('url');

        return View::make('slider::admin.sliderImage')->with('name', $name)->with('url', $url);
    }

    public function getEffect() {
        return View::make('slider::admin.effect');
    }

    public function getCaptureEffect() {
        return View::make('slider::admin.captureEffect');
    }

    public function postUpdate($id=false) {

        // SliderModel Contents

        $title = Input::get('title');
        $capture = Input::get('capture');
        $url = Input::get('url');
        $description = Input::get('description');
        $avatar = Input::get('image');

        // SliderModel Settings

        $fillmode = Input::get('fillmode');
        $autoplay = Input::get('autoplay');
        $autoplayinterval = Input::get('autoplayinterval');
        $pause = Input::get('pause');
        $effect = Input::get('effect');
        $capture_effect = Input::get('capture_effect');
        $duration = Input::get('duration');

        if($this->checkValidator($id)) {
            $key=0;
            $slider_content = array();
            $slider = SliderModel::firstOrNew(array('slider_id'=>$id));
            $slider->fill(Input::all());
            $slider->user_id = Auth::user()->user_id;
            if (!empty($avatar)) {
                foreach ($avatar as $item) {
                    $slider_content[$key] = new \stdClass();
                    $slider_content[$key]->name = $title[$key];
                    $slider_content[$key]->capture = $capture[$key];
                    $slider_content[$key]->url = $url[$key];
                    $slider_content[$key]->description = $description[$key];
                    $slider_content[$key]->avatar = $item;
                    ++$key;
                }
            }
            $slider_settings = new \stdClass();
            $slider_settings->fillmode = $fillmode;
            $slider_settings->autoplay = $autoplay;
            $slider_settings->autoplayinterval = $autoplayinterval;
            $slider_settings->pause = $pause;
            $slider_settings->effect = $effect;
            $slider_settings->capture_effect = $capture_effect;
            $slider_settings->duration = $duration;

            $slider->slider_contents = json_encode($slider_content);
            $slider->slider_settings = json_encode($slider_settings);
            $slider->save();
            return Redirect::route('admin.slider.edit',array($slider->slider_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.slider.edit',array($id));
        }
        else {
            return Redirect::to(route('admin.slider.create'))->withInput();
        }
    }

    private function checkValidator($id=false) {

        $rules = SliderModel::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }

}
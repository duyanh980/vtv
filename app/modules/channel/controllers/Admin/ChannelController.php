<?php namespace App\Modules\Channel\Controllers\Admin;

use App\Modules\Channel\Models\Channel;use Auth, View, Lang, Monster, Datatable, Input, Validator;
use Illuminate\Support\Facades\Redirect;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:34 PM
 */

class ChannelController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        return View::make('channel::admin.list');
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Channel::whereIn('channel_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Channel::all())
                ->showColumns('channel_name')
                ->searchColumns('channel_name')
                ->orderColumns('channel_id','channel_name','latest_update')
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.channel.edit',array($item->channel_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->channel_id;
                })
                ->make();
        }
    }

    public function getCreate() {
        return View::make('channel::admin.update');
    }

    public function getEdit($id) {
        $channel = Channel::find($id);
        return View::make('channel::admin.update')->with('channel',$channel);
    }

    public function postUpdate($id=false) {
        if($this->checkValidator($id)) {
            $user = Channel::firstOrNew(array('channel_id'=>$id));
            $user->fill(Input::all());
            $user->user_id = Auth::user()->user_id;
            $user->save();
            return Redirect::route('admin.channel.edit',array($user->channel_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.channel.edit',array($id));
        }
        else {
            return Redirect::to(route('admin.channel.create'))->withInput();
        }
    }

    private function checkValidator($id=false) {

        $rules = Channel::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }
}
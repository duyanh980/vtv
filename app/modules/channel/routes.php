<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:32 PM
 */

Route::group(array('prefix'=>'admin','namespace'=>'App\\Modules\\Channel\\Controllers\\Admin'),function() {

    Route::get('channel',                    array('as'   =>  'admin.channel.list',                'uses'  =>  'ChannelController@getList'));
    Route::get('channel/dataTable',          array('as'   =>  'admin.channel.dataTable',           'uses'  =>  'ChannelController@getDataTable'));

    Route::get('channel/create',             array('as'   =>  'admin.channel.create',              'uses'  =>  'ChannelController@getCreate'));
    Route::get('channel/edit/{id}',          array('as'   =>  'admin.channel.edit',                'uses'  =>  'ChannelController@getEdit'));
    Route::post('channel/update/{id?}',      array('as'   =>  'admin.channel.update',              'uses'  =>  'ChannelController@postUpdate'));


    Route::get('channel/category',                    array('as'   =>  'admin.channel.category.list',                'uses'  =>  'ChannelCategoryController@getList'));
    Route::get('channel/category/dataTable',          array('as'   =>  'admin.channel.category.dataTable',           'uses'  =>  'ChannelCategoryController@getDataTable'));

    Route::get('channel/category/create',             array('as'   =>  'admin.channel.category.create',              'uses'  =>  'ChannelCategoryController@getCreate'));
    Route::get('channel/category/edit/{id}',          array('as'   =>  'admin.channel.category.edit',                'uses'  =>  'ChannelCategoryController@getEdit'));
    Route::post('channel/category/update/{id?}',      array('as'   =>  'admin.channel.category.update',              'uses'  =>  'ChannelCategoryController@postUpdate'));

});
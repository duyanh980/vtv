<?php namespace App\Modules\Channel\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:45 PM
 */


class ChannelCategory extends \Eloquent {

    protected $table = 'channel_category';
    protected $primaryKey = 'category_id';
    protected $fillable = array('category_name','status');

    public static $rules = array(
        'category_name'=>'required|max:250',
    );


}
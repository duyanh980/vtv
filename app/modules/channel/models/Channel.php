<?php namespace App\Modules\Channel\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:45 PM
 */


class Channel extends \Eloquent {

    protected $table = 'channel';
    protected $primaryKey = 'channel_id';
    protected $fillable = array('channel_name','category_id','channel_image','channel_description','status');

    public static $rules = array(
        'channel_name'=>'required|max:250',
        'channel_image'=>'required|max:250',
        'channel_description'    =>  'required|min:8',
    );


}
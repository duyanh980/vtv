<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Channel extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_category', function(Blueprint $table) {
            $table->increments('category_id');
            $table->string('category_name');
            $table->integer('status');
            $table->integer('user_id');
            $table->timestamps();
        });
        Schema::create('channel', function (Blueprint $table) {
            $table->increments('channel_id');
            $table->string('channel_name');
            $table->string('channel_image');
            $table->integer('category_id');
            $table->integer('status');
            $table->text('channel_description');
            $table->integer('user_id');
            $table->timestamps();

            Schema::create('program', function(Blueprint $table) {
                $table->increments('program_id');
                $table->string('program_name');
                $table->string('program_image');
                $table->integer('status');
                $table->text('program_description');
                $table->integer('user_id');
                $table->timestamps();

                Schema::create('broadcast_schedule', function(Blueprint $table) {
                    $table->integer('program_id');
                    $table->integer('channel_id');
                    $table->timestamp('time');
                });

                Schema::create('program_favorite', function(Blueprint $table) {
                    $table->integer('user_id');
                    $table->integer('program_id');
                });
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('channel');
        Schema::drop('program');
        Schema::drop('broadcast_schedule');
        Schema::drop('program_favorite');
    }
}


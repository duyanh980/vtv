@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.channel.update',array((isset($channel) ? $channel->channel_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('channel::monster.moduleManager') }}
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="channel_name">{{ Lang::get('channel::monster.channelName') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('channel_name',Input::old('channel_name', isset($channel) ? $channel->channel_name : '' ), array('id' => 'channel_name', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="category_id">{{ Lang::get('channel::monster.categoryName') }}</label>
                            <div class="col-lg-6">
                                {{ Form::select('category_id',\App\Modules\Channel\Models\ChannelCategory::lists('category_name','category_id'),Input::old('category_id', isset($channel) ? $channel->category_id : '' ), array('id' => 'category_id', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="channel_description">{{ Lang::get('channel::monster.channelDescription') }}</label>
                            <div class="col-lg-6">
                                {{ Form::textarea('channel_description',Input::old('channel_description', isset($channel) ? $channel->channel_description : '' ), array('id' => 'channel_description', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.activity') }}
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                </span>
            </header>
            <div class="panel-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-xs-12"><span class="pull-left">{{ Lang::get('monster.status') }}</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('status',array('0'=>Lang::get('monster.draft'), '1'=> Lang::get('monster.publish')),Input::old('status', isset($channel) ? $channel->status : '' ), array('id' => 'status', 'class'=> 'form-control validate[required]')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($channel) ? $channel->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                           {{ Lang::get('monster.modifiedOn') }}: {{ isset($channel) ? $channel->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.channel.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                Channel Image
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12">

                            <div class="fileupload <?php echo (Input::old('channel_image', isset($channel) ? $channel->channel_image : '' )!="") ? 'fileupload-exists' : 'fileupload-new'; ?>">
                                @if(Input::old('channel_image', isset($channel) ? $channel->channel_image : '' )!="")
                                <img src="{{ url(Input::old('channel_image', isset($channel) ? $channel->channel_image : '' )) }}" id="channel_preview" class="image_preview" />
                                @endif
                                    <input type="hidden" id="channel_image" name="channel_image" value="{{Input::old('channel_image', isset($channel) ? $channel->channel_image : '' )}}" />
                                    <a href="<?php echo route('admin.media.popup.manager'); ?>?type=single-image" class="btn btn-white btn-file open-popup-ajax">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                    </a>
                                    <a href="#" class="btn btn-danger fileupload-exists remove-image" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </div>
</div>
{{ Form::close() }}


<script type="text/javascript">
    $(document).ready(function() {
        $(document).bind("addFile",function() {
            $('#channel_image').val(ImageSelected.media_file_url);
            if($('#channel_preview').get(0)) {
                $('#channel_preview').attr('src',ImageSelected.media_file_full_url);
            }
            else {
                $('.fileupload').prepend('<img src="' + ImageSelected.media_file_full_url + '" id="channel_preview" class="image_preview" />');
            }
            $('.fileupload').removeClass('fileupload-new').addClass('fileupload-exists');
        });

        $('.remove-image').on("click",function() {
            $('#channel_image').val('');
            $('#channel_preview').remove();
            $('.fileupload').removeClass('fileupload-exists').addClass('fileupload-new');
        });
    });
</script>
@stop
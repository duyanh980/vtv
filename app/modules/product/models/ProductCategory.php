<?php namespace App\Modules\Product\Models;
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 5/7/14
 * Time: 8:31 PM
 */

class ProductCategory extends \Eloquent {

    protected $table = 'product_category_links';
    protected $primaryKey = 'product_id';
    protected $fillable = array('product_id','product_category_id');

    public $timestamps = false;

    public static function updateCategory($id=false,$categories= array()) {
        if (!empty($categories)) {
            self::where('product_id', '=', $id)->delete();
            foreach ($categories as $category_id) {
                $productCategory = self::create(array('product_id' => $id, 'product_category_id' => $category_id));
                $productCategory->save();
            }
        }

    }
}
<?php namespace App\Modules\Product\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/18/14
 * Time: 8:44 AM
 */

class Product extends \Eloquent {

    protected $table = 'products';
    protected $primaryKey = 'product_id';
    protected $fillable = array('product_name','product_sku','product_price','product_special_price','product_image','product_short_description','product_description','product_status','product_category_id','status');

    public static $rules = array(
        'product_name'=>'required|min:1|max:250',
        'product_sku'=>'required|min:1|max:250',
        'product_price'=>'numeric',
        'product_special_price'=>'numeric',
        'product_image'=>'min:1|max:250',
        'product_short_description'=>'required|min:1|max:250',
        'product_status'=>'required|integer',
    );
}
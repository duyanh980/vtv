<?php namespace App\Modules\Product\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/18/14
 * Time: 8:45 AM
 */
class Category extends \Eloquent {

    protected $table = 'product_categories';
    protected $primaryKey = 'product_category_id';
    protected $fillable = array('product_category_name','product_category_slug','product_category_id','product_category_status');

    public static $rules = array(
        'product_category_name'=>'required|min:1|max:250',
        'product_category_slug'=>'required|min:1|max:250',
        'product_category_status'=>'required|integer',
    );


    public static function buildListMenuItemHtml($catalog=false,$parent_id=0) {
        $str='';
        if(!is_array($catalog) AND $parent_id==0) {
            $catalog = self::listOrder($catalog);
        }
        if($catalog===false AND $parent_id==0) {
            $catalog = self::listOrder();
        }
        if(!empty($catalog)) {
            $str .= '<ol class="dd-list">';
            foreach($catalog as $var) {
                $str .='<li class="dd-item dd3-item" data-id="'.$var->product_category_id.'">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content"><a href="'.route('admin.product.category',array($var->product_category_id)).'">'.$var->product_category_name.'</a></div>';
                if(!empty($var->items)){
                    $str .= self::buildListMenuItemHtml($var->items,$var->product_category_parent_id);
                }
                $str .='</li>';
            }
            $str .='</ol>';
        }
        return $str;
    }

    public static function tree($product_id=false,$catalog=false,$parent_id=0,$isFirst=true) {
        $str='';
        if(!is_array($catalog) AND $parent_id==0) {
            $catalog = self::listOrder($catalog);
        }
        if($catalog===false AND $parent_id==0) {
            $catalog = self::listOrder();
        }

        $cate_all = ProductCategory::where('product_id','=',$product_id)->get();

        $data = Array();
        foreach ($cate_all as $item) {
            $data[$item->product_category_id] = $item->product_category_id;
        }

        if(!empty($catalog)) {
            if($isFirst) {
                $str .= '<ul class="jqtree_common jqtree-tree" style="list-style: none;">';
            }
            else {
                $str .= '<ul class="jqtree_common" style="">';
            }
            foreach($catalog as $var) {

                $str .= '<li class="jqtree_common jqtree-folder jqtree-closed" data-id="'.$var->product_category_id.'">
                        <div class="jqtree-element jqtree_common" style="padding-bottom: 5px;">
                        <i class="jqtree_common jqtree-toggler jqtree-closed">
                        <input type="checkbox" name="category[]" value="' . $var->product_category_id . '"';

                if (isset($data[$var->product_category_id])) {
                    $str .= 'checked';
                }

                $str .= ' /></i><span class="jqtree_common jqtree-title"> '.$var->product_category_name.'</span></div>';

                if(!empty($var->items)){
                    $str .= self::tree($product_id,$var->items,$var->product_category_parent_id,false);
                }
                $str .='</li>';

            }
            $str .='</ul>';
        }
        return $str;
    }

    public static function buildOptionsCategoryHTML($catalog=false,$pre = null,$parent_id=0) {
        $str = '';
        if(!is_array($catalog) AND $parent_id==0) {
            $catalog = self::listOrder($catalog);
        }
        if($catalog===false AND $parent_id==0) {
            $catalog = self::listOrder();
        }
        if(!empty($catalog)) {
            foreach($catalog as $var) {
                if($parent_id != $var->product_category_parent_id) {
                    $pre .= '-- ';
                    $parent_id = $var->product_category_parent_id;
                }
                $str .= '<option value="'.$var->product_category_id.'">'.$pre.$var->product_category_name.'</option>';
                if(is_array($var->items)){
                    $str .= self::buildOptionsCategoryHTML($var->items,$pre,$parent_id);
                }
            }
        }
        return $str;
    }

    public static function listOrder($id=0,$parent_id=0,$listAll=true) {
        if(!$listAll) {
            $parent = Category::orderBy('order')->where('status',1)->where('product_category_parent_id','=', $parent_id)->where('product_category_id','!=',$id)->get();
        }
        else {
            $parent = Category::orderBy('order')->where('product_category_parent_id','=', $parent_id)->where('product_category_id','!=',$id)->get();
        }
        $category = array();
        if(!$parent->isEmpty()) {
            foreach($parent as $key => $var) {
                $category[$key] = new \stdClass();
                $category[$key]->product_category_id = $var->product_category_id;
                $category[$key]->product_category_name = $var->product_category_name;
                $category[$key]->product_category_parent_id = $var->product_category_parent_id;
                $category[$key]->items = self::listOrder($id,$var->product_category_id,$listAll);
            }
        }
        return $category;
    }

}
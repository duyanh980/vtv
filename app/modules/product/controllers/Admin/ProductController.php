<?php namespace App\Modules\Product\Controllers\Admin;
use App\Modules\Product\Models\Category;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\ProductCategory;
use Auth, View, Lang, Monster, Datatable, Input, Validator, Asset;
use Helper\Helper;
use Illuminate\Support\Facades\Redirect;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/18/14
 * Time: 8:43 AM
 */

class ProductController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        return View::make('product::admin.list');
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Product::whereIn('product_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Product::all())
                ->showColumns('product_name')
                ->searchColumns('product_price')
                ->orderColumns('product_name','product_price','latest_update')
                ->addColumn('product_category',function($item) {
                    $Categories = ProductCategory::join('product_categories','product_categories.product_category_id','=','product_category_links.product_category_id')->where('product_id','=',$item->product_id)->get();
                    $categoryName = '';
                    if(!$Categories->isEmpty()) {
                        foreach($Categories as $var) {
                            $categoryName .= "[".link_to_route('admin.product.category',$var->product_category_name,array($var->product_category_id)).'], ';
                        }
                        return substr($categoryName,0,-2);
                    }
                    return;
                })
                ->addColumn('product_price',function($item) {
                    return Helper::PriceFormat($item->product_price);
                })
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.product.edit',array($item->product_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->product_id;
                })
                ->make();
        }
    }

    public function getCreate() {
        Asset::container('footer')->add('jqtree', 'assets/plugin/jqTree/jqtree.css');
        Asset::container('footer')->add('tree', 'assets/plugin/jqTree/tree.jquery.js');

        return View::make('product::admin.update')
            ->with('category',Category::tree())
            ->with('listCategoryHtml',Category::buildOptionsCategoryHTML());
    }

    public function getEdit($id) {
        Asset::container('footer')->add('jqtree.css', 'assets/plugin/jqTree/jqtree.css');
        Asset::container('footer')->add('tree.jquery.js', 'assets/plugin/jqTree/tree.jquery.js');

        $product = Product::find($id);
        return View::make('product::admin.update')
            ->with('product',$product)
            ->with('category',Category::tree($id))
            ->with('listCategoryHtml',Category::buildOptionsCategoryHTML());
    }

    public function postUpdate($id=false) {
        if($this->checkValidator($id)) {
            $user = Product::firstOrNew(array('product_id'=>$id));
            $user->fill(Input::all());
            $user->user_id = Auth::user()->user_id;
            $user->save();

            ProductCategory::updateCategory($id,Input::get('category'));

            return Redirect::route('admin.product.edit',array($user->product_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.product.edit',array($id));
        }
        else {
            return Redirect::to(route('admin.product.create'))->withInput();
        }
    }

    private function checkValidator($id=false) {

        $rules = Product::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }

}
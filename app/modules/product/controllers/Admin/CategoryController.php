<?php namespace App\Modules\Product\Controllers\Admin;
use View, Datatable, Input, Monster, Validator, Lang, Redirect, Auth, Asset;
use App\Modules\Product\Models\Category;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/19/14
 * Time: 2:48 PM
 */

class CategoryController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Category::whereIn('product_category_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Category::all())
                ->showColumns('product_category_name')
                ->searchColumns('product_category_name')
                ->orderColumns('product_category_id','product_category_name','latest_update','status')
                ->addColumn('status',function($item) {
                    return Monster::status($item->product_category_status);
                })
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.product.category',array($item->product_category_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->product_category_id;
                })
                ->make();
        }
    }

    public function getList() {
        return View::make('product::admin.category.list');
    }

    public function getManager($item_id=false) {
        Asset::container('footer')->add('jquery.nestable.css', 'assets/plugin/nestable/jquery.nestable.css');
        Asset::container('footer')->add('jquery.nestable.js', 'assets/plugin/nestable/jquery.nestable.js');

        $item = Category::find($item_id);
        return View::make('product::admin.category.manager')->with('item',$item)->with('menuItemHtml',Category::buildListMenuItemHtml(0));
    }

    public function postUpdateItem($item_id=false) {
        if($this->checkItemValidator()) {
            $category = Category::firstOrNew(array('product_category_id'=>$item_id));
            $category->fill(Input::all());
            $category->user_id = Auth::user()->user_id;
            $category->save();
            $item_id = $category->category_id;
            return Redirect::route('admin.product.category',array($item_id))->withInput();
        }

        if($item_id) { // if is editing
            return Redirect::route('admin.product.category',array($item_id))->withInput();
        }
        else {
            return Redirect::route('admin.product.category')->withInput();
        }
    }

    public function postOrderItem() {
        $data = Input::get('categories');
        $this->doSorting($data);
    }

    public function getListTreeCate($post_id=false) {
        return Category::tree($post_id);
    }

    public function getSelectListTreeCate() {
        return Category::selecttree('');
    }

    public function postUpdateCatePost($id=false) {
        $name = Input::get('name');
        $parent_id = Input::get('product_category_parent_id');
        $category = Category::firstOrNew(array('product_category_id'=>$id));
        $category->product_category_name = $name;
        $category->product_category_parent_id = $parent_id;
        $category->order = 1;
        $category->user_id = Auth::user()->user_id;
        $category->save();
    }

    private function doSorting($data,$parent_id=0) {
        if(!empty($data)) {
            $i=0;
            foreach($data as $item) {
                ++$i;
                $menuItem = Category::find($item['id']);
                $menuItem->order = $i;
                $menuItem->product_category_parent_id = $parent_id;
                $menuItem->save();
                if(isset($item['children'])) {
                    $this->doSorting($item['children'],$item['id']);
                }
            }
        }
    }

    private function checkItemValidator() {
        $rules = Category::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if($validator->passes()) {
            Monster::set_message(Lang::get('monster.updateSuccess'),'success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }

}
<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/18/14
 * Time: 8:44 AM
 */

return array(
    'moduleManager'             =>  'Product Manager',
    'productName'               =>  'Name',
    'productPrice'              =>  'Price',
    'productSpecialPrice'       =>  'Special price',
    'productShortDescription'   =>  'Short description',
    'productDescription'        =>  'Description',
    'productSKU'                =>  'SKU',
    'name'                      =>  'Name',
    'slug'                      =>  'Slug',
    'productCategory'           =>  'Category',

    'addNewCategory'            =>  'Add new category',
    'hideForm'                  =>  'Hide form'
);
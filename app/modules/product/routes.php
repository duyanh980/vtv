<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/18/14
 * Time: 8:41 AM
 */


//back end
Route::group(array('prefix'=>'admin', 'before'=>'auth.admin', 'namespace'=>'App\\Modules\\Product\\Controllers\\Admin'),function() {
    Route::get('product',                  array('before'  =>  'auth.restrict',    'as'   =>    'admin.product.list',        'uses'  =>  'ProductController@getList'));
    Route::get('product/dataTable',        array('before'  =>  'auth.restrict',    'as'   =>    'admin.product.dataTable',   'uses'  =>  'ProductController@getDataTable'));
    Route::get('product/create',           array('before'  =>  'auth.restrict',    'as'   =>    'admin.product.create',      'uses'  =>  'ProductController@getCreate'));
    Route::get('product/edit/{id?}',       array('before'  =>  'auth.restrict',    'as'   =>    'admin.product.edit',        'uses'  =>  'ProductController@getEdit'));
    Route::post('product/update/{id?}',    array('before'  =>  'auth.restrict',    'as'   =>    'admin.product.update',      'uses'  =>  'ProductController@postUpdate'));
});

// category
Route::group(array('prefix'=>'admin', 'before'=>'auth.admin','namespace'=>'App\\Modules\\Product\\Controllers\\Admin'),function() {
    Route::pattern('cate_id', '[0-9]+');
    Route::pattern('post_id', '[0-9]+');
    Route::get('product/category',                               array('as'   =>  'admin.product.category.list',               'uses'  =>  'CategoryController@getList'));
    Route::get('product/category/dataTable',                     array('as'   =>  'admin.product.category.dataTable',          'uses'  =>  'CategoryController@getDataTable'));
    Route::get('product/category/edit/{cate_id?}',               array('as'   =>  'admin.product.category',                    'uses'  =>  'CategoryController@getManager'));
    Route::get('product/category/listCate/{post_id?}',           array('as'   =>  'admin.product.category.listCate',           'uses'  =>  'CategoryController@getListTreeCate'));
    Route::get('product/category/selectListCate',                array('as'   =>  'admin.product.category.selectListCate',     'uses'  =>  'CategoryController@getSelectListTreeCate'));
    Route::post('product/category/update/order',                 array('as'   =>  'admin.product.category.order',              'uses'  =>  'CategoryController@postOrderItem'));
    Route::post('product/category/update/{cate_id?}',            array('as'   =>  'admin.product.category.update',             'uses'  =>  'CategoryController@postUpdateItem'));
    Route::post('product/category/update/post',                  array('as'   =>  'admin.product.category.update.post',        'uses'  =>  'CategoryController@postUpdateCatePost'));

});
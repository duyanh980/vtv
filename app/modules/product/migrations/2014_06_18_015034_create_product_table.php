<?php

use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function($table)
		{
            $table->increments('product_id');
            $table->string('product_name',250);
            $table->string('product_slug',250);
            $table->decimal('product_price',15,2);
            $table->decimal('product_special_price',15,2);
            $table->string('product_image',250);
            $table->string('product_sku',250);
            $table->text('product_short_description');
            $table->longText('product_description');
            $table->integer('product_category_id')->default(0);
            $table->integer('user_id')->default(1);
            $table->tinyInteger('product_status')->default(1);
            $table->timestamps();

            $table->index('product_slug');
            $table->index('product_name');
            $table->index('product_price');
            $table->index('product_category_id');

            Schema::create('product_categories',function($table) {
                $table->increments('product_category_id');
                $table->string('product_category_name',250);
                $table->string('product_category_slug',250);
                $table->longText('product_category_description');
                $table->integer('product_category_parent_id')->default(0);
                $table->integer('user_id')->default(1);
                $table->integer('order')->default(0);
                $table->tinyInteger('product_category_status')->default(1);
                $table->timestamps();

                $table->index('product_category_name');
                $table->index('product_category_slug');

                Schema::create('product_category_links',function($table) {
                    $table->integer('product_id');
                    $table->integer('product_category_id');
                    $table->index(array('product_id','product_category_id'));
                });
            });

            Schema::create('product_images',function($table) {
                $table->integer('product_id');
                $table->string('product_image');

                $table->index('product_id');
            });

            Schema::create('product_attributes',function($table) {
                $table->integer('product_id');
                $table->string('key');
                $table->string('value');

                $table->index('product_id');
            });
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
        Schema::drop('product_categories');
        Schema::drop('product_category_links');
        Schema::drop('product_images');
        Schema::drop('product_attributes');
	}

}

<?php namespace App\Modules\Product\Seeds;
use App\Modules\Menu\Models\MenuItem;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/18/14
 * Time: 8:41 AM
 */



class DatabaseSeeder extends \Seeder {

    public function run() {
        $productItem = MenuItem::create(array(
            'menu_item_name'    =>  'Product',
            'menu_item_url'     =>  '/admin/product',
            'menu_item_class'   =>  'fa-shopping-cart',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  3
        ));

        MenuItem::create(array(
            'menu_item_name'    =>  'Products',
            'menu_item_url'     =>  '/admin/product',
            'menu_item_class'   =>  'fa-shopping-cart',
            'menu_item_parent_id'   =>  $productItem->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1
        ));

        MenuItem::create(array(
            'menu_item_name'    =>  'Categories',
            'menu_item_url'     =>  'admin/product/category',
            'menu_item_parent_id'   =>  $productItem->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  2
        ));
    }
}

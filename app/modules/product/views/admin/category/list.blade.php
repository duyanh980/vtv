@extends('admin.main')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">{{Lang::get('product::monster.moduleManager')}}</header>
            <div class="panel-body">
                <section class="unseen">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ route('admin.product.category') }}" class="btn btn-default btn-primary"><i class="fa fa-plus"></i> {{Lang::get('monster.addNew')}}</a>
                        </div>
                    </div>

                    <section class="adv-table" id="flip-scroll">
                        <table class="kTable table table-bordered table-striped table-condensed cf" data-source="{{ route('admin.product.category.dataTable') }}">
                            <thead>
                            <tr>
                                <th>{{Lang::get('product::monster.productName')}}</th>
                                <th>{{Lang::get('product::monster.productPrice')}}</th>
                                <th style="width:16em;">{{Lang::get('monster.latestUpdate')}}</th>
                                <th style="width: 10em;"></th>
                            </tr>
                            </thead>
                        </table>
                    </section>
                </section>
            </div>
        </section>
    </div>
</div>
@stop
@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.product.update',array((isset($product) ? $product->product_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading"></header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('product::monster.productName')}}</label>
                            <div class="col-lg-10">
                                {{ Form::text('product_name',Input::old('product_name', isset($product) ? $product->product_name : '' ), array('id' => 'product_name', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('product::monster.productSKU')}}</label>
                            <div class="col-lg-10">
                                {{ Form::text('product_sku',Input::old('product_sku', isset($product) ? $product->product_sku : '' ), array('id' => 'product_sku', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('product::monster.productPrice')}}</label>
                            <div class="col-lg-10">
                                {{ Form::text('product_price',Input::old('product_price', isset($product) ? $product->product_price : '' ), array('id' => 'product_price', 'class'=>'form-control validate[required,custom[number]]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('product::monster.productSpecialPrice')}}</label>
                            <div class="col-lg-10">
                                {{ Form::text('product_special_price',Input::old('product_special_price', isset($product) ? $product->product_special_price : '' ), array('id' => 'product_special_price', 'class'=>'form-control validate[custom[number]]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('product::monster.productShortDescription')}}</label>
                            <div class="col-lg-10">
                                {{ Form::textarea('product_short_description',Input::old('product_short_description', isset($product) ? $product->product_short_description : '' ), array('id' => 'product_short_description', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('product::monster.productDescription')}}</label>
                            <div class="col-lg-10">
                                {{ Form::textarea('product_description',Input::old('product_description', isset($product) ? $product->product_description : '' ), array('id' => 'product_description', 'class'=>'form-control validate[required] ckeditor')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.publish') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-xs-12"><span class="pull-left">{{ Lang::get('monster.status') }}</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('product_status',array('0'=>Lang::get('monster.draft'), '2' => Lang::get('monster.pendingReview'), '1'=> Lang::get('monster.publish')),Input::old('status', isset($product) ? $product->product_status : '' ), array('id' => 'product_status', 'class'=> 'form-control validate[required]')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($product) ? $product->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                            {{ Lang::get('monster.modifiedOn') }}: {{ isset($product) ? $product->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.product.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('post::monster.categories') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div id="treecate" class="filetree" data-url="{{ route('admin.post.list.category')}}"></div>
                        <div class="col-xs-12">
                            <div id="displayListCate">@if (isset($category)) {{ $category }} @endif</div>
                            <div class="add_new_category show_hide_item" >{{ Lang::get('post::monster.addNewCategory') }}</div>

                            <div id="add_new_category">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">{{ Lang::get('product::monster.name') }}</label>
                                    <div class="col-lg-10">
                                        {{Form::text('product_category_name', '',array('id' => 'product_category_name', 'class'=> 'form-control validate[required]'))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">{{ Lang::get('product::monster.category') }}</label>
                                    <div class="col-lg-10">
                                        <select name="product_category_parent_id" id="product_category_parent_id" class="form-control validate[required]">
                                            {{ $listCategoryHtml }}
                                        </select>
                                    </div>
                                </div>
                                <button type="button" name="save" class="btn btn-primary btn-update-category">{{ Lang::get('monster.save') }}</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('post::monster.featuredImage') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12">

                            <div class="fileupload {{ (isset($product) AND is_file($product->product_image)) ? 'fileupload-exists' : 'fileupload-new' }}">
                                @if(isset($product) AND is_file($product->image))
                                <img src="{{ url($product->image) }}" id="product_image_preview" class="image_preview" />
                                @endif
                                <input type="hidden" id="product_image" name="product_image" />
                                <a href="{{ route('admin.media.popup.manager') }}?type=single-image" class="btn btn-white btn-file open-popup-ajax">
                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select image</span>
                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                </a>
                                <a href="#" class="btn btn-danger fileupload-exists remove-image" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>


</div>
{{ Form::close() }}

<script type="text/javascript">
$(document).ready(function() {
    $(document).bind("addFile",function() {
        $('#product_image').val(ImageSelected.media_file_url);
        if($('#product_image_preview').get(0)) {
            $('#product_image_preview').attr('src',ImageSelected.media_file_full_url);
        }
        else {
            $('.fileupload').prepend('<img src="' + ImageSelected.media_file_full_url + '" id="product_image_preview" class="image_preview" />');
        }
        $('.fileupload').removeClass('fileupload-new').addClass('fileupload-exists');
    });


    $('.remove-image').on("click",function() {
        $('#product_image').val('');
        $('#product_image_preview').remove();
        $('.fileupload').removeClass('fileupload-exists').addClass('fileupload-new');
    });

    $(document).on('click','.btn-update-category',function() {
        if ($('#product_category_name').val()) {
            var product_category_parent_id = $('#product_category_parent_id').val();
            var product_category_name = $("#product_category_name").val();
            $("#product_category_name").val('');

            $.ajax({
                type:'post',
                url:"{{ route('admin.product.category.update.post') }}",
                data: { name :  product_category_name, product_category_parent_id : product_category_parent_id },
                success: function() {
                    $("#displayListCate").load("{{ route('admin.product.category.listCate',array((isset($product) ? $product->product_id : ''))) }}");
                    $("#displaySelectListCate").load("{{ route('admin.product.category.selectListCate') }}");
                }
            })

        } else {
            $('#category-name').focus();
        }
    });
    var num_click = 0;
    $('.add_new_category').click(function() {
        $('#add_new_category').slideToggle();
        ++num_click;
        if(num_click%2==0) {
            $(this).html('{{ Lang::get('product::monster.addNewCategory') }}');
        }
        else {
            $(this).html('{{ Lang::get('product::monster.hideForm') }}');
        }
    });

});

</script>
@stop
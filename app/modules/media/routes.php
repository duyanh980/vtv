<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/29/14
 * Time: 11:50 PM
 */

Route::group(array('prefix'=>'admin', 'before'=>'auth.admin','namespace'=>'App\\Modules\\Media\\Controllers\\Admin'),function() {

    Route::get('media',                  array('before'  =>  'auth.restrict',    'as'   =>    'admin.media.list',        'uses'  =>  'MediaController@getList'));
    Route::get('media/dataTable',        array('before'  =>  'auth.restrict',    'as'   =>    'admin.media.dataTable',   'uses'  =>  'MediaController@getDataTable'));
    Route::get('media/edit/{id?}',       array(                                  'as'   =>    'admin.media.edit',        'uses'  =>  'MediaController@getEdit'));

    Route::get('media/popup',               array('as'   =>  'admin.media.popup.manager',        'uses'  =>  'MediaController@getPopupManager'));
    Route::get('media/listDirectory/{id?}',               array('as'   =>  'admin.media.popup.list.directory',        'uses'  =>  'MediaController@getPopupListDirectory'));
    Route::get('media/setDirectory/{id?}',         array('as'  =>  'admin.media.popup.set.directory',     'uses'  =>  'MediaController@setDirectory'));
    Route::post('media/uploader', array('as'    =>  'admin.media.popup.uploader',   'uses'  =>  'MediaController@postUploader'));
    Route::get('media/libraries', array('as'    =>  'admin.media.popup.media.libraries',   'uses'  =>  'MediaController@getLibraries'));
    Route::post('media/deleteMedia', array('as' =>  'admin.media.popup.delete',    'uses'  =>  'MediaController@deleteMedia'));
    Route::post('media/folder/create', array('as' =>  'admin.media.folder.create',    'uses'  =>  'MediaController@postCreateFolder'));
});
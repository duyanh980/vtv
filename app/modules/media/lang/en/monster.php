<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/30/14
 * Time: 12:10 AM
 */

return array(
    'moduleManager'  =>  'Media Manager',
    'upload'        =>  'Upload',
    'manager'       =>  'Manager',
    'selectAll'     =>  'Select All',
    'newFolder'     =>  'New Folder',
    'delete'        =>  'Delete',

    'mediaName'     =>  'Name',
    'dimensions'    =>  'Dimension',
    'size'          =>  'Size',
    'author'        =>  'Author'
);
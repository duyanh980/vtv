<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('media_directories', function(Blueprint $table)
        {
            $table->increments('directory_id');
            $table->string('directory_name',250);
            $table->integer('directory_parent_id')->default(0);
            $table->double('directory_size')->default(0);
            $table->integer('status')->default(1);
            $table->integer('user_id');
            $table->timestamps();
            $table->softDeletes();
            Schema::create('media',function(Blueprint $table) {
                $table->bigIncrements('media_id');
                $table->string('media_name',250);
                $table->string('media_file_name',250);
                $table->string('media_file_url',1000);
                $table->double('media_file_size')->default(0);
                $table->string('media_file_type',50)->default('image');
                $table->integer('media_file_width')->default(0);
                $table->integer('media_file_height')->default(0);
                $table->integer('media_directory_id')->default(0);
                $table->integer('status')->default(1);
                $table->integer('user_id');
                $table->timestamps();
                $table->softDeletes();
            });
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media');
        Schema::drop('media_directories');
	}

}

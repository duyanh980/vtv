<?php namespace App\Modules\Media\Seeds;
use App\Modules\Media\Models\MediaDirectory;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/30/14
 * Time: 11:12 PM
 */


class DatabaseSeeder extends \Seeder {

    public function run() {
        MediaDirectory::truncate();
        MediaDirectory::create(array(
            'directory_name'    =>  'Images',
            'user_id'           =>  1
        ));
        MediaDirectory::create(array(
            'directory_name'   =>  'Other',
            'user_id'           =>  1
        ));
    }
}
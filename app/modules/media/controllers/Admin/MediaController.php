<?php namespace App\Modules\Media\Controllers\Admin;
use App\Modules\Media\Models\Media;
use App\Modules\Media\Models\MediaDirectory;
use App\Modules\System\Models\System;
use App\Modules\User\Libraries\Auth;
use App\Modules\User\Models\User;
use Helper\Helper;
use Monster\Monster;
use Validator;
use View, Input, Uploader\Uploader, Datatable, Lang;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/29/14
 * Time: 11:49 PM
 */

class MediaController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Media::whereIn('user_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Media::all())
                ->showColumns('media_name')
                ->addColumn('dimensions',function($item) {
                    return $item->media_file_width.'x'.$item->media_file_height;
                })
                ->addColumn('size',function($item) {
                    return Helper::fileSize($item->media_file_size);
                })
                ->addColumn('author',function($item) {
                    return User::name($item->user_id,true,true);
                })
                ->addColumn('action',function($item) {
                    return '<img src="' . url($item->media_file_url) . '" style="max-width:80px;max-height:80px;" />';
                })
                ->setRowID(function($item) {
                    return $item->media_id;
                })
                ->orderColumns('media_name','size','author')
                ->make();
        }
    }
    public function getList() {
        return View::make('media::admin.list');
    }

    /*POPUP PROCESS*/
    public function getPopupManager() {
        return View::make('media::admin.popup.manager');
    }

    public function getPopupListDirectory() {
        $id =0;
        if(Input::get('node')>0) {
            $id=Input::get('node');
        }
        $directories = MediaDirectory::where('directory_parent_id',$id)->where('user_id',Auth::user()->user_id)->where('status',1)->get();
        $data = array();
        if(!empty($directories)) {
            $key=0;
            foreach($directories as $directory) {
                $data[$key] = new \stdClass();
                $data[$key]->id = $directory->directory_id;
                $data[$key]->label = $directory->directory_name;
                $data[$key]->children = array();
                $hasChildren = MediaDirectory::where('directory_parent_id',$directory->directory_id)->where('status',1)->get();
                if(!$hasChildren->isEmpty()) {
                    $data[$key]->load_on_demand = true;
                    $data[$key]->children = $hasChildren;
                }
                else {
                    $data[$key]->load_on_demand = false;
                }
                ++$key;
            }
        }
        if($id==0 AND Input::get('node')!=='0') {
            $listDirectory[0] =  new \stdClass();
            $listDirectory[0]->id = 0;
            $listDirectory[0]->label = 'Root';
            $listDirectory[0]->children = $data;
            $listDirectory[0]->load_on_demand = false;
            $listDirectory[0]->isRoot = true;
        }
        else {
            $listDirectory = $data;
        }
        return json_encode($listDirectory);
    }

    public function postCreateFolder() {
        $directory = MediaDirectory::findOrNew(Input::get('directory_id'));
        if($directory->exists) {
            if($directory->user_id!=Auth::user()->user_id) {
                $directory = new MediaDirectory();
            }
        }
        if(Input::get('type')=='delete') {
            $directory->delete();
            Media::where('media_directory_id',$directory->directory_id)->delete();
            $response = array('status'=>true,'message'=>'Item deleted');
            return json_encode($response);
        }
        if(Input::get('directory_id')==0) {
            $directory->directory_parent_id = Input::get('directory_parent_id');
        }
        if($this->checkValidator()) {
            $directory->directory_name = Input::get('directory_name');
            $directory->status = 1;
            $directory->user_id = Auth::user()->user_id;
            $directory->save();
            $response = array('status'=>true,'message'=>Monster::message());
        }
        else {
            $response = array('status'=>false,'message'=>Monster::message());
        }
        return $response;
    }

    private function checkValidator() {

        $rules = MediaDirectory::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }

    public function postUploader() {
        $directory = MediaDirectory::where('directory_id',Input::get('directory'))->pluck('directory_id');
        if(!$directory) {
            $directory = 0;
        }
        $config = array(
            'path'  =>  '',
            'type'  =>  'jpg|png',
            'size'  =>  System::item('upload.size')
        );
        if(Uploader::upload($config,'file')) {
            $data = Uploader::fileInfo();
            $media = new Media();
            $media->fill($data);
            $media->media_directory_id = $directory;
            $media->user_id = Auth::user()->user_id;
            $media->save();
            $data['media_id'] = $media->media_id;
            $data['media_file_full_url'] = url($media->media_file_url);
            $data['updated_at'] =   $media->updated_at->diffForHumans();

            $response = array(
                'status'    =>  true,
                'data'      =>  $data
            );
        }
        else {
            $response = array(
                'status'    =>  false,
                'message'   =>  Uploader::error()
            );
        }
        return json_encode($response);
    }

    public function getLibraries() {
        $directory_id = Input::get('directory');
        $getMedia = Media::query();
        if($directory_id>0) {
            $getMedia->where('media_directory_id',$directory_id);
        }
        $media = $getMedia->where('user_id',Auth::user()->user_id)->skip(Input::get('offset'))->take(Input::get('limit'))->orderBy('updated_at')->get();
        $data = array();
        if(!$media->isEmpty()) {
            foreach($media as $var) {
                $k = $var->media_id;
                $data[$k] = new \stdClass();
                $data[$k]->media_id         =   $var->media_id;
                $data[$k]->media_file_full_url   =   url($var->media_file_url);
                $data[$k]->media_file_url   =   $var->media_file_url;
                $data[$k]->media_name       =   $var->media_name;
                $data[$k]->media_file_name  =   $var->media_file_name;
                $data[$k]->media_file_size  =   $var->media_file_size;
                $data[$k]->media_file_width =   $var->media_file_width;
                $data[$k]->media_file_height=   $var->media_file_height;
                $data[$k]->updated_at       =   $var->updated_at->diffForHumans();
                if($var->media_file_width>$var->media_file_height) {
                    $data[$k]->media_file_view_mode = 'landscape';
                }
                else {
                    $data[$k]->media_file_view_mode = 'portrait';
                }
            }
            $response = array('status'=>true,'data'=>$data);
        }
        else {
            $response = array('status'=>false,'message'=>'Media library is empty');
        }
        return json_encode($response);
    }

    public function deleteMedia() {
        $media = Media::find(Input::get('media_id'));
        if($media And $media->user_id == Auth::user()->user_id) {
            if(is_file($file = public_path($media->media_file_url))) {
                unlink($file);
            }
            $media->delete();
        }
    }
}
@extends('admin.main')


@section('content')

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">{{Lang::get('media::monster.moduleManager')}}</header>
            <div class="panel-body">
                <section class="unseen">
                    <div class="row">
                        <div class="col-xs-12">

                        </div>
                    </div>

                    <section class="adv-table" id="flip-scroll">
                        <table class="kTable table table-bordered table-striped table-condensed cf" data-source="{{ route('admin.media.dataTable') }}">
                            <thead>
                            <tr>
                                <th>{{ Lang::get('media::monster.mediaName') }}</th>
                                <th>{{ Lang::get('media::monster.dimensions') }}</th>
                                <th>{{ Lang::get('media::monster.size') }}</th>
                                <th>{{ Lang::get('media::monster.author') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </section>
                </section>
            </div>
        </section>
    </div>
</div>
@stop
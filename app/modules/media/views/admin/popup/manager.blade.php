@extends('admin.popup')

@section('content')
<section class="panel" id="mediaManager">
    <header class="panel-heading">{{ Lang::get('media::monster.mediaManager') }}</header>
    <ul class="nav nav-tabs">
        <li><a href="#drop" data-toggle="tab">{{ Lang::get('media::monster.upload') }}</a></li>
        <li class="active"><a href="#manager" data-toggle="tab">{{ Lang::get('media::monster.manager') }}</a></li>
    </ul>

    <div class="panel-body" id="panel-body">
        <div class="tab-content">
            <div id="drop" class="tab-pane row">
                <div class="col-xs-12">
                    <table style="width: 100%;height:100%;">
                        <tr vaglign="middle">
                            <td>Drop files anywhere to upload<br /><a id="uploader">Browse</a></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="manager" class="tab-pane active row">
                <div class="col-xs-12 col-sm-2">
                    <div class="mScrollBar hidden-xs">
                        <ul id="ListDirectories" class="filetree" data-url="{{ route('admin.media.popup.list.directory')}}"></ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 mScrollBarFile">
                    <ul id="gallery">
                    </ul>
                    <div class="loading"><button class="btn btn-primary" type="button">Loading...</button></div>
                </div>
                <div class="col-xs12 col-sm-3 mScrollBar hidden-xs">
                    <section class="panel">
                        <div id="message"></div>
                        <header class="panel-heading atachment_details">Attachment Details</header>
                        <div class="panel-body atachment_details">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="thumbnail-preview">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" />
                                    </div>
                                </div>
                                <div class="form-group" id="fileInfo">

                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right"><button class="btn btn-primary addFiles" id="<?php echo (Input::get('input') ? Input::get('input') : 'addFiles'); ?>">Add files selected</button> </div>
            </div>
        </div>
    </div>
    <div id="drop-window">
        <table style="height:100%;width:100%">
            <tr valign="middle">
                <td align="center"><h4>Drop files to upload</h4></td>
            </tr>
        </table>
    </div>

</section>




<!-- Modal -->
<div class="modal fade" id="addFolder" tabindex="-2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Folder</h4>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <div class="form-horizontal">
                        <div class="message_modal_folder"></div>
                        <div class="form-group">
                            <label class="control-label col-xs-12"><span class="pull-left">Folder</span></label>
                            <div class="col-xs-12">
                                <input type="text" name="directory_name" id="directory_name" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="updateFolder">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!-- modal -->

{{ HTML::style('assets/plugin/jqTree/jqtree.css') }}
{{ HTML::script('assets/plugin/jqTree/tree.jquery.js') }}

{{ HTML::style('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.css') }}
{{ HTML::script('assets/plugin/mCustomScrollbar/jquery.mousewheel.min.js') }}
{{ HTML::script('assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.js') }}
{{ HTML::script('assets/plugin/plupload/plupload.js') }}
{{ HTML::script('assets/plugin/magnific/jquery.magnific-popup.js') }}
<script type="text/javascript">
var ImageSelected = new Object();
var ListImageSelected = [];
    $(document).ready(function() {
        Array.prototype.clean = function(deleteValue) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] == deleteValue) {
                    this.splice(i, 1);
                    i--;
                }
            }
            return this;
        };


        @if(Input::get('type')=='multiple-images')
            var MultipleImage = true;
        @else
                var MultipleImage = false;
        @endif
        $(".mScrollBar").mCustomScrollbar();
        var setHeight = function() {
            $('#mediaManager').height($(window).height()-60);
            $('#panel-body').height($('#mediaManager').height()-$('.panel-heading').height() - $('.nav').height() - 126);
            $('.mScrollBar').height($('.panel-body').height()-30);
            $('.mScrollBarFile').height($('.panel-body').height()-30);
            $('#drop').height($('.mScrollBar').height());
            $('#drop').find('table').height($('.mScrollBar').height());
        }
        setHeight();
        $(window).resize(function() {
            setHeight();
        });
            /** action **/
        var directory = 0;
        var MediaLibraries = [];
        var $container = $('#gallery');
        var limit = 20;
        var offset = 0;
        var isLoading = false;
        var mediaLoading=false;
        $(".mScrollBarFile").mCustomScrollbar({
            callbacks:{
                whileScrolling:function(){
                    if(mcs.topPct==100) {
                        mediaLoading = loadingMediaLibraries(directory,false);
                    }
                }
            }
        });
        $('#ListDirectories').tree();
        $('#ListDirectories').bind(
            'tree.select',
            function(event) {
                if (event.node) {

                    if(mediaLoading)
                        mediaLoading.abort();
                        if(directory==event.node.id)
                            mediaLoading = loadingMediaLibraries(event.node.id);
                        else
                            mediaLoading = loadingMediaLibraries(event.node.id,true);
                        directory = event.node.id;
                }
                else {
                    directory = 0;
                    mediaLoading = loadingMediaLibraries(directory,true);
                }
            }
        );
        $('#ListDirectories').bind(
            'keith.addFolder',
            function(event) {
                if(event.node) {
                    $('#addFolder').modal('show');
                    $('#updateFolder').unbind("click");
                    $('#updateFolder').bind("click",function() {
                        updateFolder(event.node,'insert');
                    });
                }
            }
        );

        $('#ListDirectories').bind(
            'keith.removeFolder',
            function(event) {
                if(event.node) {
                    if(confirm("Are you sure?"))
                    updateFolder(event.node,'delete');
                }
            }
        );

        $('#ListDirectories').bind(
            'keith.editFolder',
            function(event) {
                if(event.node) {
                    $('#addFolder').modal('show');
                    $('#directory_name').val(event.node.name);
                    $('#updateFolder').unbind("click");
                    $('#updateFolder').bind("click",function() {
                        updateFolder(event.node,'update');
                    });
                }
            }
        );


        var updateFolder = function(node,type) {
            var directory_id = (type=='insert') ? 0 : node.id;
            $.ajax({
                type: 'post',
                url : '{{ route("admin.media.folder.create") }}',
                dataType: 'json',
                data: { type: type, directory_id: directory_id, directory_name: $('#directory_name').val(), directory_parent_id: node.id },
                success: function(res) {
                    if(res.status) {
                        if(type=='insert') {
                            $('#ListDirectories').tree(
                                'loadDataFromUrl',
                                '{{route("admin.media.popup.list.directory")}}?node='+node.id,
                                node,
                                function() {}
                            );
                        }
                        else if(type=='update') {
                            $('#ListDirectories').tree(
                                'renameNode',
                                node,$('#directory_name').val()
                            );
                        }
                        else {
                            $('#ListDirectories').tree(
                                'removeNode',
                                node
                            );
                        }
                        $('#addFolder').modal('hide');
                        $('.message_modal_folder').html('');
                    }
                    else {
                        $('.message_modal_folder').html(res.message);
                    }
                }
            });
        };
        var removeImageSelected = function(item='') {
            ImageSelected = new Object();
            $('.atachment_details').hide();
            if(MultipleImage) {
                $.each(ListImageSelected,function(i) {
                    if(this.media_id == item) {
                        delete ListImageSelected[i];
                        ListImageSelected.clean(undefined);
                    }
                });
                if(ListImageSelected.length==0) {
                    $('.addFiles').attr('disabled','disabled');
                }
            }
            else {
                $('.addFiles').attr('disabled','disabled');
            }
        };

        var selectOtherImage = function() {
                removeImageSelected();
        }

        var loadingMediaLibraries = function(directory,isNew) {
            if(isLoading && !isNew) {
                return;
            }
            isLoading = true;
            $('.loading').find('.btn').removeClass('btn-default').addClass('btn-primary');
            $('.loading').find('.btn').html('Loading...');
            $('.loading').show();
            if(isNew) {
                if(mediaLoading)
                    mediaLoading.abort();
                offset = 0;
                $container.html('');
                MediaLibraries = [];
                ListImageSelected = [];
                removeImageSelected();

            }
            return $.ajax({
                url: '{{route("admin.media.popup.media.libraries")}}',
                data: { directory : directory, limit:limit, offset: offset},
                dataType: 'json',
                success: function(res) {
                    if(res.status) {
                        $('.loading').hide();
                        isLoading = false;
                        offset += limit;
                        var html = '';
                        MediaLibraries = $.extend(MediaLibraries,res.data);
                        $.each(res.data,function() {
                            html = '<li class="attachment">'+
                                        '<div class="attachment-preview ' + this.media_file_view_mode + '">'+
                                            '<div class="thumbnail-image" data-media-id="' + this.media_id + '">'+
                                            '<div class="centered"><img src="' + this.media_file_full_url + '" /></div>'+
                                        '</div>'+
                                            '<a class="check" href="#" title="Deselect">'+
                                                '<i class="fa fa-check"></i>'+
                                            '</a>'+
                                        '</div>'+
                                    '</li>';
                            $container.append($(html));
                        });
                        effectAfterAjax();
                    }
                    else {
                        if(isNew) {
                            $('.loading').find('.btn').removeClass('btn-primary').addClass('btn-default');
                            $('.loading').find('.btn').html('Folder empty');
                        }
                        else {
                            $('.loading').hide();
                        }
                    }
                }
            });
        }

        mediaLoading = loadingMediaLibraries(directory,true);
        var effectAfterAjax = function() {
            /** effect **/
            $('#gallery .thumbnail-image').unbind('click');
            $('#gallery li .check').unbind('click');
            $('#gallery .check').unbind('mouseover');
            $('#gallery .check').unbind('mouseout');

            $('#gallery .thumbnail-image').bind("click",function() {
                if($(this).parent().parent().hasClass('selected') || $(this).parent().parent().hasClass('selected-before')) {
                    $(this).parent().parent().removeClass('selected');
                    $(this).parent().parent().removeClass('selected-before');
                    removeImageSelected($(this).data('media-id'));
                } else {
                    if(MultipleImage) {
                        $('#gallery li.selected').addClass('selected-before');
                    }
                    $('#gallery li').removeClass('selected');
                    $(this).parent().parent().addClass('selected');
                    selectImage(this);
                }
            });

            $('#gallery li .check').bind("click",function() {
                $(this).parent().parent().removeClass('selected');
            });
            $('#gallery .check').bind("mouseover",function() {
                $(this).find('.fa').removeClass('fa-check').addClass('fa-minus');
            });
            $('#gallery .check').bind("mouseout",function() {
                $(this).find('.fa').removeClass('fa-minus').addClass('fa-check');
            });

        };

        var selectImage = function(element) {
            var media_id = $(element).data('media-id');
            ImageSelected = MediaLibraries[media_id];
            ListImageSelected[media_id] = MediaLibraries[media_id];
            $('.thumbnail-preview img').attr('src',ImageSelected.media_file_full_url);
            $('#fileInfo').html( ImageSelected.media_file_name +'<br />' +
                                ImageSelected.updated_at +'<br />' +
                                '<a href="#" class="removeMedia text-primary">Delete Permanently</a>');

            $('.addFiles').removeAttr('disabled');
            $('.removeMedia').on("click",function() {
                deleteMedia(ImageSelected);
            });
            $('.atachment_details').show();
            ListImageSelected.clean(undefined);
        };

        var deleteMedia = function(ImageSelected) {
            removeImageSelected(ImageSelected.media_id);
            $('li.selected').remove();
            $.ajax({
                type : 'post',
                url  : '{{route("admin.media.popup.delete")}}',
                data : { media_id: ImageSelected.media_id }
            });
        };



        var uploader = new plupload.Uploader({
            runtimes : 'html5,flash,html4',
            browse_button : 'uploader',
            drop_element    :   'mediaManager',
            url : "{{ route('admin.media.popup.uploader') }}",
            flash_swf_url : '<?php echo url('assets/plugin/plupload/Moxie.swf'); ?>',
            chunk_size: '2mb',
            multipart_params : {directory: 0},
            filters : {
                max_file_size : '2mb',
                mime_types: [
                    {title : "Image files", extensions : '{{System::item("upload.image")}}'},
                    /*
                    {title : "Document files", extensions : '{{System::item("upload.document")}}'},
                    {title : "Media files", extensions : '{{System::item("upload.media")}}' },
                    */
                ]
            },

            init: {
                FilesAdded: function(up, files) {
                    uploader.settings.multipart_params.directory = directory;
                    $('.nav li').removeClass('active');
                    $('.nav li a[href="#manager"]').parent().addClass('active');
                    $('.tab-pane').removeClass('active');
                    $('#manager').addClass('active');
                    if(!MultipleImage)
                    $('.attachment').removeClass('selected');
                    var html = '';
                    for(var i=0;i<files.length;i++) {
                    var cls;
                    if(!MultipleImage) {
                        if((files.length-1) == i) {
                            cls = ' selected';
                        }
                        else {
                            cls = '';
                        }
                    }
                    else {
                        cls = ' selected';
                    }
                    html = '<li class="attachment' + cls + '">'+
                                '<div class="attachment-preview">'+
                                    '<div class="thumbnail-image uploading" data-media-id="" id="'+files[i].id+'">'+
                                        '<div class="centered"></div>'+
                                        '<div class="progress progress-xs">'+
                                            '<div class="progress-bar"></div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<a class="check" href="#" title="Deselect">'+
                                        '<i class="fa fa-check"></i>'+
                                    '</a>'+
                                '</div>'+
                            '</li>';
                        $container.prepend($(html));
                    }
                    uploader.start();
                },
                UploadProgress: function(up, file) {
                    $('#'+file.id).find('.progress-bar').css('width',file.percent+'%');
                },
                Error: function(up, err) {
                    $('.nav li').removeClass('active');
                    $('.nav li a[href="#manager"]').parent().addClass('active');
                    $('.tab-pane').removeClass('active');
                    $('#manager').addClass('active');
                    $('.attachment').removeClass('selected');
                    $('#message').prepend('<div class="alert alert-error fade in">'+
                                            '<button type="button" class="close close-sm" data-dismiss="alert">'+
                                            '<i class="fa fa-times"></i>'+
                                        '</button><strong>' + err.file.name + ':</strong> '+ err.message +'</div>');
                },
                FileUploaded:function(up,files,response)
                {

                    var res = JSON.parse(response.response);
                    if(res.status) {
                        $('.loading').hide();
                        var item = new Object();
                        var media_id = res.data.media_id;
                        item[media_id] = res.data;
                        ListImageSelected[media_id] = res.data;
                        $.extend(MediaLibraries,item);

                        $('#'+files.id).find('.progress').remove();
                        $('#'+files.id).attr('data-media-id',res.data.media_id);
                        $('#'+files.id).parent('.attachment-preview').addClass(res.data.media_file_view_mode);
                        $('#'+files.id).find('.centered').html('<img src="'+res.data.media_file_full_url+'" />');
                        selectImage('#'+files.id);
                        effectAfterAjax();
                    }
                    else {
                        $('#message').prepend('<div class="alert alert-error fade in">'+
                            '<button type="button" class="close close-sm" data-dismiss="alert">'+
                            '<i class="fa fa-times"></i>'+
                            '</button><strong>'+files.name+': </strong> '+ res.message +'</div>');
                        $('#'+files.id).parent().parent().remove();
                        selectOtherImage();
                    }
                }
            }
        });

        uploader.bind('Init', function(up, params) {
            if (uploader.features.dragdrop) {
                var target = $("#mediaManager");
                target.on("dragenter",function() {
                    $('#drop-window').show();
                });

                $('#drop-window').on("dragleave",function() {
                    $('#drop-window').hide();
                });

                $('#drop-window').on("drop",function() {
                    $('#drop-window').hide();
                });
            }
        });
        uploader.init();
    });
</script>
@stop
<?php namespace App\Modules\Media\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 5/2/14
 * Time: 10:38 AM
 */

class Media extends \Eloquent {
    protected $table = 'media';
    protected $primaryKey = 'media_id';

    protected $fillable = array('media_name', 'media_file_name', 'media_file_url', 'media_file_size', 'media_file_type', 'media_file_width', 'media_file_height','media_directory_id', 'user_id', 'status');

    protected $softDelete = true;

    public static $rules = array(
        'group_name'=>'required|min:2|max:50|unique:groups',
        'status'=>'required|integer',
    );
}
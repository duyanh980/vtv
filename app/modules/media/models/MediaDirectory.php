<?php namespace App\Modules\Media\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/30/14
 * Time: 11:33 PM
 */


class MediaDirectory extends \Eloquent {

    protected $table = 'media_directories';
    protected $primaryKey = 'directory_id';

    protected $fillable = array('directory_name', 'directory_parent_id', 'directory_path', 'directory_size', 'status');

    protected $softDelete = true;

    public static $rules = array(
        'directory_name'=>'required|min:2|max:50',
    );
}
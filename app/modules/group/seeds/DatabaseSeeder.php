<?php namespace App\Modules\Group\Seeds;
use App\Modules\Group\Models\Group;
use App\Modules\Menu\Models\MenuItem;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/25/14
 * Time: 11:37 PM
 */


class DatabaseSeeder extends \Seeder {

    public function run() {
        Group::truncate();
        Group::create(array(
            'group_name'   =>  'Administrator',
            'user_id' =>  '1'
        ));
        Group::create(array(
            'group_name'   =>  'User',
            'user_id' =>  '1'
        ));



        $menuGroup = MenuItem::create(array(
            'menu_item_name'    =>  'Groups',
            'menu_item_url'     =>  '/admin/group',
            'menu_item_class'   =>  'fa-group',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  10
        ));
        MenuItem::create(array(
            'menu_item_name'    =>  'Groups',
            'menu_item_url'     =>  '/admin/group',
            'menu_item_class'   =>  'fa-group',
            'menu_item_parent_id'=> $menuGroup->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1
        ));
        MenuItem::create(array(
            'menu_item_name'    =>  'Permissions',
            'menu_item_url'     =>  '/admin/permission',
            'menu_item_class'   =>  '',
            'menu_item_parent_id'=> $menuGroup->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1
        ));
    }
}
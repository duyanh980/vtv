@extends('admin.main')


@section('content')

<div class="row">
    <div class="col-xs-12">
        <section class="panel">
            <div class="panel-body profile-information">
                <div class="col-md-3">
                    <div class="profile-pic text-center">
                        {{ User::avatar($user->user_id) }}
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-desk">
                        <h1>{{ User::name($user->user_id) }}</h1>
                        <span class="text-muted">{{ User::group($user->group_id) }}</span>
                        <div><a class="btn btn-primary" href="{{ route('admin.user.edit',array($user->user_id)) }}">Update Profile</a></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@stop
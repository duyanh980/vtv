@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.user.update',array((isset($user) ? $user->user_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                User Manager
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="first_name">First Name</label>
                            <div class="col-lg-6">
                                {{ Form::text('first_name',Input::old('first_name', isset($user) ? $user->first_name : '' ), array('id' => 'first_name', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="first_name">Last Name</label>
                            <div class="col-lg-6">
                                {{ Form::text('last_name',Input::old('last_name', isset($user) ? $user->last_name : '' ), array('id' => 'last_name', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="first_name">Email</label>
                            <div class="col-lg-6">
                                {{ Form::text('email',Input::old('email', isset($user) ? $user->email : '' ), array('id' => 'email', 'class'=>'form-control validate[required,custom[email]]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="first_name">Birthday</label>
                            <div class="col-lg-6">
                                {{ Form::text('birthday',Input::old('birthday', isset($user) ? $user->birthday->format('d/m/Y') : '' ), array('id' => 'birthday', 'class'=>'datepicker form-control validate[required,custom[date]]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="first_name">Password</label>
                            <div class="col-lg-6">
                                {{ Form::text('password',null, array('id' => 'password', 'class'=>'form-control '.(!isset($user) ? 'validate[required]' : ''))) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="first_name">Password Confirm</label>
                            <div class="col-lg-6">
                                {{ Form::text('pass_confirm',null, array('id' => 'pass_confirm', 'class'=>'form-control validate[equals[password]]')) }}
                            </div>
                        </div>
                        @if(Auth::HasPermission('Permission.Group.Manager') AND !empty($groups))
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="group_id">{{ Lang::get('monster.group') }}</label>
                            <div class="col-lg-6">
                                {{ Form::select('group_id',$groups,Input::old('group_id', isset($user) ? $user->group_id : '' ), array('id' => 'group_id', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.publish') }}
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                </span>
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-xs-12"><span class="pull-left">{{ Lang::get('monster.status') }}</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('status',array('0'=>Lang::get('monster.draft'), '2' => Lang::get('monster.pendingReview'), '1'=> Lang::get('monster.publish')),Input::old('status', isset($user) ? $user->status : '' ), array('id' => 'status', 'class'=> 'form-control validate[required]')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($user) ? $user->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                            {{ Lang::get('monster.modifiedOn') }}: {{ isset($user) ? $user->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.user.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                Avatar
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12">

                            <div class="fileupload <?php echo (isset($user) AND is_file($user->avatar)) ? 'fileupload-exists' : 'fileupload-new'; ?>">
                                @if(isset($user) AND is_file($user->avatar))
                                <img src="{{ url($user->avatar) }}" id="avatar_preview" class="image_preview" />
                                @endif
                                    <input type="hidden" id="avatar" name="avatar" />
                                    <a href="<?php echo route('admin.media.popup.manager'); ?>?type=single-image" class="btn btn-white btn-file open-popup-ajax">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                    </a>
                                    <a href="#" class="btn btn-danger fileupload-exists remove-image" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>
{{ Form::close() }}

<script type="text/javascript">
    $(document).ready(function() {
        $(document).bind("addFile",function() {
            $('#avatar').val(ImageSelected.media_file_url);
            if($('#avatar_preview').get(0)) {
                $('#avatar_preview').attr('src',ImageSelected.media_file_full_url);
            }
            else {
                $('.fileupload').prepend('<img src="' + ImageSelected.media_file_full_url + '" id="avatar_preview" class="image_preview" />');
            }
            $('.fileupload').removeClass('fileupload-new').addClass('fileupload-exists');
        });

        $('.remove-image').on("click",function() {
            $('#avatar').val('');
            $('#avatar_preview').remove();
            $('.fileupload').removeClass('fileupload-exists').addClass('fileupload-new');
        });
    });
</script>
@stop
@extends('admin.login')

@section('content')
{{ Form::open(array('url'=>route('user.reset'), 'class'=> 'form-signin', 'id' => 'reset-form', 'method' => 'get')) }}
<input type="hidden" name="email" value="{{ isset($user) ? $user->email : '' }}">
<input type="hidden" name="token" value="{{ isset($user) ? $user->reset_token : '' }}">
<h2 class="form-signin-heading">Creat new password</h2>
<div class="login-wrap">
    <div class="user-login-info">
        {{ Monster::message() }}
        {{ Form::password('password', array('class' => 'form-control', 'name'=> 'pass', 'placeholder'=>'Password', 'id'=> 'password', isset($user) ? '' : 'readonly' )) }}
        {{ Form::password('password', array('class' => 'form-control', 'name'=> 'repass', 'placeholder'=>'Re-enter Password', 'id'=> 'repassword', isset($user) ? '' : 'readonly' )) }}
    </div>
    <button class="btn btn-lg btn-login btn-block" type=button id="btn-reset">Creat new password</button>
</div>
{{ Form::close() }}
<script type="text/javascript">
    $(document).on('click','.btn-login',function() {
        if (!$('#repassword').val())
            $('#repassword').focus();

        if (!$('#password').val())
            $('#password').focus();

        if ($('#password').val() && $('#repassword').val() ) {
            $("#reset-form").submit();
        }
    });
</script>
@stop
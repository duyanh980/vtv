<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Forgot Password ?</h4>
        </div>
        <div class="modal-body">
            {{ Monster::message() }}
            @if (isset($email)) @else <p>Enter your e-mail address below to reset your password.</p> @endif
            <input type="text" id="forget-email" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix" value="{{ $email }}">

        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
            <button class="btn btn-success" type="button">Submit</button>
        </div>
    </div>
</div>


<p>Dear <b>{{ $first_name }} {{ $last_name }}</b>,</p>
<p>Email: {{ $email }}</p>
<p>Please click on the link: <a href="{{ route('user.reset') }}?email={{ $email }}&token={{ $reset_token }}">{{ route('user.reset') }}?email={{ $email }}&token={{ $reset_token }}</a> to change your password!</p>
<p>Note: Link is enabled for 24 hours prior to {{ $reset_token_expiry}}</p>

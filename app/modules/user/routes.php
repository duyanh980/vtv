<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/17/14
 * Time: 8:59 PM
 */

//front end
Route::group(array('namespace'=>'App\\Modules\\User\\Controllers'),function() {
    Route::get('login',    array('as'  => 'login' ,         'uses'   =>  'UserController@getLogin'));
    Route::post('login',   array('as'  => 'login',          'uses'   =>  'UserController@postLogin'));
    Route::get('logout',   array('as'  => 'user.logout',    'uses'   =>  'UserController@getLogout'));

    Route::get('forgetPassword', array('as' => 'user.forgetPassword' , 'uses' => 'UserController@getForgetPassword'));
    Route::get('contentEmail',   array('as' => 'user.contentEmail' ,   'uses' => 'UserController@getContentEmail'));
    Route::get('reset',          array('as' => 'user.reset' ,          'uses' => 'UserController@getReset'));
});

//back end
Route::group(array('prefix'=>'admin', 'before'=>'auth.admin', 'namespace'=>'App\\Modules\\User\\Controllers\\Admin'),function() {
    Route::get('user',                  array('before'  =>  'auth.restrict',    'as'   =>    'admin.user.list',        'uses'  =>  'UserController@getList'));
    Route::get('user/dataTable',        array('before'  =>  'auth.restrict',    'as'   =>    'admin.user.dataTable',   'uses'  =>  'UserController@getDataTable'));
    Route::get('user/profile/{id?}',    array(                                  'as'   =>     'admin.user.profile',     'uses'  =>  'UserController@getProfile'));
    Route::get('user/create',           array('before'  =>  'auth.restrict',    'as'   =>    'admin.user.create',      'uses'  =>  'UserController@getCreate'));
    Route::get('user/edit/{id?}',       array(                                  'as'   =>    'admin.user.edit',        'uses'  =>  'UserController@getEdit'));
    Route::post('user/update/{id?}',    array(                                  'as'   =>    'admin.user.update',      'uses'  =>  'UserController@postUpdate'));
});

<?php namespace App\Modules\User\Seeds;
use App\Modules\Menu\Models\MenuItem;
use Illuminate\Support\Facades\Hash;
use App\Modules\User\Models\User;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/25/14
 * Time: 11:40 PM
 */


class DatabaseSeeder extends \Seeder {

    public function run() {
        User::truncate();
        User::create(array(
            'first_name'    =>  'The',
            'last_name'     =>  'Jupitech',
            'email'         =>  'admin@jupitech.sg',
            'birthday'      =>  '1990-01-22',
            'password'      =>  Hash::make('admin@jpt'),
            'status'        =>  '1',
            'group_id'       =>  '1',
        ));

        MenuItem::create(array(
            'menu_item_name'    =>  'Users',
            'menu_item_url'     =>  '/admin/user',
            'menu_item_class'   =>  'fa-user',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  9,
        ));
    }
}
<?php namespace App\Modules\User\Controllers\Admin;
use App\Modules\Group\Models\Group;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Monster\Monster,Auth;
use View, Validator, Input, Datatable, App\Modules\User\Models\User, Lang, URL;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/20/14
 * Time: 9:57 PM
 */

class UserController extends \BackendController {

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    User::whereIn('user_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(User::all())
                ->showColumns('first_name', 'last_name', 'email')
                ->searchColumns('first_name','last_name','email')
                ->addColumn('last_login',function($item) {
                    return $item->last_login->diffForHumans();
                })
                ->addColumn('status',function($item) {
                    return Monster::status($item->status);
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.user.edit',array($item->user_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->user_id;
                })
                ->orderColumns('first_name','last_name','email','last_login','status')
                ->make();
        }
    }
    public function getList() {
        return View::make('user::admin.list');
    }

    public function getProfile($id=false) {
        if(!$id) {
            $id = Auth::user()->user_id;
        }
        if(!Auth::HasPermission('Permission.User.Manager') AND $id!==Auth::user()->user_id) {
            Monster::set_message(Lang::get('monster.youDontHavePermissionToViewThatPage'));
            return Redirect::to(URL::previous());
        }
        $user = User::find($id);
        return View::make('user::admin.profile')->with('user',$user);
    }

    public function getCreate() {
        $groups = Group::all();
        $groupAllowed = array();
        if(!$groups->isEmpty()) {
            foreach($groups as $var) {
                if(Auth::HasPermission('Group.'.$var->group_name.'.Manager'))
                    $groupAllowed[$var->group_id] = $var->group_name;
            }
        }
        return View::make('user::admin.update')->with('groups',$groupAllowed);
    }

    public function getEdit($id = false) {
        if(!$id) {
            $id = Auth::user()->user_id;
        }
        if(!Auth::HasPermission('Permission.User.Manager') AND $id!==Auth::user()->user_id) {
            Monster::set_message(Lang::get('monster.youDontHavePermissionToViewThatPage'));
            return Redirect::to(URL::previous());
        }
        $user = User::find($id);
        $groups = Group::all();
        $groupAllowed = array();
        if(!$groups->isEmpty()) {
            foreach($groups as $var) {
                if(Auth::HasPermission('Group.'.$var->group_name.'.Manager'))
                $groupAllowed[$var->group_id] = $var->group_name;
            }
        }
        return View::make('user::admin.update')->with('user',$user)->with('groups',$groupAllowed);
    }

    public function postUpdate($id=false) {

        if(!Auth::HasPermission('Permission.User.Manager') AND ($id!==Auth::user()->user_id OR $id!=false)) {
            Monster::set_message(Lang::get('monster.youDontHavePermissionToViewThatPage'));
            return Redirect::to(URL::previous());
        }
        if(!Auth::HasPermission('Permission.User.Manager')) {
            $id = Auth::user()->user_id;
        }

        if($this->checkValidator($id)) {
            $user = User::firstOrNew(array('user_id'=>$id));
            $user->fill(Input::except(array('birthday','group_id')));
            $group = Group::find(Input::get('group_id'));
            if($group AND Auth::HasPermission('Group.'.$group->group_name.'.Manager')) {
                $user->group_id = $group->group_id;
            }
            $user->birthday = Carbon::createFromFormat("d/m/Y",Input::get('birthday'))->format("Y-m-d");
            $user->save();

            if(Auth::HasPermission("")) {
                $user->set_group_permission($id,Input::get('group_permissions'));
            }
            return Redirect::route('admin.user.edit',array($user->user_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.user.edit',array($id));
        }
        else {
            return Redirect::to(route('admin.user.create'))->withInput();
        }
    }

    private function checkValidator($id) {
        $rules = User::$rules;
        if($id) {
            $rules['email'] = 'required|min:2|max:50|unique:users,email,' .$id. ',user_id';
            $rules['password'] = '';
        }
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }
}
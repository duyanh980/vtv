<?php namespace App\Modules\User\Controllers;
use Carbon\Carbon;
use View, Input, Auth, Monster, Mail, Hash;
use Illuminate\Support\Facades\Redirect, User;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/17/14
 * Time: 8:33 PM
 */

class UserController extends \FrontendController {

    public function __construct() {
        parent::__construct();
    }

    public function getLogin() {


        return View::make('user::login');
    }

    public function getLogout() {
        Auth::logout();
    }

    public function getReset() {
        Monster::set_message('');

        $today = date('Y-m-d H:i:s');
        $user = User::where('email', '=', Input::get('email'))->where('reset_token', '=', Input::get('token'))->where('reset_token_expiry', '>=', $today)->first();

        if (!empty($user)) {
            $pass= Input::get('pass');
            $repass = Input::get('repass');

            if ($pass && $repass) {
                if ($pass == $repass) {
                    Monster::set_message('Creat new password succesful. Please login!','true');
                    $user->password = Hash::make($pass);
                    $user->reset_token = '';
                    $user->save();
                    return Redirect::route('login');
                }
                Monster::set_message('You have entered an incorrect password. Please re-enter!','error');
            }
        } else {
            Monster::set_message('Time limited or password changed. Please check your latest email!','error');
        }

        return View::make('user::reset')->with('user', $user);
    }

    public function getForgetPassword() {

        $email = Input::get('email');

        $user = User::where('email', '=', $email)->first();

        if (!empty($user)) {
            Monster::set_message('Email sent to your email address. Please check email!','true');

            $user->reset_token = Hash::make($user->user_id . $user->email);
            $user->reset_token_expiry = date('Y:m:d H:i:s', strtotime(date('Y-m-d H:i:s') . " +1 day"));
            $user->save();

            $data = User::where('email', '=', $email)->first()->toArray();

            Mail::send('user::contentEmail', $data, function($message)
            {
                $message->from('admin@jupitech.sg', 'Jupitech CMS');
                $message->to(Input::get('email'))->subject('Forgot Password');
            });

            return View::make('user::forgetPassword')->with('email',$email);
        }

        Monster::set_message('Email not exist','error');
        return View::make('user::forgetPassword')->with('email',$email);

    }

    public function getContentEmail() {
        return View::make('user::contentEmail');
    }

    public function postLogin() {
        if(Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'status'=>1),true)) {
            $user = User::find(Auth::user()->user_id);
            $user->last_login = Carbon::now();
            $user->save();
            if(Auth::HasPermission('Website.Backend.Login')) {
                return Redirect::route('admin.dashboard');
            }
            else {
                return Redirect::route('home');
            }
        }
        Monster::set_message('Email or password incorrect','error');
        return Redirect::to('login');
    }
}
<?php namespace App\Modules\Menu\Seeds;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\MenuItem;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/24/14
 * Time: 10:28 PM
 */

class DatabaseSeeder extends \Seeder {

    public function run() {
        Menu::truncate();
        MenuItem::truncate();
        Menu::create(array(
            'menu_name' => 'Admin Menu',
            'status' => '1',
            'user_id' => 1,
        ));
        MenuItem::create(array(
            'menu_item_name'    =>  'Dashboard',
            'menu_item_url'     =>  '/admin',
            'menu_item_class'   =>  'fa-home',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  0
        ));
        MenuItem::create(array(
            'menu_item_name'    =>  'Menu',
            'menu_item_url'     =>  '/admin/menu',
            'menu_item_class'   =>  'fa-list',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  99
        ));
        /*
        MenuItem::create(array(
            'menu_item_name'    =>  'Theme Options',
            'menu_item_url'     =>  '/admin/options',
            'menu_item_class'   =>  'fa-tachometer',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1
        ));
        */
    }
}
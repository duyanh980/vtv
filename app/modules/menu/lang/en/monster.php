<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 11:23 PM
 */


return array(
    'moduleManager'    =>  'Menu Manager ',
    'menuName'   =>  'Menu Name',
    'manager'   =>  'Menu Manager',
    'name'      =>  'Name',
    'url'       =>  'URL',
    'class'     =>  'Class',
    'target'    =>  'Target'
);
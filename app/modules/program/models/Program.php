<?php namespace App\Modules\Program\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:45 PM
 */


class Program extends \Eloquent {

    protected $table = 'program';
    protected $primaryKey = 'program_id';
    protected $fillable = array('program_name','program_image','program_description','status');

    public static $rules = array(
        'program_name'=>'required|max:250',
        'program_image'=>'required|max:250',
        'program_description'    =>  'required|min:8',
    );


}
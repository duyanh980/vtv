<?php namespace App\Modules\Program\Models;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:45 PM
 */


class ProgramSchedule extends \Eloquent {

    protected $table = 'boardcast_schedule';
    protected $primaryKey = 'id';
    protected $fillable = array('channel_id','program_id','time');

    public static $rules = array(
        'channel_id'=>'required',
        'program_id'=>'required'
    );


}
@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.program.update',array((isset($program) ? $program->program_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('program::monster.moduleManager') }}
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="program_name">{{ Lang::get('program::monster.programName') }}</label>
                            <div class="col-lg-6">
                                {{ Form::text('program_name',Input::old('program_name', isset($program) ? $program->program_name : '' ), array('id' => 'program_name', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="program_description">{{ Lang::get('program::monster.programDescription') }}</label>
                            <div class="col-lg-6">
                                {{ Form::textarea('program_description',Input::old('program_description', isset($program) ? $program->program_description : '' ), array('id' => 'program_description', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.activity') }}
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                </span>
            </header>
            <div class="panel-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-xs-12"><span class="pull-left">{{ Lang::get('monster.status') }}</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('status',array('0'=>Lang::get('monster.draft'), '1'=> Lang::get('monster.publish')),Input::old('status', isset($program) ? $program->status : '' ), array('id' => 'status', 'class'=> 'form-control validate[required]')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($program) ? $program->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                           {{ Lang::get('monster.modifiedOn') }}: {{ isset($program) ? $program->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.program.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                Channel Image
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12">

                            <div class="fileupload <?php echo (Input::old('program_image', isset($program) ? $program->program_image : '' )!="") ? 'fileupload-exists' : 'fileupload-new'; ?>">
                                @if(Input::old('program_image', isset($program) ? $program->program_image : '' )!="")
                                <img src="{{ url(Input::old('program_image', isset($program) ? $program->program_image : '' )) }}" id="program_preview" class="image_preview" />
                                @endif
                                    <input type="hidden" id="program_image" name="program_image" value="{{Input::old('program_image', isset($program) ? $program->program_image : '' )}}" />
                                    <a href="<?php echo route('admin.media.popup.manager'); ?>?type=single-image" class="btn btn-white btn-file open-popup-ajax">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                    </a>
                                    <a href="#" class="btn btn-danger fileupload-exists remove-image" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </div>
</div>
{{ Form::close() }}


<script type="text/javascript">
    $(document).ready(function() {
        $(document).bind("addFile",function() {
            $('#program_image').val(ImageSelected.media_file_url);
            if($('#program_preview').get(0)) {
                $('#program_preview').attr('src',ImageSelected.media_file_full_url);
            }
            else {
                $('.fileupload').prepend('<img src="' + ImageSelected.media_file_full_url + '" id="program_preview" class="image_preview" />');
            }
            $('.fileupload').removeClass('fileupload-new').addClass('fileupload-exists');
        });

        $('.remove-image').on("click",function() {
            $('#program_image').val('');
            $('#program_preview').remove();
            $('.fileupload').removeClass('fileupload-exists').addClass('fileupload-new');
        });
    });
</script>
@stop
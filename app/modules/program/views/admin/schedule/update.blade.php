@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.program.schedule.update',array((isset($schedule) ? $schedule->id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('program::monster.scheduleManager') }}
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="date">Date</label>
                            <div class="col-lg-6">
                                {{ Form::text('date',Input::old('date', isset($schedule) ? $schedule->date : '' ), array('id' => 'date', 'class'=>'form-control validate[required] datepicker')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="program_id">{{ Lang::get('program::monster.programName') }}</label>
                            <div class="col-lg-6">
                                {{ Form::select('program_id',\App\Modules\Program\Models\Program::lists('program_name','program_id'),Input::old('program_id', isset($schedule) ? $schedule->program_id : '' ), array('id' => 'program_id', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3" for="channel_id">{{ Lang::get('channel::monster.channelName') }}</label>
                            <div class="col-lg-6">
                                {{ Form::select('channel_id',\App\Modules\Channel\Models\Channel::lists('channel_name','channel_id'),Input::old('channel_id', isset($schedule) ? $schedule->channel_id : '' ), array('id' => 'channel_id', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12 text-right">
                                <button type="button" class="btn btn-primary" id="addTime">Add time</button>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-lg-3" for="channel_id">{{ Lang::get('program::monster.time') }}</label>
                            <div class="col-lg-6">
                                <div class="input-group bootstrap-timepicker">
                                    {{ Form::text('hour',Input::old('hour', isset($schedule) ? $schedule->hour : '' ), array('id' => 'hour', 'class'=>'form-control validate[required] timepicker-24')) }}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-clock-o"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.activity') }}
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                </span>
            </header>
            <div class="panel-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.program.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
</div>
{{ Form::close() }}

@stop
<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:32 PM
 */

Route::group(array('prefix'=>'admin','namespace'=>'App\\Modules\\Program\\Controllers\\Admin'),function() {

    Route::get('program',                    array('as'   =>  'admin.program.list',                'uses'  =>  'ProgramController@getList'));
    Route::get('program/dataTable',          array('as'   =>  'admin.program.dataTable',           'uses'  =>  'ProgramController@getDataTable'));

    Route::get('program/create',             array('as'   =>  'admin.program.create',              'uses'  =>  'ProgramController@getCreate'));
    Route::get('program/edit/{id}',          array('as'   =>  'admin.program.edit',                'uses'  =>  'ProgramController@getEdit'));
    Route::post('program/update/{id?}',      array('as'   =>  'admin.program.update',              'uses'  =>  'ProgramController@postUpdate'));



    Route::get('program/schedule',                    array('as'   =>  'admin.program.schedule.list',                'uses'  =>  'ProgramScheduleController@getList'));
    Route::get('program/schedule/dataTable',          array('as'   =>  'admin.program.schedule.dataTable',           'uses'  =>  'ProgramScheduleController@getDataTable'));

    Route::get('program/schedule/create',             array('as'   =>  'admin.program.schedule.create',              'uses'  =>  'ProgramScheduleController@getCreate'));
    Route::get('program/schedule/edit/{id}',          array('as'   =>  'admin.program.schedule.edit',                'uses'  =>  'ProgramScheduleController@getEdit'));
    Route::post('program/schedule/update/{id?}',      array('as'   =>  'admin.program.schedule.update',              'uses'  =>  'ProgramScheduleController@postUpdate'));

});
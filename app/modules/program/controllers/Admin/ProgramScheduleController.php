<?php namespace App\Modules\Program\Controllers\Admin;

use App\Modules\Channel\Models\Channel;
use App\Modules\Program\Models\Program;
use Auth, View, Lang, Monster, Datatable, Input, Validator;
use Illuminate\Support\Facades\Redirect;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:34 PM
 */

class ProgramScheduleController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getList() {
        return View::make('program::admin.schedule.list');
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Program::whereIn('program_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Program::all())
                ->showColumns('channel_name')
                ->searchColumns('channel_name')
                ->orderColumns('program_id','channel_name','latest_update')
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.program.edit',array($item->program_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->program_id;
                })
                ->make();
        }
    }

    public function getCreate() {
        return View::make('program::admin.schedule.update');
    }

    public function getEdit($id) {
        $program = Program::find($id);
        return View::make('program::admin.schedule.update')->with('program',$program);
    }

    public function postUpdate($id=false) {
        if($this->checkValidator($id)) {
            $program = Program::firstOrNew(array('program_id'=>$id));
            $program->fill(Input::all());
            $program->user_id = Auth::user()->user_id;
            $program->save();
            return Redirect::route('admin.program.schedule.edit',array($user->program_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.program.schedule.edit',array($id));
        }
        else {
            return Redirect::to(route('admin.program.schedule.create'))->withInput();
        }
    }

    private function checkValidator($id=false) {

        $rules = Program::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message('Update Success','success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }
}
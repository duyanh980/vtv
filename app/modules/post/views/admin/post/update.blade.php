@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.post.update',array((isset($post) ? $post->post_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">

                @if ( isset($post) ? $post->post_id : '' )
                    {{ Lang::get('post::monster.editPost') }}
                @else
                    {{ Lang::get('post::monster.addNew') }}
                @endif
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('post::monster.postTitle')}}</label>
                            <div class="col-lg-10">
                                {{ Form::text('post_title',Input::old('post_title', isset($post) ? $post->post_title : '' ), array('id' => 'post_title', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{Lang::get('post::monster.postContent')}}</label>
                            <div class="col-lg-10">
                                {{ Form::textarea('post_content',Input::old('post_content', isset($post) ? $post->post_content : '' ), array('id' => 'post_content', 'class'=>'form-control validate[required] ckeditor')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-4">

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('monster.publish') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-xs-12"><span class="pull-left">{{ Lang::get('monster.status') }}</span></label>
                        <div class="col-xs-12">
                            {{ Form::select('status',array('0'=>Lang::get('monster.draft'), '2' => Lang::get('monster.pendingReview'), '1'=> Lang::get('monster.publish')),Input::old('status', isset($post) ? $post->status : '' ), array('id' => 'status', 'class'=> 'form-control validate[required]')) }}

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left">{{ Lang::get('monster.modifiedBy') }}: {{ User::name((isset($post) ? $post->user_id: ''),true); }}
                            </span>
                        </label>
                        <div class="col-xs-12">
                           {{ Lang::get('monster.modifiedOn') }}: {{ isset($post) ? $post->updated_at->diffForHumans() : ''; }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                            {{ HTML::link(route('admin.post.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('post::monster.categories') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div id="treecate" class="filetree" data-url="{{ route('admin.post.list.category')}}"></div>
                        <div class="col-xs-12">
                            <div id="displayListCate">@if (isset($category)) {{ $category }} @endif</div>
                            <div class="add_new_category show_hide_item" >{{ Lang::get('post::monster.addNewCategory') }}</div>

                            <div id="add_new_category">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">{{ Lang::get('post::monster.name') }}</label>
                                    <div class="col-lg-10">
                                        {{Form::text('category_name', '',array('id' => 'category-name', 'class'=> 'form-control validate[required]'))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">{{ Lang::get('post::monster.category') }}</label>
                                    <div class="col-lg-10">
                                        <select name="category_parent_id" id="category_parent_id" class="form-control validate[required]">
                                            {{ $listCategoryHtml }}
                                        </select>
                                    </div>
                                </div>
                                <button type="button" name="save" class="btn btn-primary btn-update-category">{{ Lang::get('monster.save') }}</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('post::monster.tag') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-9">
                            {{ Form::text('post_tag','', array('id' => 'post_tag', 'class'=>'form-control')) }}
                        </div>
                        <button type="button" class="btn btn-default btnAddTags">{{ Lang::get('post::monster.add') }}</button>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-12">
                            <span class="pull-left"><i>{{ Lang::get('post::monster.select_tag') }}</i></span>
                        </label>
                        <div class="listTags">
                            @if (isset($keyword))
                                @foreach ($keyword as $item)
                                <span class="tag">
                                    <i class="fa fa-times delete-tag" ></i>
                                    <input type=hidden name=tag[] value="{{$item->keyword}}"> {{$item->keyword}}
                                </span>&nbsp;&nbsp;
                                @endforeach
                            @endif
                        </div>
                    </div>
                    @if(!empty($tags))
                    <div class="form-group">
                        <div class="show_hide_item choose_tag">{{ Lang::get('post::monster.choose_tag') }}</div>
                        <div id="choose_tag">
                            @foreach ($tags as $keyword)
                            <span class="choose_tag_db" >{{ $keyword->keyword }}</span>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </section>

        <section class="panel">
            <header class="panel-heading">
                {{ Lang::get('post::monster.featuredImage') }}
            </header>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12">

                            <div class="fileupload <?php echo (isset($post) AND is_file($post->image)) ? 'fileupload-exists' : 'fileupload-new'; ?>">
                                @if(isset($post) AND is_file($post->image))
                                <img src="{{ url($post->image) }}" id="avatar_preview" class="image_preview" />
                                @endif
                                <input type="hidden" id="avatar" name="image" />
                                <a href="<?php echo route('admin.media.popup.manager'); ?>?type=single-image" class="btn btn-white btn-file open-popup-ajax">
                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select image</span>
                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                </a>
                                <a href="#" class="btn btn-danger fileupload-exists remove-image" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </div>


</div>
{{ Form::close() }}

<script type="text/javascript">

    var tagsAvailable = ['@if (isset($keyword_str)){{ $keyword_str }}@endif'];
    $(document).ready(function() {

        $( "#post_tag" ).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "{{ route('admin.post.tag.search') }}",
                    dataType: "json",
                    data: {
                        term: request.term,
                        tagsAvailable : tagsAvailable
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
            }
        });


        $(document).bind("addFile",function() {
            $('#avatar').val(ImageSelected.media_file_url);
            if($('#avatar_preview').get(0)) {
                $('#avatar_preview').attr('src',ImageSelected.media_file_full_url);
            }
            else {
                $('.fileupload').prepend('<img src="' + ImageSelected.media_file_full_url + '" id="avatar_preview" class="image_preview" />');
            }
            $('.fileupload').removeClass('fileupload-new').addClass('fileupload-exists');
        });


        $('.remove-image').on("click",function() {
            $('#avatar').val('');
            $('#avatar_preview').remove();
            $('.fileupload').removeClass('fileupload-exists').addClass('fileupload-new');
        });
    });

    var setTag = function(post_tag) {
        $('#post_tag').val();
        var html = '<span class="tag">' +
            '<i class="fa fa-times delete-tag"></i>' +
            '<input type=hidden name=tag[] value="' + post_tag + '"> ' + post_tag +
            '</span>&nbsp;&nbsp;';

        if ($.inArray(post_tag, tagsAvailable) > -1) {
            return false;
        } else {
            tagsAvailable.push(post_tag);
            $('.listTags').prepend(html);
        }

        $('#post_tag').val('');
    };

    $(document).on('click','.btnAddTags',function() {
        if ($('#post_tag').val()) {
            setTag($('#post_tag').val());
        } else {
            $('#post_tag').focus();
        }
    });
    $(document).on('click','.choose_tag_db',function() {
        setTag($(this).html());
    });

    $(document).on('click','.delete-tag',function() {

        var tag = $(this).parent().find('input').val();

        $(this).parent().remove();

        $.grep(tagsAvailable,function() {
            return this==tag;
        },true);

    });

    $(document).on('click','.btn-update-category',function() {
        if ($('#category-name').val()) {
            var category_parent_id = $('#category_parent_id').val();

            $.ajax({
                type:'post',
                url:"{{ route('admin.post.category.update.post') }}",
                data: { name :  $("#category-name").val(), category_parent_id : category_parent_id },
                success: function() {
                    $("#displayListCate").load("{{ route('admin.post.category.listCate',array((isset($post) ? $post->post_id : ''))) }}");
                    $("#displaySelectListCate").load("{{ route('admin.post.category.selectListCate') }}");
                    $("#category-name").val('');
                }
            })

        } else {
            $('#category-name').focus();
        }
    });
    var num_click = 0;
    $('.add_new_category').click(function() {
        $('#add_new_category').slideToggle();
        ++num_click;
        if(num_click%2==0) {
            $(this).html('{{ Lang::get('post::monster.addNewCategory') }}');
        }
        else {
            $(this).html('{{ Lang::get('post::monster.hideForm') }}');
        }
    });

    $('.choose_tag').click(function() {
        $('#choose_tag').slideToggle();
    })
</script>
@stop
@extends('admin.main')

@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.post.update.item',array($menuID, (isset($item) ? $item->category_id : ''))), 'id'=> 'validate')) }}
<div class="row">
    <div class="col-lg-4 col-xs-12">
        <section class="panel">
            <header class="panel-heading">{{Lang::get('post::monster.moduleManager')}}</header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('post::monster.name') }}</label>
                            <div class="col-lg-8">
                                {{Form::text('category_name',Input::old('category_name', isset($item) ? $item->category_name : ''),array('class'=> 'form-control validate[required]'))}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('post::monster.slug') }} </label>
                            <div class="col-lg-8">
                                {{Form::text('category_slug',Input::old('category_slug', isset($item) ? $item->category_slug : ''),array('class'=> 'form-control validate[required]'))}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{ Lang::get('monster.status') }}</label>
                            <div class="col-lg-8">
                                {{ Form::select('status',array('0'=>Lang::get('monster.draft'), '2' => Lang::get('monster.pendingReview'), '1'=> Lang::get('monster.publish')),Input::old('status', isset($item) ? $item->status : '' ), array('id' => 'status', 'class'=> 'form-control validate[required]')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" name="save" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                                <button type="submit" name="create" value="addNew" class="btn btn-primary">{{ Lang::get('monster.addNew') }}</button>
                                {{ HTML::link(route('admin.menu.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-8 col-xs-12">
        <section class="panel">
            <header class="panel-heading">{{Lang::get('monster.order')}}</header>
            <div class="panel-body">

                <div class="dd" id="menu_manager">
                    {{ $menuItemHtml }}
                </div>
            </div>
        </section>
    </div>
</div>
{{ Form::close() }}

<script type="text/javascript">
    $(document).ready(function() {
        $('#menu_manager').nestable({
            group: 1
        }).on('change', function(e) {
                var list   = e.length ? e : $(e.target),
                    output = list.data('output');
                $.ajax({
                    type:'post',
                    url:"{{ route('admin.post.order.item') }}",
                    data: { menu : list.nestable('serialize') }
                })
            });
    });
</script>
@stop
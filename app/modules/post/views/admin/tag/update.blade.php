@extends('admin.main')


@section('content')
{{ Monster::message() }}
{{ Form::open(array('url'=>route('admin.post.tag.update',array((isset($tag) ? $tag->tag_id : ''))), 'id'=> 'validate')) }}

<div class="row">
    <div class="col-lg12">
        <section class="panel">
            <header class="panel-heading">
                @if ( isset($tag) ? $tag->tag_id : '' )
                    {{Lang::get('post::monster.editTag')}}
                @else
                    {{ Lang::get('post::monster.addNewTag') }}
                @endif
            </header>
            <div class="panel-body">
                <div class="form">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-lg-2" for="menu_name">{{Lang::get('post::monster.keyword')}}</label>
                            <div class="col-lg-10">
                                {{ Form::text('keyword',Input::old('keyword', isset($tag) ? $tag->keyword : '' ), array('id' => 'keyword', 'class'=>'form-control validate[required]')) }}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-12" style="text-align:right;">
                    <button type="submit" class="btn btn-primary">{{ Lang::get('monster.save') }}</button>
                    {{ HTML::link(route('admin.post.tag.list'), Lang::get('monster.backToList'), array('class' => 'btn btn-default')) }}
                </div>
            </div>
        </section>

    </div>
</div>
{{ Form::close() }}


@stop
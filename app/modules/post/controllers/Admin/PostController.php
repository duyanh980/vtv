<?php namespace App\Modules\Post\Controllers\Admin;
use Auth, View, Lang, Monster, Datatable, Input, Validator;
use App\Modules\Post\Models\Post;
use App\Modules\Post\Models\Tags;
use App\Modules\Post\Models\PostCategory;
use App\Modules\Post\Models\PostTags;
use App\Modules\Post\Models\Category;
use Illuminate\Support\Facades\Redirect;
use Orchestra\Support\Facades\Asset;

/**
 * Author: Trong
 * Date: 4/22/14
 * Time: 1:34 PM
 */

class PostController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Post::whereIn('post_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Post::all())
                ->showColumns('post_title')
                ->searchColumns('post_title')
                ->orderColumns('post_id','post_title','latest_update','status')
                ->addColumn('status',function($item) {
                    return Monster::status($item->status);
                })
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.post.edit',array($item->post_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->post_id;
                })
                ->make();
        }
    }

    public function getList() {
        return View::make('post::admin.post.list');
    }

    public function getCreate() {

        Asset::container('footer')->add('jqtree.css', 'assets/plugin/jqTree/jqtree.css');
        Asset::container('footer')->add('tree.jquery.js', 'assets/plugin/jqTree/tree.jquery.js');


        $tags = Tags::all();
        $category = Category::tree('');
        return View::make('post::admin.post.update')
                    ->with('tags',$tags)
                    ->with('category',$category)
                    ->with('listCategoryHtml',Category::buildOptionsCategoryHTML());
    }

    public function getEdit($id) {

        Asset::container('footer')->add('jqtree.css', 'assets/plugin/jqTree/jqtree.css');
        Asset::container('footer')->add('tree.jquery.js', 'assets/plugin/jqTree/tree.jquery.js');

        $tags = PostTags::getLimitPostTags(20);

        $post = Post::find($id);

        $category = Category::tree($id);

        $keyword = Post::find($id)->tags()->get();

        $keyword_str = Post::strKeyword($keyword);

        return View::make('post::admin.post.update')
                    ->with('tags',$tags)
                    ->with('post',$post)
                    ->with('category',$category)
                    ->with('listCategoryHtml',Category::buildOptionsCategoryHTML())
                    ->with('keyword',$keyword)
                    ->with('keyword_str',$keyword_str);
    }


    public function postUpdate($id=false) {
        if($this->checkValidator()) {
            $post = Post::firstOrNew(array('post_id'=>$id));
            $post->fill(Input::all());
            $post->user_id = Auth::user()->user_id;
            $post->save();

            PostCategory::updatePostCategory($id,Input::get('category'));

            PostTags::updatePostTags($id);

            return Redirect::route('admin.post.edit',array($post->post_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.post.edit',array($id));
        }
        else {
            return Redirect::route('admin.post.create')->withInput();
        }
    }


    private function checkValidator() {

        $rules = Post::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message(Lang::get('monster.updateSuccess'),'success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }

    public function getListCate() {
        $id=0;

        if(Input::get('node')>0) {
            $id=Input::get('node');
        }

        $cate_parent = Category::where('category_parent_id',$id)->get();

        $data = array();
        if(!empty($cate_parent)) {
            $key=0;
            foreach($cate_parent as $item) {
                $data[$key] = new \stdClass();
                $data[$key]->id = $item->category_id;
                $data[$key]->label = $item->category_name;
                $data[$key]->children = array();
                $hasChildren = Category::where('category_parent_id',$item->category_id)->get();
                if(!$hasChildren->isEmpty()) {
                    $data[$key]->load_on_demand = true;
                    $data[$key]->children = $hasChildren;
                }
                else {
                    $data[$key]->load_on_demand = false;
                }
                ++$key;
            }
        }

        return json_encode($data);
    }

}
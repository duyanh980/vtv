<?php namespace App\Modules\Post\Controllers\Admin;
use App\Modules\Post\Models\Tags;
use Auth, View, Lang, Monster, Datatable, Input, Validator;
use Illuminate\Support\Facades\Redirect;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 1:34 PM
 */

class TagController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getTagDataTable() {
        $items = Input::get('items');
        $action = strtolower(Input::get('action'));
        if($action!='') {
            if(!empty($items)) {
                if($action=='delete') {
                    Tags::whereIn('tag_id',$items)->delete();
                }
            }
            return;
        }
        else {
            return Datatable::collection(Tags::all())
                ->showColumns('keyword')
                ->searchColumns('keyword')
                ->orderColumns('keyword')
                ->addColumn('latest_update',function($item) {
                    return $item->updated_at->diffForHumans();
                })
                ->addColumn('action',function($item) {
                    return '<a href="'.route('admin.post.tag.edit',array($item->tag_id)).'" class="btn-sm btn-primary"><i class="fa fa-edit"></i> '.Lang::get('monster.edit').'</a>';
                })
                ->setRowID(function($item) {
                    return $item->tag_id;
                })
                ->make();
        }

    }


    public function getSearchTag() {
        $tags = Tags::where('keyword','LIKE',Input::get('term').'%')->take(10)->get();

        $data = array();
        if(!$tags->isEmpty()) {
            $i=0;
            foreach($tags as $var) {
                $data[$i] = new \stdClass();
                $data[$i]->label = $var->keyword;
                $data[$i]->id = $var->tag_id;
                ++$i;
            }
        }
        return json_encode($data);
    }

    public function getListTag() {
        return View::make('post::admin.tag.list');
    }

    public function getCreateTag() {
        return View::make('post::admin.tag.update');
    }

    public function getEditTag($id) {
        $tag = Tags::find($id);
        return View::make('post::admin.tag.update')->with('tag',$tag);
    }

    public function postUpdateTag($id=false) {

        if($this->checkValidatorTag()) {
            $tag = Tags::firstOrNew(array('tag_id'=>$id));
            $tag->fill(Input::all());
            $keyword = Tags::where('keyword','=',Input::get('keyword'))->get();
            foreach ($keyword as $item ) {
                if (isset($item)) {
                    $messages = Lang::get('post::monster.existingKeyword');
                    Monster::set_message($messages,'error',true);
                    return Redirect::route('admin.post.tag.create')->withInput();
                }
            }
            $tag->save();
            return Redirect::route('admin.post.tag.edit',array($tag->tag_id));
        }

        if($id) { // if is editing
            return Redirect::route('admin.post.tag.edit',array($id));
        }
        else {
            return Redirect::route('admin.post.tag.create')->withInput();
        }
    }

    private function checkValidatorTag() {

        $rules = Tags::$rules;
        $validator = Validator::make(Input::all(),$rules);
        if ($validator->passes()) {
            Monster::set_message(Lang::get('monster.updateSuccess'),'success');
            return true;
        }
        $messages = $validator->messages()->all('<p>:message</p>');
        Monster::set_message($messages,'error',true);
        return false;
    }

}
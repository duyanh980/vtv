<?php

use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories',function($table) {
            $table->increments('category_id');
            $table->string('category_name',250);
            $table->string('category_slug',250);
            $table->integer('category_order');
            $table->integer('category_parent_id')->default(0);
            $table->integer('user_id');
            $table->integer('order');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            Schema::create('post_categories',function($table) {
                $table->bigInteger('post_id');
                $table->bigInteger('category_id');

                Schema::create('posts',function($table) {
                    $table->bigIncrements('post_id');
                    $table->string('post_title',250);
                    $table->longText('post_content');
                    $table->integer('status');
                    $table->integer('user_id');
                    $table->string('image',250);
                    $table->timestamps();

                    Schema::create('post_tags',function($table) {
                        $table->bigInteger('post_id');
                        $table->bigInteger('tag_id');

                        Schema::create('tags',function($table) {
                            $table->bigIncrements('tag_id');
                            $table->Text('keyword');
                            $table->timestamps();
                        });
                    });
                });
            });
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('categories');
        Schema::drop('post_categories');
        Schema::drop('posts');
        Schema::drop('post_tags');
        Schema::drop('tags');
	}

}

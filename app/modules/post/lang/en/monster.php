<?php
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/22/14
 * Time: 11:23 PM
 */


return array(
    'moduleManager'     =>  'Post Manager',
    'addNew'            =>  'Add New Post',
    'editPost'          =>  'Edit Post',
    'postName'          =>  'Title',
    'manager'           =>  'Post',
    'name'              =>  'Name',
    'slug'              =>  'Slug',
    'class'             =>  'Class',
    'target'            =>  'Target',
    'postTitle'         =>  'Title',
    'postContent'       =>  'Content',
    'editTag'           =>  'Edit Tag',
    'keyword'           =>  'Keyword',
    'addNewTag'         =>  'Add New Tag',
    'existingKeyword'   =>  'Existing keyword',
    'tagManager'        =>  'TAG MANAGER',
    'categories'        =>  'Categories',
    'category'          =>  'Category',
    'featuredImage'     =>  'Featured Image',
    'add'               =>  'Add',
    'tag'               =>  'Tag',
    'choose_tag'        =>  'Choose from the most used tags',
    'select_tag'        =>  'Separate tags with commas',
    'order'             =>  'Order',
    'addNewCategory'    =>  'Add new category',
    'hideForm'          =>  'Hide form'
);
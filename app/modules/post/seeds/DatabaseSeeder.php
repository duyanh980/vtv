<?php namespace App\Modules\Post\Seeds;
use App\Modules\Menu\Models\MenuItem;
use App\Modules\Post\Models\Post;
use App\Modules\Post\Models\Category;
use App\Modules\Post\Models\Tags;
use App\Modules\Post\Models\PostTags;
use App\Modules\Post\Models\PostCategory;

/**
 * Author: Trong
 * Date: 4/25/14
 * Time: 11:37 PM
 */

class DatabaseSeeder extends \Seeder {

    public function run() {

        Post::truncate();
        Category::truncate();
        Tags::truncate();
        PostTags::truncate();
        PostCategory::truncate();

        Post::create(array(
            'post_title'   =>  'Draghi hints at ECB policy move',
            'post_content' =>  'The European Central Bank keeps its benchmark interest rate at 0.25%, but bank chief Mario Draghi hints it may act soon to counter low inflation in the eurozone.',
            'status' =>  '1',
            'user_id' =>  '1',
            'image' =>  '',
        ));
        Post::create(array(
            'post_title'   =>  'Why we need to make good processed food',
            'post_content' =>  'Can we create meals that are both satisfying and enjoyable but more convenient and better tailored to our physical needs? Glenn Zorpette thinks so. ',
            'status' =>  '1',
            'user_id' =>  '1',
            'image' =>  '',
        ));

        Category::create(array(
            'category_name'   =>  'Bussiness',
            'category_slug' =>  '',
            'category_parent_id' =>  '0',
            'user_id' =>  '1',
            'order' =>  '1',
        ));

        Category::create(array(
            'category_name'   =>  'Health',
            'category_slug' =>  '',
            'category_parent_id' =>  '0',
            'user_id' =>  '1',
            'order' =>  '1',
        ));

        Tags::create(array(
            'keyword'   =>  'News',
        ));

        Tags::create(array(
            'keyword'   =>  'Bussiness',
        ));

        Tags::create(array(
            'keyword'   =>  'Heatlh',
        ));


        PostCategory::create(array(
            'post_id'   =>  '1',
            'category_id'  => '1'
        ));

        PostCategory::create(array(
            'post_id'   =>  '2',
            'category_id'  => '2',
        ));

        PostTags::create(array(
            'post_id'   =>  '1',
            'tag_id'  => '1',
        ));

        PostTags::create(array(
            'post_id'   =>  '2',
            'tag_id'  => '2',
        ));


        $postMenu = MenuItem::create(array(
            'menu_item_name'    =>  'Posts',
            'menu_item_url'     =>  '/admin/post',
            'menu_item_class'   =>  'fa-edit',
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1,
        ));

        MenuItem::create(array(
            'menu_item_name'    =>  'Posts',
            'menu_item_url'     =>  '/admin/post',
            'menu_item_class'   =>  'fa-edit',
            'menu_item_parent_id'=> $postMenu->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1,
        ));
        MenuItem::create(array(
            'menu_item_name'    =>  'Categories',
            'menu_item_url'     =>  '/admin/post/category',
            'menu_item_class'   =>  'fa-bars',
            'menu_item_parent_id'=> $postMenu->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1,
        ));
        MenuItem::create(array(
            'menu_item_name'    =>  'Tags',
            'menu_item_url'     =>  '/admin/post/tags',
            'menu_item_class'   =>  'fa-tags',
            'menu_item_parent_id'=> $postMenu->menu_item_id,
            'menu_id'           =>  1,
            'status'            =>  1,
            'user_id'           =>  1,
            'order'             =>  1,
        ));
    }
}
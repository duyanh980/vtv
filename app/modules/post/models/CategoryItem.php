<?php namespace App\Modules\Post\Models;
use Illuminate\Support\Facades\Request;
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 4/28/14
 * Time: 1:49 PM
 */

class CategoryItem extends \Eloquent {

    protected $table = 'categories';
    protected $primaryKey = 'category_id';
    protected $fillable = array('category_name','category_slug','category_order','category_parent_id','user_id','status');

    protected static $menuItem = array();
    public static $rules = array(
        'category_name'=>'required|max:50',
        'status'=>'required|integer',
    );

    public static function buildListMenuItemHtml($menu_id,$catalog=false,$parent_id=0) {
        $str='';
        if(!is_array($catalog) AND $parent_id==0) {
            $catalog = self::listOrder($menu_id, $catalog);
        }
        if($catalog===false AND $parent_id==0) {
            $catalog = self::listOrder($menu_id);
        }
        if(!empty($catalog)) {
            $str .= '<ol class="dd-list">';
            foreach($catalog as $var) {
                $str .='<li class="dd-item dd3-item" data-id="'.$var->category_id.'">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content"><a href="'.route('admin.post.manager',array($var->menu_id,$var->category_id)).'">'.$var->category_name.'</a></div>';
                if(!empty($var->items)){
                    $str .= self::buildListMenuItemHtml($menu_id,$var->items,$var->category_parent_id);
                }
                $str .='</li>';
            }
            $str .='</ol>';
        }
        return $str;
    }

    public static function listOrder($id=0,$parent_id=0,$listAll=true) {
        if(!$listAll) {
            $parent = CategoryItem::orderBy('order')->where('status',1)->where('category_parent_id','=', $parent_id)->where('category_id','!=',$id)->get();
        }
        else {
            $parent = CategoryItem::orderBy('order')->where('category_parent_id','=', $parent_id)->get();

        }
        $category = array();

        if(!empty($parent)) {
            $module = Request::segment(1).'/'.Request::segment(2);
            foreach($parent as $key => $var) {

                $category[$key] = $var;
                $category[$key]->items = self::listOrder($id,$var->category_id,$listAll);

            }
        }

        return $category;

    }


}
<?php namespace App\Modules\Post\Models;
use Illuminate\Support\Facades\Input;
use DB;
use App\Modules\Post\Models\Tags;
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 5/7/14
 * Time: 8:31 PM
 */

class PostTags extends \Eloquent {

    protected $table = 'post_tags';
    protected $primaryKey = 'post_id';
    protected $fillable = array('post_id','tag_id');

    public    $timestamps = false;

    public static $rules = array(
        'post_id'=>'required|integer',
        'tag_id'=>'required|integer',
    );

    public static  function getLimitPostTags($limit=false) {
        $tags = PostTags::groupBy('tag_id')->orderBy('count', 'desc')->take($limit)->select(DB::raw('count(\'tag_id\') as count'),'tag_id')->get();
        $data = array();
        if(!$tags->isEmpty()) {
            foreach ($tags as $item ) {
                $result = Tags::find($item->tag_id);
                if($result) {
                    $data[] = $result;
                }
            }
        }
        return $data;
    }
    public static function updatePostTags($id=false) {

        $tag_arr = Input::get('tag');

        PostTags::where('post_id', '=', $id)->delete();

        if (isset($tag_arr)) {

            foreach ($tag_arr as $keyword ) {

                $tag = Tags::where('keyword', '=', $keyword)->get();

                if (is_array($tag)) {
                    foreach ($tag as $item ) {
                        $postTag = PostTags::create(array('post_id' => $id, 'tag_id' => $item));
                        $postTag->save();
                    }
                } else {
                    $tags = Tags::firstOrNew(array('keyword' => $keyword));
                    $tags->save();

                    $postTag = PostTags::firstOrNew(array('post_id' => $id, 'tag_id' => $tags->tag_id));
                    $postTag->save();

                }
            }

        }

    }


}
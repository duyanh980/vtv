<?php namespace App\Modules\Post\Models;
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 4/28/14
 * Time: 1:50 PM
 */

class Post extends \Eloquent {

    protected $table = 'posts';
    protected $primaryKey = 'post_id';
    protected $fillable = array('post_title','post_content','status','image');


    public static $rules = array(
        'post_title'=>'required|min:1|max:50',
        'status'=>'required|integer',
    );

    public function tags() {
        return $this->belongsToMany('App\Modules\Post\Models\Tags', 'post_tags', 'post_id', 'tag_id');
    }

    public static function strKeyword($keyword) {
        $data = array();
        if(!empty($keyword)) {
            foreach ($keyword as $item) {
                $data[] = $item->keyword;
            }
            $keyword_str = implode( "','" , $data);

            return $keyword_str;
        }
    }

}
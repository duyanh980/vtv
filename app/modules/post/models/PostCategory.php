<?php namespace App\Modules\Post\Models;
use Illuminate\Support\Facades\Input;
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 5/7/14
 * Time: 8:31 PM
 */

class PostCategory extends \Eloquent {

    protected $table = 'post_categories';
    protected $primaryKey = 'post_id';
    protected $fillable = array('post_id','category_id');

    public $timestamps = false;

    public static $rules = array(
        'post_title'=>'required|min:1|max:50',
        'category_id'=>'required|integer',
    );

    public static function updatePostCategory($id=false,$categories = array()) {

        if (!empty($categories)) {
            self::where('post_id', '=', $id)->delete();
            foreach ($categories as $category_id) {
                $postCate = self::create(array('post_id' => $id, 'category_id' => $category_id));
                $postCate->save();
            }
        }

    }
}
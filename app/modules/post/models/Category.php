<?php namespace App\Modules\Post\Models;
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 4/28/14
 * Time: 1:49 PM
 */

class Category extends \Eloquent {

    protected $table = 'categories';
    protected $primaryKey = 'category_id';
    protected $fillable = array('category_name','category_slug','category_order','category_parent_id','user_id','status');

    protected static $menuItem = array();

    public static $rules = array(
        'category_name'=>'required|max:50',
        'status'=>'required|integer',
    );

    public static function buildListMenuItemHtml($catalog=false,$parent_id=0) {
        $str='';
        if(!is_array($catalog) AND $parent_id==0) {
            $catalog = self::listOrder($catalog);
        }
        if($catalog===false AND $parent_id==0) {
            $catalog = self::listOrder();
        }
        if(!empty($catalog)) {
            $str .= '<ol class="dd-list">';
            foreach($catalog as $var) {
                $str .='<li class="dd-item dd3-item" data-id="'.$var->category_id.'">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content"><a href="'.route('admin.post.category',array($var->category_id)).'">'.$var->category_name.'</a></div>';
                if(!empty($var->items)){
                    $str .= self::buildListMenuItemHtml($var->items,$var->category_parent_id);
                }
                $str .='</li>';
            }
            $str .='</ol>';
        }
        return $str;
    }

    public static function tree($post_id,$catalog=false,$parent_id=0,$isFirst=true) {
        $str='';
        if(!is_array($catalog) AND $parent_id==0) {
            $catalog = self::listOrder($catalog);
        }
        if($catalog===false AND $parent_id==0) {
            $catalog = self::listOrder();
        }

        $cate_all = PostCategory::where('post_id','=',$post_id)->get();

        $data = Array();
        foreach ($cate_all as $item) {
            $data[$item->category_id] = $item->category_id;
        }

        if(!empty($catalog)) {
            if($isFirst) {
                $str .= '<ul class="jqtree_common jqtree-tree" style="list-style: none;">';
            }
            else {
                $str .= '<ul class="jqtree_common" style="">';
            }
            foreach($catalog as $var) {

                $str .= '<li class="jqtree_common jqtree-folder jqtree-closed" data-id="'.$var->category_id.'">
                        <div class="jqtree-element jqtree_common" style="padding-bottom: 5px;">
                        <i class="jqtree_common jqtree-toggler jqtree-closed">
                        <input type="checkbox" name="category[]" value="' . $var->category_id . '"';

                if (isset($data[$var->category_id])) {
                    $str .= 'checked';
                }

                $str .= ' /></i><span class="jqtree_common jqtree-title"> '.$var->category_name.'</span></div>';

                if(!empty($var->items)){
                    $str .= self::tree($post_id,$var->items,$var->category_parent_id,false);
                }
                $str .='</li>';

            }
            $str .='</ul>';
        }
        return $str;
    }

    public static function buildOptionsCategoryHTML($catalog=false,$pre = null,$parent_id=0) {
        $str = '';
        if(!is_array($catalog) AND $parent_id==0) {
            $catalog = self::listOrder($catalog);
        }
        if($catalog===false AND $parent_id==0) {
            $catalog = self::listOrder();
        }
        if(!empty($catalog)) {
            foreach($catalog as $var) {
                if($parent_id != $var->category_parent_id) {
                    $pre .= '-- ';
                    $parent_id = $var->category_parent_id;
                }
                $str .= '<option value="'.$var->category_id.'">'.$pre.$var->category_name.'</option>';
                if(is_array($var->items)){
                    $str .= self::buildOptionsCategoryHTML($var->items,$pre,$parent_id);
                }
            }
        }
        return $str;
    }


    public static function listOrder($id=0,$parent_id=0,$listAll=true) {
        if(!$listAll) {
            $parent = Category::orderBy('order')->where('status',1)->where('category_parent_id','=', $parent_id)->where('category_id','!=',$id)->get();
        }
        else {
            $parent = Category::orderBy('order')->where('category_parent_id','=', $parent_id)->where('category_id','!=',$id)->get();
        }
        $category = array();
        if(!$parent->isEmpty()) {
            foreach($parent as $key => $var) {
                $category[$key] = new \stdClass();
                $category[$key]->category_id = $var->category_id;
                $category[$key]->category_name = $var->category_name;
                $category[$key]->category_parent_id = $var->category_parent_id;
                $category[$key]->items = self::listOrder($id,$var->category_id,$listAll);
            }
        }
        return $category;
    }


}
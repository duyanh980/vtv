<?php namespace App\Modules\Post\Models;
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 5/5/14
 * Time: 3:38 PM
 */
class Tags extends \Eloquent {

    protected $table = 'tags';
    protected $primaryKey = 'tag_id';
    protected $fillable = array('tag_id','keyword');

    protected $softDelete = false;

    public static $rules = array(
        'keyword'=>'required|min:1|max:50'
    );

    public function post() {
        return $this->belongsToMany('App\Modules\Post\Models\Post', 'post_tags', 'post_id', 'tag_id');
    }

    public function posttag() {
        return $this->hasMany('App\Modules\Post\Models\PostTags', 'post_id', 'tag_id');
    }
}
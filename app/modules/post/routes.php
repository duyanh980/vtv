<?php
/**
 * Created by PhpStorm.
 * User: trongvd85
 * Date: 4/28/14
 * Time: 1:52 PM
 */

Route::group(array('prefix'=>'admin', 'before'=>'auth.admin','namespace'=>'App\\Modules\\Post\\Controllers\\Admin'),function() {
    Route::pattern('post_id', '[0-9]+');
    Route::get('post',                        array('as'   =>  'admin.post.list',            'uses'  =>  'PostController@getList'));
    Route::get('post/dataTable',              array('as'   =>  'admin.post.dataTable',       'uses'  =>  'PostController@getDataTable'));
    Route::get('post/create',                 array('as'   =>  'admin.post.create',          'uses'  =>  'PostController@getCreate'));
    Route::get('post/edit/{post_id?}',        array('as'   =>  'admin.post.edit',            'uses'  =>  'PostController@getEdit'));
    Route::get('post/edit/list/category',     array('as'   =>  'admin.post.list.category',   'uses'  =>  'PostController@getListCate'));
    Route::post('post/update/{post_id?}',     array('as'   =>  'admin.post.update',          'uses'  =>  'PostController@postUpdate'));
});

Route::group(array('prefix'=>'admin', 'before'=>'auth.admin','namespace'=>'App\\Modules\\Post\\Controllers\\Admin'),function() {
    Route::pattern('tag_id', '[0-9]+');
    Route::get('post/tags',                   array('as'   =>  'admin.post.tag.list',        'uses'  =>  'TagController@getListTag'));
    Route::get('post/tags/dataTable',         array('as'   =>  'admin.post.tag.dataTable',   'uses'  =>  'TagController@getTagDataTable'));
    Route::get('post/tags/create',            array('as'   =>  'admin.post.tag.create',      'uses'  =>  'TagController@getCreateTag'));
    Route::get('post/tags/edit/{tag_id?}',    array('as'   =>  'admin.post.tag.edit',        'uses'  =>  'TagController@getEditTag'));
    Route::get('post/tags/search',            array('as'   =>  'admin.post.tag.search',      'uses'  =>  'TagController@getSearchTag'));
    Route::post('post/tags/view',             array('as'   =>  'admin.post.tag.view',        'uses'  =>  'TagController@postViewTag'));
    Route::post('post/tags/update/{tag_id?}', array('as'   =>  'admin.post.tag.update',      'uses'  =>  'TagController@postUpdateTag'));
});

Route::group(array('prefix'=>'admin', 'before'=>'auth.admin','namespace'=>'App\\Modules\\Post\\Controllers\\Admin'),function() {
    Route::pattern('cate_id', '[0-9]+');
    Route::pattern('post_id', '[0-9]+');
    Route::get('post/category',                               array('as'   =>  'admin.post.category.list',        'uses'  =>  'CateController@getList'));
    Route::get('post/category/dataTable',                     array('as'   =>  'admin.post.category.dataTable',   'uses'  =>  'CateController@getDataTable'));
    Route::get('post/category/edit/{cate_id?}',               array('as'   =>  'admin.post.category',             'uses'  =>  'CateController@getManager'));
    Route::get('post/category/listCate/{post_id?}',           array('as'   =>  'admin.post.category.listCate',    'uses'  =>  'CateController@getListTreeCate'));
    Route::get('post/category/selectListCate',                array('as'   =>  'admin.post.category.selectListCate',    'uses'  =>  'CateController@getSelectListTreeCate'));
    Route::post('post/category/update/order',                array('as'   =>  'admin.post.category.order',       'uses'  =>  'CateController@postOrderItem'));
    Route::post('post/category/update/{cate_id?}',            array('as'   =>  'admin.post.category.update',      'uses'  =>  'CateController@postUpdateItem'));
    Route::post('post/category/update/post',                 array('as'   =>  'admin.post.category.update.post',       'uses'  =>  'CateController@postUpdateCatePost'));

});

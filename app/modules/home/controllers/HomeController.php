<?php namespace App\Modules\Home\Controllers;
use App\Libraries\Facebook;
use View, Redirect;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/17/14
 * Time: 9:33 PM
 */

class HomeController extends \FrontendController {

    public function __construct() {
        parent::__construct();
    }

    public function getIndex() {
         return Redirect::to("admin");
    }

    public function getLogin() {
    }
    public function getUser() {
        $user = Facebook::User();
    }
}
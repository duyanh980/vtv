<?php namespace App\Modules\Home\Controllers\Admin;
use View, Asset;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/27/14
 * Time: 2:32 PM
 */

class HomeController extends \BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function getDashBoard() {

        Asset::container('footer')->add('css3clock.script.js', 'assets/plugin/css3clock/script.js');
        Asset::container('footer')->add('css3clock.style.css', 'assets/plugin/css3clock/style.css');

        return View::make('home::admin.dashboard');
    }
}
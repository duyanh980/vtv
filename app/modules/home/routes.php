<?php 
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 4/17/14
 * Time: 9:35 PM
 */

//Frontend
Route::group(array('namespace'=>'App\\Modules\\Home\\Controllers'),function() {
    Route::get('/',array('as'=>'home.view' , 'uses' =>  'HomeController@getIndex'));
    Route::get('hlogin',array('as'=>'home.view' , 'uses' =>  'HomeController@getLogin'));
});
//Backend
Route::group(array('prefix'=>'admin','before'=>'auth.admin','namespace'=>'App\\Modules\\Home\\Controllers\\Admin'),function() {
    Route::get('/',array('as'=>'admin.dashboard' , 'uses' =>  'HomeController@getDashboard'));
});

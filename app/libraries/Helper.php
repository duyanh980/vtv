<?php namespace Helper;
/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 6/12/14
 * Time: 11:46 AM
 */

class Helper {

    protected static $labelFileSize = array('Bytes','Kb','Mb','Gb','Tb');

    public static function fileSize($size=0,$labelOffset = '0') {
        if($size>1024) {
            return self::fileSize($size/1024,++$labelOffset);
        }
        return round($size,2) . self::$labelFileSize[$labelOffset];
    }

    public static function PriceFormat($price=0.00,$current='SGD') {
        return ' '. $current . ' ' . number_format($price,2,'.',',');
    }

    public static function getYoutubeID($url) {
        $parts = parse_url($url);
        if(isset($parts['query'])){
            parse_str($parts['query'], $qs);
            if(isset($qs['v'])){
                return $qs['v'];
            }else if($qs['vi']){
                return $qs['vi'];
            }
        }
        if(isset($parts['path'])){
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path)-1];
        }
        return false;
    }
}
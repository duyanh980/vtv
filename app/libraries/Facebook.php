<?php namespace App\Libraries;

use Facebook\FacebookPageTabHelper;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Config, Session;
use Whoops\Example\Exception;
use DetectDevice;

/**
 * Author: Keith
 * Email: duyanh980@gmail.com
 * Date: 7/3/14
 * Time: 9:11 AM
 */

Class Facebook {
    public static $video_id = false;

    public static function init() {
        FacebookSession::setDefaultApplication(Config::get('facebook.id'),Config::get('facebook.secret'));
    }
    public static function Login() {
        self::init();
        $detect = new DetectDevice();
        if(!$detect->isMobile() && !$detect->isTablet()) {
            $loginHelper = new FacebookRedirectLoginHelper(Config::get('facebook.url'));
            $pageHelper = new FacebookPageTabHelper();
            $video_id = $pageHelper->getAppData();
        } else {
            $video_id = \Input::get("app_data");
            $url =url("video");
            if($video_id) {
                $url .= "?app_data=".$video_id;
            }
            $loginHelper = new FacebookRedirectLoginHelper($url);
            $pageHelper = $loginHelper;
        }
        self::$video_id = $video_id;
        // see if a existing session exists

        if (Session::has("facebook_token")) {
            // create new session from saved access_token
            $session = new FacebookSession(Session::get("facebook_token"));
            try {
                if (!$session->validate()) {
                    $session = null;
                }
            } catch (Exception $e) {
                $session = null;
            }
        } else {
            // no session exists
            try {
                if(!$detect->isMobile() && !$detect->isTablet()) {
                    $session = $pageHelper->getSession();
                } else {
                    $session = $loginHelper->getSessionFromRedirect();
                }
                if($session!='') {
                    Session::put("facebook_token",$session->getAccessToken());
                }
            } catch (FacebookRequestException $ex) {
                // When Facebook returns an error
            } catch (Exception $ex) {
                error_log($ex->getMessage());
            }
        }
        if(isset($session) And $session!='') {
            return true;
        }
        else {
            Session::forget("facebook_token");
        }
        $url = $loginHelper->getLoginUrl(Config::get('facebook.permission'));
        return $url;
    }

    public static function getAppData() {
        return self::$video_id;
    }
    public static function User() {
        return self::API('/me?fields=id,name,email');
    }

    public static function API($url,$method='GET',$data=array()) {
    
        self::init();
        if(Session::has("facebook_token")) {
            try {
                $session = new FacebookSession(Session::get("facebook_token"));
                $request = new FacebookRequest($session,$method,$url);
                $response = $request->execute();
                $data = $response->getGraphObject();
                return $data;
            } catch(FacebookRequestException $ex) {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function isLogin() {
        return Session::has("facebook_token");
    }
}